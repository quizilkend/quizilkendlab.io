Auf [MkDocs](https://gitlab.com/pages/mkdocs/-/blob/master/README.md) basierende Webseite, mit Material für Schüler:innen der Fächer Physik und NwT. Zusammengestellt von Samuel Greiner (Kepler Gymnasium Tübingen)

Aus den markdown-Dateien generierte PDFs finden sich hier: [aktuelle PDFs](https://gitlab.com/quizilkend/quizilkend.gitlab.io/-/jobs/artifacts/master/browse?job=pdfpages)

---

Lizenz:
sofern nicht anders angegeben, stehen alle Materialien hier unter CC-BY-SA-NC 4.0:

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
