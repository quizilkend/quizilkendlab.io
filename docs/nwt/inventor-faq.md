---
title: Inventor FAQ
description: Antworten auf häufige Fragen zu Inventor
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Inventor - FAQ

## Ich möchte eine Zeichnung zum Fräsen exportieren {#sec:DXFExport}

Am Kepi in Tübingen stehen zum Fräsen von Holz KOSY-Fräsen zur Verfügung. Diese werden über die Software nccad angesteuert.

Nccad kann 2D-Zeichnungen im `.DXF`-Format lesen. Praktischerweise kann Inventor genau das auch exportieren. Um also Holz von deiner Lehrkraft fräsen zu lassen, gehst du so vor.

1. Erstelle dein Bauteil mit Inventor. Achte darauf, dass alle Skizzen vollständig bestimmt sind!

    ![Bauteil zum Exportieren](inventor-faq-assets/ExportZumFraesen1.png){ loading=lazy width="40%"}

2. Wähle die obere Fläche deines Bauteils durch <kbd>Klick</kbd> aus.

    ![Ausgewählte Fläche zum Exportieren](inventor-faq-assets/ExportZumFraesen2.png){ loading=lazy width="40%"}

3. Öffne das Kontextmenü durch <kbd>Rechtsklick</kbd> und wähle `Fläche exportieren als...` aus.

    ![Kontextmenü zum Exportieren der Fläche](inventor-faq-assets/ExportZumFraesen3.png){ loading=lazy width="30%"}

4. In dem Fenster, das sich öffnet vergibst du einen schlauen Namen, kontrollierst, kontrollieren, dass das Format `Autocad DXF (*.dxf)` ausgewählt ist und speicherst die Datei an eine sinnvolle Stelle.

    ![Export Fenster](inventor-faq-assets/ExportZumFraesen4.png){ loading=lazy width="40%"}

    Fertig! Nun kannst du die Datei an deine Lehrerin zum Fräsen geben.


## Von der `dxf`-Zeichnung zum Fräsen mit nccad

Die von Inventor exportierten `.dxf`-Zeichnungen können in nccad importiert werden, um die letzten Schritte auf dem Weg zum Fräsen durchzuführen.

1. Import der Zeichnung
    Im Startbildschirm unter Import `DXF` anwählen und die oben exportierte Datei auswählen.
    
    ![nccad Import DXF zum Fräsen](inventor-faq-assets/nccad-import-dxf.png){ loading=lazy width="85%"}

2. Konvertieren der Polygonzüge zu einzelnen Geraden.
    Inventor exportiert die Zeichnung als Polygonzüge und Kreisbögen. Möchte man an einzelnen Segmenten etwas ändern, müssen die Polygonzüge zunächst wieder in die Segmente konvertiert werden. Das geht in nccad mit dem Werkzeug `Konvertierungen`. Man wählt die richtige Konvertierung aus, klickt auf `Ausführen` und klickt anschließend die Polygonzüge an, die man zerlegt haben möchte.  

    ![nccad Auswahl Konvertierungs-Werkzeug](inventor-faq-assets/nccad-konvertierung1.png){ loading=lazy width="48%" align=left}

    ![nccad Ergebnis der Konvertierung der Polygonzüge](inventor-faq-assets/nccad-konvertierung2.png){ loading=lazy width="48%"}

    Nun können Änderungen an den einzelnen Segmenten vorgenommen werden.

3. Parameter vergeben

4. Fräsbahnen überprüfen

5. Ab zum Fräsen
    - Werkstück einlegen
    - Werkstück Nullpunkt setzen.
    - Los geht's