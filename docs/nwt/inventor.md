---
title: Inventor Kurs
description: Übersicht über die Kursinhalte
author: Jürgen Lange, Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Inventor Kurs

Hier gibt es einen ausführlichen Kurs zu den Grundfunktionen und einigen fortgeschrittenen Funktionen von Inventor

- [Anleitung](inventor-assets/2024-11-28 Inventor Anleitung.pdf): Von der ersten Konstruktion bis zur Baugruppe und Simulationen

- [Übungen](inventor-assets/2023-11-19 Inventor Übungen.pdf): Begleitend zu den Anleitungen


## Häufige Fragen zu Inventor

Am Ende der Anleitung ist ein Kapitel mit häufigen Fragen zum Konstruieren mit Inventor. Falls du also an einer Stelle nicht weiterkommst, lohnt es sich dort mal nachzusehen.

# Geschickte Planung und Konstruktion eines Bauteils

![Geschickte Planung und Konstruktion eines Bauteils](FreeCAD-assets/FreeCAD-Geschicktes_Konstruieren.png){ loading=lazy width="80%"}

CC-BY-NC-SA Jürgen Gräber

