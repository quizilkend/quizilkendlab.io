---
title: Platinendesign
description: Allgemeine Hinweise zum Platinendesign
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Platinendesign

Sobald man eine etwas komplexere Schaltung hat, wird der Aufbau auf einem Steckbrett schnell ärgerlich.

Schnell kann es zu unzuverlässigen Kontakten kommen, es hängen zu viele Kabel herum und die Schaltung ist trotz weniger Bauteile kaum noch zu überblicken.

![Zu viele Kabel auf dem Breadboard](PCB-assets/too-many-cables.jpg){ loading=lazy width="75%"}


Daher werden kompliziertere Schaltungen meistens auf einer Platine realisiert. Hier werden auf einer leitenden Platte (meist mit Kupfer beschichteter Kunststoff) Unterbrechungen gefräst, sodass sich am Ende leitende Bahnen zwischen den Bauteilen ergeben. Die Bauteile müssen anschließend nur noch eingelötet werden und wenn man gut geplant hat, ist die Schaltung auf diese Art solide umgesetzt.

![PCB noch ohne Bestückung](PCB-assets/pcb-copper.jpg){ loading=lazy width="75%"}

## Schritte zu einer fertigen Platine

Um zu einer fertigen Platine zu kommen braucht es einige Schritte:

1. [Schaltung planen](fritzing.md#sec:SchaltungPlanen)  
    Die Bauteile auswählen und Verbindungen planen.  
    Einen übersichtlichen Schaltplan erstellen.
2. [Platine planen](fritzing.md#sec:PlatineUmsetzen)  
    Platinenumriss planen, Bauteile auf der Platine platzieren, Verbindungen auf der Platine planen.
3. [Fräs- und Bohrvorgänge planen](#sec:CopperCam)  
    Auswählen welche Geometrien mit welchem Werkzeug und welchen Einstellungen hergestellt werden sollen.
4. [Fräsen](#sec:Fraesen)  
    Ausführung der geplanten Bearbeitung auf der Fräse

Die Schritte 1 und 2 benötigen die meiste Planung. Sie sind unter Verwendung der Software [fritzing](fritzing.md) beschrieben.


## Fräsen vorbereiten mit CopperCam {#sec:CopperCam}

[CopperCam](https://www.galaad.net/coppercam-deu.html) ist eine Software, die aus einer Gerber-Datei Fräsbahnen berechnen kann. Es lassen sich die unterschiedlichen Fräswerkzeuge der Fräse einstellen und das Exportformat wählen, sodass es zur Fräse passt.  
Damit eignet sich CopperCam gut, um Schüler:innen das Arbeiten mit der gewünschten PCB-Software zu ermöglichen ohne, dass diese einen für die Fräse passenden Export anbieten muss.

### Bei jeder Nutzung

Hier werden nur knapp die Schritte skizziert, um von einer exportierten Gerber-Datei zum NC-Code zu kommen.

1. Öffnen <kbd>Strg</kbd>+<kbd>O</kbd>
    - Falls nur auf einer Seite Leiterbahnen vorliegen, nur diese Seite öffnen, und zwar als `Lötseite`.
    - Den Text bei `Komponentenseite`löschen, falls nur eine Seite designt wurde, ansonsten hier die Datei für die Komponentenseite auswählen.
    - Bei Bohren die `...drill`-Datei wählen.
    - Da noch die Kontur-Ebene geöffnet wird, sollte `Isolationskonturen sofort berechnen` abgewählt werden.
2. Kontur öffnen  
    Für die passende Form wird die Kontur als zusätzliche Ebene geöffnet:  
    `Datei` → `Öffnen` → `Weitere Ebene` (<kbd>Strg</kbd>+<kbd>Shift</kbd>+<kbd>O</kbd>)
3. Layout überprüfen: Passt alles, soweit wie gewünscht? 
    - Sollte ein Groundfill gewünscht sein, kann man die GND-Pads deaktivieren, dann sind die Lötstellen mit der Kupferseite verbunden.
    - Sind die Lötpunkte zu klein, kann man über `Bearbeiten` -> `Lötpunkte bearbeiten` die Größe der Lötpunkte gruppenweise ändern.
    - Sind die Bohrungen unpassend zu den Fräs- und Bohrwerkzeugen? Dann kann über `Parameter` -> `Aktive Werkzeuge` eingestellt werden, ob die Bohrungen rundgefräst werden sollen, und ab welchem Durchmesser das passieren soll.
4. Isolationskonturen berechnen  
    ![Icon Fräskonturen berechnen](PCB-assets/CopperCam-Fraeskonturen-berechnen.png){ loading=lazy .off-glb} in der Werkzeugleiste wählen.  
    Beachten, dass Leiterbahnen, die auf der Unterseite laufen sollen mögicherweise gespiegelt werden müssen.
5. NC-Code ausgeben  
    `Maschine` → `Fräsen`. Hier noch die Reihenfolge der einzelnen Schritte auswählen, sodass möglichst wenige Werkzeugwechsel nötig sind.  
    Der NC-Code wird im Editor angezeigt und kann abgespeichert werden.

### Vor der ersten Benutzung

Schritte die in CopperCam vor der ersten Benutzung durchgeführt werden sollten. Diese Einstellungen sollten dann auf allen Rechnern einer Schule gleich sein.

1. Werkzeuge definieren  
    `Parameter` → `Werkzeugsammlung`
2. Anwendungen der Werkzeuge definieren  
    `Parameter` → `Aktive Werkzeuge`  
    Hier wird angegeben, welches Werkzeug mit welchen Parametern für den jeweiligen Arbeitsschritt verwendet werden soll.
3. Passendes Exportformat wählen  
    `Parameter` → `Ausgabedatenformat`  
    Ein Format wählen, was die jeweilige Fräse versteht. Falls die Daten nicht bekannt sind hilft leider nur Ausprobieren.  
    Für die KOSY kann das HPGL-Format genutzt werden.

### Wenn etwas nicht funktioniert

Hier sind Schwierigkeiten aufgeführt, die mir schon einmal passiert sind und auch, wie man sie löst.

- Bohrungen, Kontur und Leiterbahnen sind an unterschiedlichen Orten und teils verdreht.
    Fritzing scheint beim Export nicht immer alles richtig zu machen und manchmal die Koordinaten zu verzocken.
    In dem Fall müssen die Ebenen (Kupferunterseite, Bohrungen und Kontur) einzeln importiert werden. In Coppercam können die Ebenen einzeln ausgewählt werden (in der Werkzeugleiste ziemlich weit rechts) und dann gedreht und/oder verschoben werden (in der Menü-Leiste unter Datei). So legt man die Ebenen wieder richtig aufeinander und verfährt danach wie gewohnt.
- Der Groundfill von fritzing funktioniert nicht.
    Fritzing ist im Export des Groundfills nicht besonders geschickt. Oft ist es praktischer den Groundfill nicht in Fritzing durchzuführen, sondern die entsprechenden Lötpads in CopperCam zu deaktivieren (siehe oben).    



## Fräsen mit der KOSY {#sec:Fraesen}

Zum Fräsen an der KOSY werden in nccad folgende Schritte ausgeführt:

1. Import  
    `Datei` → `Import` → `HPGL` 
    Die entsprechende Datei auswählen
2. Werkzeuge zuweisen  
    - CopperCam hat beim Export die Formen bereits gruppiert, sodass den Gruppen nur noch das entsprechende Werkzeug und die Bearbeitungsparameter zugewiesen werden müssen.  
    - Wichtig ist noch die Option `Stop für Handbetrieb` bei allen Werkzeugen auszuwählen, damit die Fräse nach jedem Bearbeitungsvorgang anhält. So kann man das Werkzeug wechseln. Und den Fräsvorgang dann fortsetzen.
    - Ich verwende auf der KOSY folgende Parameter:
        - Gravieren: $150 \cdot \frac{0,1 mm}{s}$ für die xy-Richtung und $30 \cdot \frac{0,1 mm}{s}$ in z-Richtung; 0,15 mm tief.
        - Bohren: $30 \cdot \frac{0,1 mm}{s}$ in z-Richtung 
        - Ausschneiden: mit 3mm Schaftfräser $50 \cdot \frac{0,1 mm}{s}$ in xy-Richtung und $30 \cdot \frac{0,1 mm}{s}$ in z-Richtung
3. Los geht's