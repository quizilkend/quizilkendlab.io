---
title: Arduino - FAQ
description: Antworten auf häufige Fragen beim Arduino
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---


# Arduino - FAQ

## Ich kann meinen Sketch nicht speichern/keine Bibliotheken installieren

Das Problem liegt hierbei im Berechtigungsmanagement der Schul-Laptops. Die Arduino-IDE hat keine Schreibrechte auf den `Dokumente`-Ordner und kann daher dort auch keine Bibliotheken ablegen, oder sketches abspeichern.

**Lösung:**

1. Den Haupt-Speicherort ändern:  
Einstellungen öffnen: `Datei → Voreinstellungen`  
Dort unter  `Sketchbook-Speicherort` einen anderen Ort auswählen. Bspw: direkt im Benutzerordner den Ordner `Arduino` erstellen (`C:\Users\schueler\Arduino`)

2. Testen ob sich der Sketch jetzt dort abspeichern lässt.

3. Eine Bibliothek installieren:  
Unter `Werkzeuge → Bibliotheken verwalten` nach der entsprechenden Bibliothek suchen und installieren.

Glückwunsch! Nun sollte sich die Bibliothek und die Arduino Entwicklungsumgebung insgesamt einfach nutzen lassen.


## Mein Code lässt sich nicht auf den Arduino hochladen

Manchmal lässt sich der geschriebene Code nicht auf den Arduino hochladen. Das kann mehrere Ursachen haben:

1. Der Arduino ist *überlastet*.  
Du hast einen Motor oder ähnliches angeschlossen, was viel Leistung vom Arduino fordert.  
**Lösung:** Du unterbrichst für die Zeit des Hochladens den Kontakt zu den Komponenten die Leistung ziehen.  
Zum Beispiel: Motor, MotorShield, Pumpe, etc.

2. Der Serielle Port ist bereits besetzt:  
Die Kommunikation per USB zwischen Arduino und PC läuft über den `seriellen Port`. Sollte dieser bereits besetzt sein, da etwas an den seriellen Pins `RX` und `TX`, muss man während des Hochladens die Kontakte an diesen Pins unterbrechen.

## Ich möchte mehrere Größen mit dem Serial-Plotter visualisieren

Das geht tatsächlich relativ einfach. Die darzustellenden Größen müssen nur in einer Zeile durch ein Komma getrennt auf der seriellen Schnittstelle ausgegeben werden.

Ein Beispiel, bei dem 3 Werte (val1, val2 und val3) parallel geplottet werden:

````
Serialprint(val1);
Serialprint(",");
Serialprint(val2);
Serialprint(",");
Serialprintln(val3);
````

Die Funktionalität des Plotters ist etwas eingeschränkt, so können die Größen nur in einem Diagramm dargestellt werden und außerdem haben sie alle die gleiche y-Achse. Dennoch ist die Darstellung für die meisten Zwecke ausreichend und vor allem schnell eingerichtet.
