---
title: FreeCAD Kurs
description: Übersicht über die Kursinhalte
author: Jürgen Gräber, Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---
# FreeCAD Kurs

Hier gibt es den exzellenten FreeCAD-Kurs von Jürgen Gräber und unten ein paar Zusatzinformationen, sowie auf einer separaten Seite eine [FreeCAD FAQ](/nwt/freecad-faq)

Der Kurs gliedert sich in folgende Kapitel:

- [I. Erste Konstruktionen, Technische Zeichnung](FreeCAD-assets/2022-11-13_FreeCAD_Anleitung_I_(GR).pdf)

- [II. Fortgeschrittene Konstruktionen und Baugruppen](FreeCAD-assets/2020-12-08_FreeCAD_Anleitung_II_(GR).pdf)

- [IV. Biege-/Festigkeitssimulationen: FEM](FreeCAD-assets/2020-05-22_FreeCAD_Anleitung_IV_FEM_GR.pdf)
    - Ergänzung: [Materialdaten zu Kiefernholz](FreeCAD-assets/Material_Kiefer_Longitudinal.FCMat){ download="Material_Kiefer_Longitudinal.FCMat" }

## Geschickte Planung und Konstruktion eines Bauteils

![Geschickte Planung und Konstruktion eines Bauteils](FreeCAD-assets/FreeCAD-Geschicktes_Konstruieren.png){ loading=lazy width="80%"}

CC-BY-NC-SA Jürgen Gräber

## FreeCAD-Zusammenfassung

Auf diesem Blatt sind einige der wichtigsten Fähigkeiten im Umgang mit einer CAD-Software zusammengefasst und die entsprechenden Abschnitte im Kurs genannt.

![FreeCAD Zusammenfassung](FreeCAD-assets/FreeCAD-Zusammenfassung.png){ loading=lazy width="60%"}

## Video-Kurs

Abgestimmt auf die Schritte dieses Kurses gibt es Lehrvideos auf YouTube. Diese werden bei Änderungen und Aktualisierungen des Kurses jedoch nicht zwingend angepasst, sodass es kleine Unterschiede geben kann.

[Video Kurs](https://www.youtube.com/watch?v=hJanMQTHAOI&list=PL2pwOSjXuHVD7ZGsTPb4MelAwQs5a3ohm)


## Urheber- und Lizenzinformationen

Der Kurs wurde von Jürgen Gräber erstellt, welcher ihn unter [CC-BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0) veröffentlicht hat.

Ergänzungen hier auf der Seite stehen unter den in [Lizenz](/index#sec:Lizenz) angegebenen Bedingungen zur Verfügung.
