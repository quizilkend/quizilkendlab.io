---
title: 3D-Bauteile
description: 3D-Modelle häufig im NwT-Unterricht genutzter Komponenten
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# 3D-Bauteile

Hier gibt es verschiedene 3D-Modelle von Bauteilen aus dem NwT-Unterricht zum Download (für Inventor und FreeCAD).


## Metallbau Komponenten von Traudl-Riess

Um ein vollständigeres 3D-Modell eines Projekts erstellen zu können, gibt es hier die  Bauteile von Traudl-Riess als 3D-Modelle für FreeCAD und Inventor.

Um dieses Sammelsurium gut nutzen zu können, solltest du Baugruppen gut verwenden können ([FreeCAD](freecad.md)-Anleitung 2, [Inventor](inventor.md) Anleitung 1 Kap. 7).

![Getriebeauto assembly aus Traudl-Riess-Komponenten](traudl-komponenten-assets/Getriebeauto_assembly.png){ loading=lazy width="40%" align=right}

Beispiel eines Projekts mit den Metallbauteilen:

- Dateien für *[Inventor](traudl-komponenten-assets/traudl-komponenten-Inventor.zip){ download="traudl-komponenten-Inventor.zip" }*
- Dateien für *[FreeCAD](traudl-komponenten-assets/traudl-komponenten-FreeCAD.zip){ download="traudl-komponenten-FreeCAD.zip" }*

Die heruntergeladene Datei ist ein zip-Archiv, das man entpacken muss, um die einzelnen Dateien zu nutzen.

### Bennenung der Bauteile

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Buegel.png){ loading=lazy width="50%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Achse_Distanzrolle.png){ loading=lazy width="50%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Diverses.png){ loading=lazy width="50%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Platten.png){ loading=lazy width="50%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Raeder.png){ loading=lazy width="50%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Staebe.png){ loading=lazy width="50%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Winkelstaebe.png){ loading=lazy width="65%"}

![Traudl-Riess-Komponenten Bügel](traudl-komponenten-assets/Traudl-Bennenung_Zahnraeder.png){ loading=lazy width="50%"}

Die mit Lochraster versehenen Alustäbe gehören zwar nicht zum Sortiment von Traudl-Riess, dennoch sind sie praktisch und daher hier aufgeführt:

![Alu Lochraster Stäbe](traudl-komponenten-assets/Traudl-Bennenung-LochProfil.png){ loading=lazy width="50%"}



## Sonstige Bauteile

Auch andere Bauteile für NwT-Projekte wurden konstruiert, damit sie schnell verwendet werden können: **[Download als zip-Archiv](3D-Komponenten-assets/3D-Komponenten-assets.zip)**.


* Lampenbaustein mit 4 LEDs: `Lampenbautein`

    ![Lampenbaustein](3D-Komponenten-assets/Lampenbaustein.png){ loading=lazy width="30%"}

* 9V-Batterie mit Clip: `9V-Batt+Clip`

    ![9V Batterie mit Clip](3D-Komponenten-assets/9V-Batt+Clip.png){ loading=lazy width="30%"}
