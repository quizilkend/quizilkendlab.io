---
title: Naturwissenschaftliche Experimente
description: Hinweise zum naturwissenschaftlichen Experimentieren und der Versuchsdokumentation
author: Andreas Kalt, Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Naturwissenschaftliche Experimente

![Hübsche Grafik zur Illustration, dass es hier um Experimente geht](NwT-Experimentieren-assets/Experiment.webp){ loading=lazy width="100%"}

Naturwissenschaftliche Experimente dienen dazu Regelmäßigkeiten in der Natur zu finden und idealerweise diese auch zu erklären. Damit dieses Ziel erreicht werden kann, müssen verschiedene Kriterien erfüllt sein.

## Kriterien an naturwissenschaftliche Experimente

* Beim Experimentieren werden Beobachtungen unter **möglichst kontrollierten Bedingungen** gemacht. Das bedeutet, dass man in der Regel eine (teilweise) *künstliche* Umgebung schafft (z.B. in einem Labor), weil man die Bedingungen in der natürlichen Umwelt nicht vollständig kontrollieren kann.  
Kann man nicht alle Umgebungsbedingungen (»Faktoren«) kontrollieren oder ist das Experiment im Labor nicht möglich, versucht man möglichst viele Faktoren zu kontrollieren und sich der unbekannten Faktoren bewusst zu werden.

* Bei einem Experiment wird immer nur **ein Faktor** untersucht. Alle anderen Faktoren werden gleich gehalten. Der zu untersuchende Faktor wird möglichst mehrfach verändert, um möglichst genau abschätzen zu können, wie sich die untersuchte Eigenschaft in Abhängigkeit des untersuchten Faktors verhält.

* Als **Kontrolle** kann ein Durchgang dienen, bei dem der gesuchte Faktor ganz weggelassen oder eine Messung wiederholt werden. Damit kann überprüft werden, ob die Ergebnisse des Versuches vertrauenswürdig sind.

* Ein Experiment muss **wiederholbar und überprüfbar** sein, damit auch andere Wissenschaftler:innen sich überzeugen können, dass sie bei gleichen Ausgangsbedingungen zum gleichen Ergebnis kommen. Daher muss ein Experiment sehr exakt [protokolliert](#sec:Protokoll) werden.

### Fazit

Ein Experiment muss **komplett nachvollziehbar** und **möglichst objektiv** sein.

1. Nachvollziehbarkeit: Eine unbeteiligte Person muss das Experiment unter exakt gleichen Voraussetzungen wiederholen können
2. Objektivität: Die Ergebnisse des Experiments sind unabhängig von der Person, die es durchführt.

## Protokoll/Dokumentation {#sec:Protokoll}

Experimente sind ein wichtiger Bestandteil naturwissenschaftlicher Forschung. Jedes Experiment muss exakt protokolliert werden, um die Ergebnisse später nachvollziehen und vergleichen zu können.

Ein Versuchsprotokoll muss **klar formuliert** und **sauber formatiert** sein, so dass es auch für Personen außerhalb der eigenen Gruppe nachvollziehbar ist.

Ein Protokoll ist immer ähnlich aufgebaut.
Einen typischen Aufbau findest du in den nächsten Grafiken.

![Naturwissenschaftlicher Protokoll-Rohling Seite 1](NwT-Experimentieren-assets/Naturwissenschaftliches-Protokoll1.png){ loading=lazy width="47%" align=left}

![Naturwissenschaftlicher Protokoll-Rohling Seite 2](NwT-Experimentieren-assets/Naturwissenschaftliches-Protokoll2.png){ loading=lazy width="47%" align =right}

Stark basierend/übernommen von [Andreas Kalt CC-BY-SA](https://herr-kalt.de/nwt/versuchsprotokoll)
