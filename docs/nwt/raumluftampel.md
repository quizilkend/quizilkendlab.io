---
title: Arduino Raumluftampel
description: Beschreibung zu Bau und Programmierung in NwT7
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Arduino Raumluftampel

![Aufgebaute Raumluftampel](raumluftampel-assets/AufgebauteCO2Ampel_freigeschnitten.png){ loading=lazy width="50%"}

Durch die Corona-Pandemie im Jahr 2020 und 2021 und die hohe Übertragung in Innenräumen durch Aerosole, bekommt zielgerichtetes Lüften eine besondere Aufmerksamkeit. Eine Anzeige über die Qualität der Raumluft kann mehrere Vorteile bieten. Einerseits sinkt die Konzentrationsfähigkeit bei mangelnder Versorgung mit Sauerstoff, andererseits steigt die Wahrscheinlichkeit der Übertragung einer Infektionskrankheit über Aerosole, wenn die Luft nicht häufig durch Lüften ausgetauscht wird.

Daher ist es nicht nur zu Pandemie-Zeiten sinnvoll Klassenzimmer oder Innenräume mit Messgeräten auszustatten, die die Qualität der Raumluft anzeigen.

In den folgenden Abschnitten wird ein einfacher Aufbau einer solchen Ampel vorgestellt, dieser basiert auf einer indirekten Messung der Partikelanzahl in der Luft und der Auswertung mit einem Arduino Microcontroller.

## Messprinzip

Der Sensor MQ-135 ist ein breitbandiger Metalloxid-Halbleiter Gassensor. Kernstück ist ein Zinndioxid-Element.

**Stark vereinfacht** beruht das Messprinzip darauf, dass sich auf dem Material des Sensors Sauerstoffatome anlagern. Dabei nehmen die Sauerstoff-Atmome Elektronen aus dem Sensormaterial auf. Wenn sich nun Partikel aus der Umgebungsluft an die Sauerstoffatome anlagern, geben diese wiederum Elektronen an das Sensormaterial zurück. Durch die gestiegene Anzahl an Elektronen steigt die elektrische Leitfähigkeit des Sensormaterials.

Wenn wir also die elektrische Leitfähigkeit des Sensormaterials messen, können wir Rückschlüsse darauf ziehen, wie viele Partikel sich in der Raumluft befinden.

Damit eignet sich der Sensor nicht zum Nachweis eines spezifischen Gases in der Luft, allerdings aber für eine Messung der Partikelbelastung der Raumluft insgesamt. Daher kann diese Art der Messung als Hinweisgeber für rechtzeitiges Lüften verwendet werden.

Einen guten Eindruck der Vorgänge vermittelt folgende schematische Animation:

[![Schematische Animation von Figaro Enginnering Inc.](raumluftampel-assets/HalbleiterGassensor.gif){ loading=lazy}](https://www.figaro.co.jp/en/technicalinfo/principle/mos-type.html)



## Material {#sec:Material}

Das Set setzt sich aus folgendem Material zusammen:

* MQ-135 Luftgüte Sensor ([Datenblatt](raumluftampel-assets/MQ-135_Gas_Sensor_Modul_Datenblatt.pdf))  
  Wichtige Info: Bei erster Inbetriebnahme muss sich der Sensor etwa 24h am Stück "einbrennen". Am besten schließt man den fertigen Aufbau für ein bis zwei Tage an ein USB-Ladegerät an. Nach der Einbrennphase ist auf dem Sensor-Board gespeichert, dass das Einbrennen abgeschlossen ist.

    [![MQ-135 Sensor](raumluftampel-assets/mq-135.png){ loading=lazy width="20%"}](https://www.az-delivery.de/products/mq-135-gas-sensor-modul)

* Arduino Nano Microcontroller  

    [![Arduino Nano](raumluftampel-assets/ArduinoNano.png){ loading=lazy width="30%"}](https://www.az-delivery.de/products/nano-v3-0-pro?variant=6126616281115)

* RGB-LED ([Datenblatt](raumluftampel-assets/SMD_RGB_Modul_Datenblatt.pdf))  

    [![RGB-LED Platine](raumluftampel-assets/RGB-LED.png){ loading=lazy width="20%"}](https://www.az-delivery.de/products/smd-rgb-modul?variant=8154187300960)

* Buzzer (optional, [Datenblatt](raumluftampel-assets/Buzzer_Modul_passiv_Datenblatt.pdf))  

    [![Buzzer-Platine](raumluftampel-assets/Buzzer.png){ loading=lazy width="20%"}](https://www.az-delivery.de/products/buzzer-modul-passiv?variant=8175796584544)

* 400 Pin Steckbrett  

    [![400 Pin Steckbrett](raumluftampel-assets/400Breadboard.png){ loading=lazy width="20%"}](https://www.az-delivery.de/products/mini-breadboard?variant=12236752093280)

* Steckbrett Kabel  

    [![Jumper-Kabel](raumluftampel-assets/JumperCable.png){ loading=lazy width=15%}](https://www.az-delivery.de/products/steckbrucken-m-m-jumper-kabel)

## Aufbau

Die Schaltung wird auf einem Steckbrett aufgebaut. Dieses hat außen jeweils zwei Leiterbahnen, die sich für die `+` und `-` Verbindungen anbieten. Außerdem in der Mitte einen großen Bereich in dem die Pins immer in Spalten miteinander verbunden sind.

Die Verbindungen sind in folgender Grafik einmal dargestellt:

![Schematische Darstellung der Verbindungen auf einem 400 Pin Steckbrett](raumluftampel-assets/Breadboard_Leitungen.jpeg){ loading=lazy width="50%"}

Die aufgebaute Schaltung mit Sensor und LED-Anzeige sieht wie folgt aus:

![Schematische Darstellung des Aufbaus der Raumluftampel](raumluftampel-assets/Schaltung_Steckplatine.png){ loading=lazy width="85%"}

Die Farbe der Kabel spielt für die Funktion des Aufbaus keine Rolle und wurde hier nur der Übersicht halber so gewählt.

### Verbindungen

Die einzelnen Bauteile sind mit verschiedenen Kontakten am Arduino verbunden.

Die RGB-LED:

* Der Pin für die Farbe blau ist  beim Arduino mit dem Pin D9 verbunden

* Der Pin für die Farbe rot ist beim Arduino mit dem Pin D11 verbunden

* Der Pin für die Farbe grün ist beim Arduino mit dem Pin D10 verbunden

* Der Pin für den Kontakt am Minuspol (auch GND für Ground) ist über einen Umweg mit dem GND-Pin des Arduino verbunden

Der Gassensor MQ-135:

* Der VCC Pin für die Spannungeversorgung ist über einen Umwag mit dem 5V Pin des Arduino verbunden

* Der GND Pin (Ground oder Minuspol)  ist über einen Umweg mit dem GND-Pin des Arduino verbunden

* Der Pin A0, an dem der Widerstand/die Leitfähigkeit des Sensors gemessen wird ist mit dem Pin A0 des Arduino verbunden

Beide Elemente "teilen" sich einen GND-Pin am Arduino.

## Einsatz

Auf deinem Arduino läuft bereits ein Programm, welches die Güte der Umgebungsluft misst.

### Funktionalität

Das Programm startet sich, wenn der Arduino mit Spannung versorgt wird. Entweder man schließt den Arduino mit dem beiliegenden Kabel an einen Rechner an, oder an ein USB-Ladegerät.  
Bei erster Inbetriebnahme sollte man die [Einbrennphase](#sec:Material) des Sensors beachten.

#### Kalibrierung

Das auf dem Arduino laufende Programm kalibriert den Sensor erst einmal auf frische Luft. Damit die Kalibrierung erfolgreich abgeschlossen werden kann, muss die Schaltung erst einmal etwa 10 Minuten an der frischen Luft sein. Während der Kalibrierung leuchtet die LED blau (so lange sollte die Raumluftampel auch an der frischen Luft sein). Nach Abschluss der Kalibrierung wechselt die LED auf eine andere Farbe.

Während der Kalibrierung wird der Sensor auf Betriebstemperatur gebracht und der Messwert des Sensors als Wert für frische Umgebungsluft festgelegt.

#### Messung

Anschließend wird einmal pro Sekunde die Partikelbelastung in der Luft gemessen und der Wert über die LED ausgegeben:

* <span style="color:green">Grün: Gute Luft</span>

* <span style="color:orange">Orange: Mittlere Luft</span>

* <span style="color:red">Rot: Schlechte Luft</span>

Sollte der Messwert des Sensors unter den der vorher gemessenen frischen Luft fallen, so beginnt der Kalibrierungsvorgang erneut.

### Zuverlässigkeit

Durch die indirekte Messung kann es immer vorkommen, dass unser Messgerät keine zuverlässigen Daten liefert. Am stabilsten und zuverlässigsten arbeitet die Raumluftampel, wenn sie lange Zeit am Stück (mehrere Stunden bis Tage) läuft. Am besten lässt man sie dann noch einmal einige Minuten an frischer Luft kalibrieren, um möglichst zuverlässige Ergebnisse zu erhalten.

Unter diesen Umständen liefert der Sensor ähnliche Messwerte, wie ein spezifischer CO2-Sensor.

## Programmierung

### Arduino Microcontroller

Bevor man sich der tatsächlichen Programmierung zuwendet, sollten wir uns einmal klar machen, was unser ArduinoMicrocontroller eigentlich kann und aus welchen grundlegenden Bestandteilen er besteht:

* Analoge Eingänge: An den Kontakten A0-A7 kann der Arduino eine anliegende Spannung zwischen 0V und 5V messen.
* Ein-/Ausgänge: An den Kontakten D0-D13 kann der Arduino:
    * Unterscheiden ob eine Spannung größer ist als 3V (digitaler Eingang)
    * Eine Spannung von 0V oder 5V ausgeben (digitaler Ausgang)
    * Ein Signal ausgeben, das sehr schnell zwischen 0V und 5V wechselt, so dass man eine "virtuelle Spannung" zwischen 0V und 5V ausgeben kann (Pulsweitenmodulation an den Kontakten mit `~` D3,D5,D6,D9,D10,D11)
* Ein Microcontroller: hier findet die Signalverarbeitung aus den Eingängen statt und Befehle an die Ausgänge werden ausgeführt.

![Arduino Nano Pin-Belegung](raumluftampel-assets/ArduinoPins.png){ loading=lazy width="60%"}

Mit dem Arduino ist daher eine ganze Menge möglich. Am Ende lässt sich die Funktionalität immer auf das Auslesen von Spannungen an den Eingängen und das Ausgeben von Spannungen an den Ausgängen zurückführen.

### ArduBlock

Das Programm wurde in einer grafischen Programmiersprache (ArduBlock) geschrieben, um einfacher verständlich zu sein:

![Schematische Darstellung des Codes in ArduBlock](raumluftampel-assets/ArdublockAmpel.png){ loading=lazy width="100%"}

Allerdings nehmen Definitionen und Unterfunktionen sehr viel Raum ein. Dennoch lassen sich auch in der großen und nicht ganz übersichtlichen Grafik einige Dinge erkennen: Das Programm gliedert sich in verschiedene Bereiche, die farblich unterschiedlich markiert sind:

* Dunkles grün: Der sogennannte Programmkopf.
  Hier sind Eckdaten zum Programm aufgeführt, wie Programmname, Autor, Version und Datum. Der Microcontrolle hat hier nichts zu tun, diese Informationen sind zur Übersicht gedacht.

    ![Ardublock Darstellung des Programmkopfes](raumluftampel-assets/ArdublockAmpelProgrammkopf.png){ loading=lazy width="60%"}

* Gelb: Das eigentliche Programm.
  Hier arbeitet der Microcontroller generell alle Befehle von oben nach unten ab. Der gelbe Bereich gliedert sich wiederum in drei Bereiche  

    ![Ardublock Darstellung des Programmteils](raumluftampel-assets/ArdublockAmpelProgramm.png){ loading=lazy width="85%"}

    * Der Bereich vor dem Setup:
      Hier werden Dinge definiert, die für das ganze Programm von Nutzen sind. Bei unserer Raumluftampel zum Beispiel die Grenzwerte, ab denen die Luft als mittel oder schlecht angesehen wird.

    * Das Setup:
      Die Befehle hier werden vom Microcontroller nur einmal beim Start ausgeführt und dann nicht mehr.

    * Der Loop:
      Die Befehle hier, werden von oben nach unten abgearbeitet und dann beginnt der Microcontroller von vorn. So dass diese immer wieder ausgeführt werden.

* Helles Grün: Funktionen  
  Funktionen sind eine Sammlung von Befehlen (die wieder von oben nach unten abgearbeitet werden), welche sich von anderen Stellen im Programmcode ausführen lassen. Das ermöglicht, dass das eigentliche Programm übersichtlicher ist, da statt aller Befehle nur der Funktionsname aufgerufen wird.

    ![Ardublock Darstellung einer Funktion](raumluftampel-assets/ArdublockAmpelFunktion.png){ loading=lazy width="85%"}


### Übersicht Ardublock und Microcontroller

Eine hervorragende Übersicht über Grundlagen der Funktion eines Microcontrollers und der Bedienung mittels ArduBlock finden sich in den Begleitheften des letsgoING-Kurses der Hochschule Reutlingen:

1. [Microcontroller kennenlernen](raumluftampel-assets/1-Mikrocontroller_kennenlernen.pdf)
2. [Digitale Signale und Variablen](raumluftampel-assets/2-Digitale_Signale_und_Variablen.pdf)
3. [Analoge Signale und Variablen](raumluftampel-assets/3-Analoge_Signale_und_Variablen.pdf)

### Installation ArduinoIDE und ArduBlock

Um das auf dem Arduino lufende Programm den eigenen Bedürfnissen anzupassen, benötigt man ein Programm um einerseits den Code zu schreiben und andererseits das Programm auf den Arduino hochzuladen.

Eine Software mit der Code für den Arduino Microcontroller geschrieben und auf den Arduino hochgeladen werde kann ist die **Arduino IDE**. Sie kann hier heruntergeladen und anschließend installiert werden:  
[Arduino-IDE Download](https://www.arduino.cc/en/software)

Die Installation der grafischen Programmieroberfläche **ArduBlock** ist leider etwas umständlicher und beinhaltet folgende Schritte:

1. Installation der Arduino-IDE

2. Einmaliger Start der Arduino-IDE  
  Dabei erstellt die IDE in deinem Benutzerverzeichnis einen Ordner `Arduino`.

3. Erstelle in dem Arduino-Ordner in deinem Dokumentenverzeichnis folgende Ordnerstruktur:  
  `tools/ArduBlockTool/tool/` also im Ordner Arduino einen Ordner `tools` in diesem einen Ordner `ArduBlockTool` und in diesem wiederum einen Ordner `tool`.  
  [Bebilderte Anleitung für Windows](http://arduino-basics.com/ardublock/ardublock-herunterladen/)

4. Lade die ArduBlock-Datei herunter: [ArduBlock Download](https://github.com/letsgoING/ArduBlock2/raw/master/ArduBlockTool/tool/ardublock_letsgoing_21.jar)

5. Verschiebe die heruntergeladene `.jar`-Datei in den eben erstellten `tools`-Ordner.

Nun kannst du die ArduBlock-Umgebung starten, indem du in der ArduinoIDE zu Werkzeuge → ArduBlock navigierst.

![Ardublock aus der IDE starten](raumluftampel-assets/ardublockIDE.png){ loading=lazy}

### Auslesen der Sensorwerte der Raumluftampel:

Ist die Raumluftampel an einen Rechner angeschlossen, so kann man sich über den `Serial Monitor` die Messwerte ausgeben lassen. Die Raumluftampel gibt einen virtuellen CO2-Gehalt der Luft an, unter der Annahme, dass nur CO2 als Spurengas vorhanden ist.

Aufruf des Serial Monitors:

* Aus der Arduino IDE: Werkzeuge → Serieller Monitor (<kbd> Strg </kbd> + <kbd> Shift ↑ </kbd> + <kbd> M</kbd> )
* Aus ArduBlock: In der oberen Leiste <kbd> Klick </kbd> auf `Serieller Monitor`

### Upload eines Programms auf den Arduino

Der Arduino Microcontroller führt immer das zuletzt hochgeladene Programm aus. Um also Änderungen vorzunehmen, muss man das geänderte Programm auf den Arduino hochladen.  
Es gibt keine Möglichkeit sich anzeigen zu lassen, welcher Code gerade auf dem Arduino ausgeführt wird. Wir haben immer nur die Möglichkeit unseren aktuellen Code auf den Arduino hochzuladen, so dass dieser ausgeführt wird.

Generell geschieht das aus ArduBlock heraus. In der oberen Zeile ist ein Button `Hochladen auf den Arduino`. Nachdem dieser gedrückt wurde übergibt ArduBlock das Programm an die Arduino IDE. Diese baut aus dem generierten geschriebenen Code einen maschinenlesbaren Code (dieser Vorgang nennt sich kompilieren) und versucht dann den maschinenlesbaren Code auf den Arduino hochzuladen. Damit das gelingt müssen ein paar Voraussetzungen erfüllt sein:

* Der Port (`Werkzeuge → Port`) muss korrekt ausgewählt sein
* Das Board muss korrekt ausgewählt sein: `Werkzeuge → Board → Arduino Nano`
* Unter Prozessor muss der alte Bootloader ausgewählt sein: `Werkzeuge → Prozessor → ATmega328P (old bootloader)`

Nach diesen Vorbereitungen sollte es möglich sein, den mit ArduBlock geschriebenen grafischen Code auf den Arduino hochzuladen, ohne eine weitere Fehlermeldung angezeigt zu bekommen.



## Quellen und weiterführende Links:
Hier sind in ungeordnerter Reihenfolge Quellen aufgelistet, die Vorbild für dieses Projekt waren, oder die einfach weiterführende Informationen bieten, teilweise sind die Informationen nur auf Englisch verfügbar. Die Links verweisen alle auf das Web-Archive um langfristig auf denselben Inhalt zu verweisen.

* Code der auf dem Microcontroller läuft:  
  [Als ArduBlock-Datei](raumluftampel-assets/ArdublockAmpel.abp){ download="ArdublockAmpel.apb" }  
  [Als Datei für die Arduino-Entwicklungsumgebung](raumluftampel-assets/CO2-Ampel-deutsch.ino){ download="CO2-Ampel-deutsch.ino" }

* Raumluft/CO2-Sensor auf Basis des MQ-135  
  [NEOE CO2-Sensor](https://web.archive.org/web/20201204112059/https://webcache.googleusercontent.com/search?q=cache%3A41bFs1AU9p4J%3Ahttps%3A%2F%2Fwww.neoe.io%2Fblogs%2Ftutorials%2Fluftqualitatssensor-reagiert-auf-co2-mqtt-kompatibel-aufbau-variante-breadboard+&cd=1&hl=de&ct=clnk&gl=de)

* Datenblätter der Komponenten:  
  sind oben bereits aufgeführt.  
  [Ausführlicheres Datenblatt des Sensors mit Langzeitmessung](https://web.archive.org/web/20201107152733/https://www.winsen-sensor.com/d/files/PDF/Semiconductor%20Gas%20Sensor/MQ135%20(Ver1.4)%20-%20Manual.pdf)

* Beschreibung einer Bibliothek zur Auswertung des MQ-135 Sensors. Da kann man etwas über das Messprinzip und die Auswertung lernen.  
  [Beschreibung](https://web.archive.org/web/20200924123418/https://hackaday.io/project/3475-sniffing-trinket/log/12363-mq135-arduino-library)  
  [Bibliothek](https://github.com/GeorgK/MQ135)

* Wissenschaftlicher [Überblicksartikel](https://doi.org/10.1524/teme.1985.52.2.59) über Gassensoren auf Basis von Metalloxid-Halbleitern

* [Projekt](https://web.archive.org/web/20210121101506/https://davidegironi.blogspot.com/2014/01/cheap-co2-meter-using-mq135-sensor-with.html), bei dem die Messwerte des MQ-135 mit einem CO2-Sensor verglichen wurden.
