---
title: FreeCAD - FAQ
description: Antworten auf häufige Fragen zur Nutzung von FreeCAD
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# FreeCAD - FAQ

## Gibt es ein universelles Maßband/Metermaß?

Ja, das gibt es. Das sollte in jedem Arbeitsbereich angezeigt werden. Es befindet sich in der Ansicht-Symbolleiste:

![FreeCAD Maßband in der Ansicht Werkzeugleiste](FreeCAD-FAQ-assets/FreeCAD-Massband.png)

Mit dem Maßband wird die Strecke zwischen zwei beliebigen Punkten im Objekt gemessen.  
Häufig kann man nicht perfekt erkennen, was auf dem Maßband steht, dann muss man das Objekt drehen, damit das Maßband über dem Objekt schwebt.

## Größe der Markerpunkte ändern {#sec:Markergroesse}

Die Markerpunkte sind die Anfangs-, End- und Eckpunkte bei Skizzen. Wenn diese etwas größer sind, geht das Konstruieren oft leichter von der Hand.

Die Größe lässt sich in den FreeCAD-Einstellungen ändern:
`Bearbeiten` → `Einstellungen` → `Anzeige`

Unter `Markergröße` kannt du nun die Größe der Punkte angeben, praktisch sind zB. `13px`.

## Die Maus macht nicht das, was sie soll (Navigationsstil)

FreeCAD verfügt über viele Navigationsstile, bei denen das Verhalten unterschiedlicher großer CAD-Softwares imitiert werden kann. Unser Standard-Navigationsstil ist `Gesture`.

Den Navigationsstil kann mit einem <kbd>Rechtsklick</kbd> in der Zeichenebene unter Navigationsstil auswählen:

![FreeCAD Navigationsstil auswählen](FreeCAD-FAQ-assets/FreeCAS-Navigationsstil.png){ loading=lazy width="40%"}


## Ich möchte eine Zeichnung zum Fräsen oder Schneiden exportieren {#sec:DXFExport}

Am Kepi in Tübingen stehen zum Fräsen von Holz KOSY-Fräsen zur Verfügung. Diese werden über die Software nccad angeseteuert.

Nccad kann 2D-Zeichnungen im `.DXF`-Format lesen. Praktischerweise kann FreeCAD genau das auch exportieren. Um also Holz von deiner Lehrkraft passend zuschneiden zu lassen gehst du so vor.

1. Erstelle eine Zeichnung (Arbeitsbereich `Sketcher`) von deinem Bauteil.

    1. Diese Zeichnung soll alle Elemente enthalten: Umriss, Aussparungen, Bohrungen, etc.
    2. Die Zeichnung muss vollständig eingeschränkt sein.

    ![Eingeschränkter Sketch zum Exportieren](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Sketch.png){ loading=lazy width="65%"}

2. Schließe die Zeichnung und wähle sie unter `Modell` in der `Combo-Ansicht` aus.

    ![Auswahl der Skizze in der Combo-Ansicht](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Select.png){ loading=lazy width="65%"}

3. Wähle im Menü `Datei` den Punkt `Exportieren` und achte darauf, dass im folgenden Fenster als Format `Autodesk DXF 2D (*.dxf)` ausgewählt ist und exportiere die Zeichnung an den gewünschten Ort.

    ![Richtiges Format zum Export auswählen](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Format.png){ loading=lazy width="65%"}

4. Übergib die Datei an deine Lehrkraft und lasse deine Skizze fräsen.


## Mein Bauteil zum Fräsen/Schneiden basiert nicht nur auf einer einzelnen Skizze.

Wenn dein Bauteil zu komplex ist, um die obigen Schritte auszuführen, weil du nicht nur eine Skizze extrudiert hast, dann ist der Weg etwas umständlicher.

![Komplexeres Bauteil, das nicht nur auf einer Skizze aufbaut](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Shapebinder1.png){ loading=lazy width="45%"}

1. Wechsle in den `Draft`-Arbeitsbereich

     ![Draft Arbeitsbereich](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Shapebinder2.png){ loading=lazy width="35%"}

2. Wähle die Oberfläche, die du exportieren möchtest mit <kbd>Mausklick</kbd> an.

    ![Oberfläche auswählen](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Shapebinder3.png){ loading=lazy width="35%"}

3. Klicke in der Werkzeugleiste auf das Symbol für <kbd>Flächenverbinder</kbd>.

    ![Flächenverbinder Symbol](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Shapebinder4.png){ loading=lazy width="35%"}

4. In der Baumansicht (links) gibt es nun ein Element `Facebinder`, dieses markierst du durch <kbd>Klick</kbd> und exportierst es als `.DXF`, wie im [letzten Abschnitt](#sec:DXFExport)

    ![Shapebinder in der Kombo-Ansicht](FreeCAD-FAQ-assets/FreeCAD-DXF-Export-Shapebinder5.png){ loading=lazy width="35%"}

## Ich möchte mein Bauteil 3D-Drucken {#sec:stlExport}

**Kurz:**

* Markiere den zu druckenden Körper in Baumansicht.

    ![Markierter Körper in FreeCAD](FreeCAD-FAQ-assets/FreeCAD-stl-Export-select.png){ loading=lazy width="35%"}

* Wähle im Menü unter `Datei` den Punkt `Exportieren`

* Wähle als zu exportierendes Format `STL Mesh (*.stl *.ast)`.

    ![Format für stl-Export auswählen](FreeCAD-FAQ-assets/FreeCAD-stl-Export-format.png){ loading=lazy width="60%"}

* Übergib die `.stl`-Datei deiner Lehrkraft, damit die sich um den Rest kümmert und nimm später das gedruckte 3D-Teil entgegen.

**Lang:**

Häufig möchte man das sorgfältig konstruierte Bauteil am 3D-Drucker fertigen. Dummerweise versteht der 3D-Drucker keine `*.FCStd`-Dateien.

Der 3D-Drucker versteht nur sogenannten G-Code. In G-Code Dateien stehen größtenteils einfach die Positionen, die der Kopf des 3D-Druckers anfahren soll. Um das Bauteil also in solchen G-Code zu übersetzen brauchen wir eine Software, die die 3D-Informationen des Bauteils liest und diese in sinnvolle Bahnen des Druckkopfes übersetzt.

Da die Drucker am Kepi von Ultimaker sind, wird die Software Ultimaker Cura verwendet. Hier lassen sich 3D Objekte im `*.stl`-Format importieren und entsprechender G-Code ausgeben, der dann gedruckt werden kann.
