---
title: fritzing - Schaltungsplanung
description: Schaltungsplanung, -dokumentation und Leiterplatinendesign mit fritzing
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Schaltungsplanung mit fritzing

[fritzing](https://fritzing.org) ist eine simple Software zur Planung und Darstellung von elektrischen Schaltungen. Hier wird gezeigt, wie man vom ersten Schaltungsentwurf bis zur geplanten Platine vorgeht.

fritzing ist zwar eine Open-Source-Software, allerdings werden die ausführbaren Dateien verkauft, um die Weiterentwicklung von fritzing und den Betrieb der Webseite zu finanzieren. Schülerinnen und Schüler der Kepler-Gymnasiums können sich an [mich](mailto:greiner@kepi.de) wenden, um die Software zu Hause zu nutzen.


Als Beispiel soll hier eine Platine zur Messung der Temperatur mit einem Temperaturabhängigen Widerstand, NTC.  
Dazu wird der NTC mit einem Vergleichswiderstand in Reihe geschaltet und zwischen NTC und Vergleichswiderstand wird die Spannung gemessen. Diese Schaltung nennt man *Spannungsteilerschaltung*.



![NTC-Spannungsteilerschaltung zur Temperaturmessung](fritzing-assets/spannungsteiler-NTC.png){ loading=lazy width="40%"}

## Erster Start

Beim ersten Start von fritzing wird man mit diesem Fenster begrüßt:

![fritzing Startfenster](fritzing-assets/fritzing-startfenster.jpg){ loading=lazy width="85%"}

Das Fenster von fritzing teilt sich in jedem Arbeitsschritt in 5 Bereiche auf.

1. Menüleiste  
    Hier finden sich alle Werkzeuge, die sich nicht direkt in den anderen Bereichen wiederfinden.
2. Ansichtenumschalter  
    Hier kann man zwischen den verschiedenen Ansichten in fritzing umschalten. Die Ansichten sind in der Abfolge des Platinendesigns geordnet:
    - Steckplatine: Zuerst die Schaltung [planen](#sec:SchaltungPlanen)
    - Schaltplan: Einen [Schaltplan](#sec:SchaltplanErstellen) der Schaltung erstellen
    - Leiterplatte: Die Schaltung auf einer [Leiterplatte](#sec:PlatineUmsetzen) umsetzen
3. Projekt-Fenster  
    Hier wird die Schaltung in Abhängigkeit von der gewählten Ansicht dargestellt.
4. Bibliothek  
    Alle verfügbaren Bauteile, die in die Schaltung aufgenommen werden können.
5. Inspektor  
    Eigenschaften des ausgewählten Bauteils.


## Schaltung planen {#sec:SchaltungPlanen}

Der erste Schritt auf dem Weg zu einer Schaltung auf einer Leiterplatte ist, die Schaltung erst einmal vernünftig zu planen.

In fritzing geschieht das in der `Steckplatinen-Ansicht` ②. Hier kann die Schaltung so geplant werden, als würdest du sie mit einer Steckplatine tatsächlich aufbauen.

![Leere Steckplatinen-Ansicht](fritzing-assets/fritzing-steckplatine.jpg){ loading=lazy width="50%"}

In der Abbildung ist die Steckplatine ausgewählt und man kann im Inspektor ⑤ sehen, dass als Größe `half+`gewählt wurde.

In dieser Ansicht lassen sich nun die Bauteile aus der Bibliothek ④ mittels Drag-n-Drop um die Steckplatine positionieren.  
Die Kontakte der Bauteile lassen durch <kbd>Linksklick</kbd>+<kbd>Ziehen</kbd> verbinden.

Für einen sinnvollen und möglichst übersichtlichen Plan sollte man einige Dinge beachten:

- Nutze die richtige Bauteilform (im Inspektor ⑤), sonst hat man am Ende auf der Leiterplatte den falschen Bohrdurchmesser oder falsche Abstände.
- Setze die Eigenschaften der Bauteile richtig im Inspektor ⑤.
- Für eine bessere Übersicht kann man die Drähte (`Wire`) mit unterschiedlichen Farben versehen.
- Bei den Verbindungsdrähten sollte man mit rechten Winkeln (notfalls auch 45°) arbeiten.  
    Ein <kbd>Klick</kbd> in die Mitte einer Verbindung fügr einen Biegepunkt ein.  
    Mit <kdb>Shift</kbd>+<kbd>Ziehen</kbd> rastet die Verbindung in 45°-Schritten ein.
- Um die Bauteile zu erkennen ist es sinnvoll die Bauteilbeschriftung anzeigen zu lassen.  
    <kbd>Rechtsklick</kbd> auf das Bauteil und `Bauteilbeschriftung anzeigen`.
- Beuteilbeschriftungen lassen sich mit einem Doppelklick anpassen.
- Ein <kbd>Klick</kbd> auf eine Verbindung hebt alle Stellen hervor, die mit diesem Punkt verbunden sind.
- Bei Bauteilen mit biegbaren Beinchen (zum Beispiel Widerstände) lassen sich die Enden der Beinchen an die richtigen Stellen ziehen.
- Manche Bauteile haben keine Zeichnung für die Steckplatine und können daher nicht hinzugefügt werden.

Unser Beispiel (Messung und Regelung der Temperatur mit einer Lampe) könnte in der Steckplatinen-Ansicht so aussehen:

![Beispiel einer Steckplatine in Fritzing](fritzing-assets/fritzing-steckplatine-beispiel.jpg){ loading=lazy width="80%"}

Teilweise passen die Bauteile nicht perfekt, wie im Beispiel die Spannungsquelle und die Lampe.


## Schaltplan erstellen {#sec:SchaltplanErstellen}

Im nächsten Schritt wechselt man in der Ansichtenumschalter ② auf `Schaltplan`. Hier sieht man direkt alle Bauteile, die man eben  eingefügt hat allerdings als genormte Schaltungssymbole. Die Verbindungen sind nocht nicht ausgeführt sondern als sogenannte Air-Wires dargestellt.  

![fritzing Schaltplan-Ansicht ungeroutet mit Air-Wires](fritzing-assets/fritzing-schaltplan.jpg){ loading=lazy width="80%"}

In diesem Schritt geht es nun darum, die Bauteile sinnvoll zu positionieren und zu drehen, um anschließend die Verbindungen so auszuführen, dass der Plan übersichtlich ist. Häufig ist das mit Probieren verbunden und kann, gerade bei komplizierteren Schalplänen lange dauern.

- Verbindungen werden durch <kbd>Linksklick</kbd>+<kbd>Ziehen</kbd> zwischen den zu Verbindenden Enden erstellt.
- Einrasten der Verbindungen im 45°-Winkel mit <kbd>Shift</kbd>+<kbd>Ziehen</kbd>.
- Biegepunkte in Verbindungen lassen durch ziehen in der Mitte erzeugen.
- An Biegepunkte lassen sich auch Verbindungen von anderen Bauteilen anschließen.
- Bauteile lassen sich durch <kbd>Klicken</kbd>+<kbd>Ziehen</kbd> in ihrer Mitte verschieen.
- Bauteile lassen sich über das Kontextmenü (<kbd>Rechtsklick</kbd>) drehen.
- Auch hier sollten sinnvolle Bauteilbeschriftungen stehen, diese lassen sich durch <kbd>Ziehen</kbd> verschieben.

Wenn alle Verbindungen erstellt sind, erkennt das auch fritzing sodass in der unteren roten Leiste `Routing fertiggestellt`steht.

![Fertig gerouteter Schaltplan](fritzing-assets/fritzing-schaltplan-beispiel.jpg){ loading=lazy width="80%"}

An dieser Stelle ist die Dokumentation der Schaltung abgeschlossen. Im kommenden Schritt geht es noch um die Umsetzung auf einer Leiterplatte.



## Platine planen {#sec:PlatineUmsetzen}

Im letzten Schritt geht es darum die Schaltung nicht nur als Prototyp auf einem Steckbrett zu verwirklichen, sondern die Bauteile auf einer Leiterplatte zu montieren, sodass die Schaltung deutlich robuster ist.

Beim Wechsel in die Leiterplatten-Ansicht sieht unsere Schaltung so aus.

![fritzing rohe Leiterplatten-Ansicht](fritzing-assets/fritzing-leiterplatte.jpg){ loading=lazy width="80%"}

Man kann die Leiterplatte in grau erkennen, sowie die sogenannten *footprints* der elektronischen Bauteile (im Beispiel Arduino und zwei Widerstände). Außerdem sind die Verbindungen wieder als Air-Wires dargestellt.

Unsere Schaltung zur Temperaturmessung soll als Shield auf dem Arduino aufgebaut werden können, sodass die Leiterplatte sich später auf den Arduino aufstecken lässt.  
Der NTC soll nicht auf dem Shield sein, sondern mit Kabeln angeschlossen, sodass wir die Temperatur auch an anderen Orten, als direkt am Arduino messen können.

Für ein erfolgreiches Platinendesign ergeben sich somit folgende Schritte:

1. Braucht es zusätzliche Verbinder, da Bauteile außerhalb der Platine liegen?  
    Typisch wären Sensoren oder eine Spannungsversorgung.  
    Typische Verbinder findet man in der `Core`-Bibliothek unter `Verbindung`.
2. Abmessungen und Form der Leiterplatte festlegen.  
    Leiterplatte auswählen und im `Inspektor` ⑤ die entsprechende Eigenschaft festlegen.
3. Leiterbahnen ziehen  
    Im 45°-Winkel durch Halten der <kbd>Shift</kbd>-Taste.  
    Sind externe Bauteile in der Schaltung verbindet man nur die Verbindungsstücke mit Leiterbahnen. Es bleiben Air-Wires an den externen Bauteilen.
4. Leiterbahnen dicker machen  
    Oft ist es sinnvoll, die Leiterbahnen dicker auszuführen.  
    In der `Menü-Leiste` ① unter `Routing` `Alle Leiterbahnen auswählen`.  
    Im `Inspektor` ⑤ als Breite `dick` oder `extra dick`auswählen.
5. Design Rule Check (DRC)
    Du solltest abschließend deinen Entwurf einem Prüfung unterziehen, ob sich Leiterbahnen überkreuzen, zu dünn sind, oder zu nah beieinander liegen. Das geht mit dem DRC.  
    Aufruf über `Routing` in ① → `Entwurfsregelprüfung (DRC)`.  
    Einstellungen über `Routing` in ① → `Autorouter/DRC Einstellungen` → `benutzerdefiniert`.

Nach einem erfolgreichen DRC könnte im Beispiel das Shield so aussehen.

![fritzing Bild eines einfachen Arduino Shields](fritzing-assets/fritzing-leiterplatte-shield.jpg){ loading=lazy width="80%"}

Dieses Shield könnte man in die Fertigung geben.

Komplexere Schaltungen erfordern oft einiges an Zeit und geschickter Positionierung/Planung. Ein paar Tipps für kompliziertere Schaltungen:

- Manchmal sind an mehreren Stellen Kontakte mit der gleichen Funktion.  
    Der Arduino hat verschiedene GND-Kontakte, sodass sich die passenden auswählen lassen.
- Ein Widerstand lässt sich nutzen, um über eine Leiterbahn zu *springen*. So lässt sich unter einem Widerstand eine andere Leiterbahn durchführen.
- Es lässt sich Arbeit sparen, indem man die gesamte Platte mit GND verbindet, das nennt sich *Massefüllung/Ground Fill*.  
    So muss man für GND keine zusätzlichen Verbindungen ziehen, solange kein GND-Kontakt von anderen Leiterbahnen umzingelt ist.
    1. Bei Verbindern zu externen Bauteilen müssen die Elemente, die mit der Massefüllung verbunden sein sollen als `Saat für die Massefüllung` gesetzt werden.
    2. Unter `Routing` in ① → `Massefüllung` → `Saat für Massefüllung wählen` alle Elemente wählen, die mit der Massefüllung verbunden sein sollen.
    3. Unter `Routing` in ① → `Massefüllung` → `Massefüllung (Oben und unten)` wählen.
    4. Überprüfen, ob alle Elemente, die mit GND verbunden sein sollen auch tatsächlich mit der Massefüllung verbunden sind.
    5. Aushalten, dass fritzing die Massefüllung nicht sehr gut implementiert hat, sodass es jetzt zusätzliche Air-Wires zwischen den GND-Kontakten geben kann.
- Sollte die Schaltung zu kompliziert werden, lassen sich Leiterbahnen auf der Ober- und Unterseite der Platte führen.

![fritzing Bild eines einfachen Arduino Shields mit Massefüllung](fritzing-assets/fritzing-leiterplatte-shield-fill.jpg){ loading=lazy width="80%"}

### Die anderen Ansichten

Wenn du externe Bauteile mit Verbindern auf der Platine angebracht hast, werden diese Verbinder nun in deinen anderen Ansichten (Steckplatine und Schaltplan) wieder auftauchen und mit Air-Wires verbunden sein.

Das ist ärgerlich aber leider unvermeidlich. 


## Platine exportieren

Um die Platine zu fertigen exportiert man die Leiterplatte als Extended Gerber.

`Datei` in ① → `Exportieren` → `für die Produktion` → `Extended Gerber (RS-274X)`.

Die weiteren Schritte zur fertigen Platine sind in [PCB](PCB.md) beschrieben.


## Referenzen

Eine weitere ausführliche Anleitung findet sich hier:  
[AZ-Delivery HowTo Fritzing](https://www.az-delivery.de/blogs/azdelivery-blog-fur-arduino-und-raspberry-pi/how-to-fritzing-teil-1)