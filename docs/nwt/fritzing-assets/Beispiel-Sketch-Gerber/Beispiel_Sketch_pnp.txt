*Pick And Place List
*Company=
*Author=
*eMail=
*
*Project=Beispiel_Sketch
*Date=15:47:58
*CreatedBy=Fritzing 0.9.10b.2022-05-14.CD-1801-0-29c2cede
*
*
*Coordinates in mm, always center of component
*Origin 0/0=Lower left corner of PCB
*Rotation in degree (0-360, math. pos.)
*
*No;Value;Package;X;Y;Rotation;Side;Name
1;;;40.8305;-24.637;0;Bottom;Copper Fill78
2;;;32.9692;-51.7261;0;Bottom;Copper Fill7
3;;;37.0586;-1.73888;0;Bottom;Copper Fill61
4;;;40.8305;-24.637;0;Bottom;Copper Fill38
5;;;29.4386;-1.73888;0;Bottom;Copper Fill18
6;;;40.8178;-1.77698;0;Bottom;Copper Fill36
7;;;40.5892;-51.7261;0;Bottom;Copper Fill49
8;;;44.6786;-1.73888;0;Bottom;Copper Fill24
9;;;52.2732;-51.7261;0;Bottom;Copper Fill52
10;;;62.4332;-51.7261;0;Bottom;Copper Fill17
11;;;44.6786;-1.73888;0;Bottom;Copper Fill64
12;;;59.8932;-51.7261;0;Bottom;Copper Fill16
13;;;27.8892;-51.7261;0;Bottom;Copper Fill44
14;;;38.0492;-51.7261;0;Bottom;Copper Fill48
15;;;54.8386;-1.73888;0;Bottom;Copper Fill66
16;;;22.8092;-51.7261;0;Bottom;Copper Fill42
17;;;31.9786;-1.73888;0;Bottom;Copper Fill59
18;;;62.4586;-1.73888;0;Bottom;Copper Fill69
19;;;40.8178;-1.77698;0;Bottom;Copper Fill76
20;;;47.1932;-51.7261;0;Bottom;Copper Fill11
21;;;39.5986;-1.73888;0;Bottom;Copper Fill62
22;;;34.4678;-26.7833;0;Bottom;Copper Fill1
23;;;40.8305;-26.3134;0;Bottom;Copper Fill37
24;;;39.9415;-25.4498;0;Bottom;Copper Fill79
25;;;26.5938;-50.0243;0;Bottom;Copper Fill34
26;;;54.8386;-1.73888;0;Bottom;Copper Fill26
27;;;34.5186;-1.73888;0;Bottom;Copper Fill20
28;;;59.8932;-51.7261;0;Bottom;Copper Fill55
29;;;32.9692;-51.7261;0;Bottom;Copper Fill46
30;;;43.3578;-3.45338;0;Bottom;Copper Fill71
31;;;49.7332;-51.7261;0;Bottom;Copper Fill12
32;;;43.3578;-1.77698;0;Bottom;Copper Fill72
33;;;34.5186;-1.73888;0;Bottom;Copper Fill60
34;;;42.1386;-1.73888;0;Bottom;Copper Fill23
35;;;57.3786;-1.73888;0;Bottom;Copper Fill27
36;;;30.4292;-51.7261;0;Bottom;Copper Fill6
37;;;43.3578;-1.77698;0;Bottom;Copper Fill32
38;;;31.9786;-1.73888;0;Bottom;Copper Fill19
39;;;27.8892;-51.7261;0;Bottom;Copper Fill5
40;;;42.1386;-1.73888;0;Bottom;Copper Fill63
41;;;41.6433;-25.4498;0;Bottom;Copper Fill40
42;;;34.4678;-26.7833;0;Bottom;Copper Fill57
43;10k;THT;45.9281;-25.4205;0;Bottom;R2
44;;;34.467;-26.6905;0;Bottom;Bauteil1
45;;;62.4586;-1.73888;0;Bottom;Copper Fill29
46;;;41.6433;-25.4498;0;Bottom;Copper Fill80
47;;;37.0586;-1.73888;0;Bottom;Copper Fill21
48;;;40.8178;-3.45338;0;Bottom;Copper Fill35
49;;;59.9186;-1.73888;0;Bottom;Copper Fill28
50;;;22.8092;-51.7261;0;Bottom;Copper Fill3
51;;;57.3786;-1.73888;0;Bottom;Copper Fill67
52;;;61.1378;-22.0462;0;Bottom;Copper Fill30
53;;;40.8305;-26.3134;0;Bottom;Copper Fill77
54;;;25.3492;-51.7261;0;Bottom;Copper Fill4
55;;;35.5092;-51.7261;0;Bottom;Copper Fill47
56;;;52.2986;-1.73888;0;Bottom;Copper Fill25
57;;;43.3578;-3.45338;0;Bottom;Copper Fill31
58;;;61.1378;-22.0462;0;Bottom;Copper Fill70
59;;;59.9186;-1.73888;0;Bottom;Copper Fill68
60;;;47.1932;-51.7261;0;Bottom;Copper Fill50
61;;;52.2732;-51.7261;0;Bottom;Copper Fill13
62;;;62.4332;-51.7261;0;Bottom;Copper Fill56
63;;;26.5938;-50.0243;0;Bottom;Copper Fill74
64;;;40.8178;-3.45338;0;Bottom;Copper Fill75
65;;;26.5938;-51.7007;0;Bottom;Copper Fill33
66;;;20.2692;-51.7261;0;Bottom;Copper Fill41
67;;;39.5986;-1.73888;0;Bottom;Copper Fill22
68;;THT;61.1681;-24.1505;0;Bottom;J1
69;;;35.5092;-51.7261;0;Bottom;Copper Fill8
70;;;25.3492;-51.7261;0;Bottom;Copper Fill43
71;;;29.4386;-1.73888;0;Bottom;Copper Fill58
72;;;26.5938;-51.7007;0;Bottom;Copper Fill73
73;;;54.8132;-51.7261;0;Bottom;Copper Fill14
74;;;30.4292;-51.7261;0;Bottom;Copper Fill45
75;;;52.2986;-1.73888;0;Bottom;Copper Fill65
76;;;20.2692;-51.7261;0;Bottom;Copper Fill2
77;;;38.0492;-51.7261;0;Bottom;Copper Fill9
78;;;39.9415;-25.4498;0;Bottom;Copper Fill39
79;;;49.7332;-51.7261;0;Bottom;Copper Fill51
80;;;57.3532;-51.7261;0;Bottom;Copper Fill15
81;;;54.8132;-51.7261;0;Bottom;Copper Fill53
82;;;40.5892;-51.7261;0;Bottom;Copper Fill10
83;;;57.3532;-51.7261;0;Bottom;Copper Fill54
