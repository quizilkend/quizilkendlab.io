---
title: FreeCAD Kurs 1
description: Erste Konstruktionen, Technische Zeichnungen
author: Jürgen Gräber, Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# FreeCAD Kurs 1

In diesem Kursteil wird eine grundsätzliche Konstruktionsmethode für CAD-Software vorgestellt, sowie FreeCAD vorgestellt und erste Schritte mit der Software unternommen.

## Arbeiten mit einer CAD-Software

Eine gängige Vorgehensweise zur Konstruktion von dreidimensionalen Körpern mithilfe einer CAD-Software ist Konstruieren mit Hilfe von Skizzen.

* Eine Grundskizze wird erzeugt:

    ![Grundskizze erzeugen](FreeCAD1-assets/image12.png){ loading=lazy width="25%"}

* Die Skizze wird zu einem 3D-Körper aufgepolstert:

    ![Skizze zu einem Körper aufpolstern](FreeCAD1-assets/image13.png){ loading=lazy width="25%"}

* Auf einer Fläche des Körpers (hier Oberseite) wird eine weitere Skizze erstellt und aufgepolstert:

    ![Skizze auf Oberseite erstellen und aufpolstern](FreeCAD1-assets/image14.png){ loading=lazy width="25%"}

* Auf einer der Vorderflächen wird eine weitere Skizze erstellt und eine Vertiefung erzeugt:

    ![Skizze auf vorderer Fläche und Vertiefung erzeugen](FreeCAD1-assets/image15.png){ loading=lazy width="25%"}

Hierbei ist das Vorgehen immer gleich: Eine Skizze wird in einer Ebene erstellt und aus dieser Skizze wird ein 3D-Objekt erstellt.  
Für die erste Skizze stehen die XY-, XZ- und die YZ-Ebene zur Verfügung. Für weitere Skizzen kann zusätzlich jede Oberfläche des Körpers als Skizzenebene gewählt werden.

![Ansicht einer Skizze in den Basis-Ebenen](FreeCAD1-assets/image16.png){ loading=lazy width="40%"}
![Ansicht des 3D-Körpers in den Basis-Ebenen](FreeCAD1-assets/image17.png){ loading=lazy width="40%"}


## Einteilung des Arbeitsbildschirms von FreeCAD

![Arbeitsbereich von FreeCAD](FreeCAD1-assets/ArbeitsbereichKommentiert.png){ loading=lazy width="80%"}

1. Hauptansicht:  
Hier siehst du den zu bearbeitenden Körper und führst alle Konstruktionsschritte durch.

2. Ansichtenwürfel:  
Drehst du den Ansichtenwürfel, dreht sich auch dein Körper.

3. Geöffnete Dateien:  
Hier siehst du in Tabs die geöffneten Dateien..

4. Arbeitsbereiche:  
Für unterschiedliche Aufgaben gibt es unterschiedliche Arbeitsbereiche, zum Beispiel für das erstellen von Skizzen, technischen Zeichnungen oder 3D-Körpern.

5. Befehle:  
Hier werden die Befehle als Symbole angezeigt, die im aktuellen Arbeitsbereich zur Verfügung stehen.

6. Baumansicht:  
Hier kannst du jeden Konstruktionsschritt sehen.  
Durch Markieren und Betätigen der Leertaste können einzelne Konstruktionsschritte ein- oder ausgeblendet werden.  
Es lohnt sich die Schritte sinnvoll zu benennen, um bei komplizierten Körpern den Überblick zu behalten.

7. Eigenschaften:  
Hier kannst du alle wichtigen Eigenschaften zum markierten Objekt sehen und verändern.  
Das Eigenschaften-Fenster lässt sich zwischen `Ansicht` und `Daten` umschalten.

8. Aufgaben Reiter in der Combo-Ansicht:  
Je nach Arbeitsbereich und Arbeitsschritt werden hier Aktionen oder Informationen angezeigt.

## Grundeinstellung/Abkürzungen

1. Navigationsstil  
Um später geschickter mit der Maus arbeiten zu können, empfiehlt es sich den Navigationsstil `Gesture` zu wählen. Dazu wählst du nach einem <kbd>Rechtsklick</kbd> in der Hauptansicht ① `Navigationsstile` → `Gesture`.

    ![Maustestenbelegung beim Navigationsstil `Gesture`](FreeCAD1-assets/NavigationsstileMaus.png){ loading=lazy width="85%"}

2. Arbeitsbereich: Part Design  
Im Arbeitsbereich `Part Design` sollten diese Symbolleisten sichtbar sein (<kbd>Rechtsklick</kbd> in eine Leiste):

    ![Symbolleisten im Arbeitsbereich Part Design](FreeCAD1-assets/PartDesignSymbolleisten.png){ loading=lazy width="80%"}

3. Arbeitsbereich: Sketcher  
Im Arbeitsbereich `Sketcher` sollten diese Symbolleisten sichtbar sein (<kbd>Rechtsklick</kbd> in eine Leiste):

    ![Symbolleisten im Arbeitsbereich Sketcher](FreeCAD1-assets/SketcherSymbolleisten.png){ loading=lazy width="80%"}

4. Größe der Markerpunkte  
Die Markerpunkte sind sind die Anfangs-, End- und Eckpunkte bei Skizzen. Wenn diese ein bisschen größer sind, geht das Konstruieren oft leichter von der Hand. Du kannst die Größe in den Einstellungen (`Bearbeiten/Einstellungen`) ändern:

    ![Markergröße in FreeCAD Einstellungen ändern](FreeCAD1-assets/Markergroesse.png){ loading=lazy width="60%"}


5. Maustasten und Tastendruck  
Eingaben mit der Maus und mit der Tastatur werden in dieser Anleitung so hervorgehoben:  
    * <kbd>Klick</kbd>: Druck auf die linke Maustaste
    * <kbd>Rechtsklick</kbd>: Druck auf die rechte Maustaste
    * <kbd>Strg</kbd>+<kbd>C</kbd>: Gleichzeitiges Drücken der Tasten `Strg` und `C`
    * <kbd>OK</kbd>, <kbd>Schließen</kbd>: Mausklick auf `OK`, `Schließen`
    * <kbd>![PartDesign Arbeitsbereich Logo](FreeCAD1-assets/Workbench_PartDesign.svg){ .twemoji height=1em}</kbd>: Klick auf das Symbol ![PartDesign Arbeitsbereich Logo](FreeCAD1-assets/Workbench_PartDesign.svg){ .twemoji height=1em}
    * ...

## 1. Grundkörper

### 1.0 Darum geht's

Einen ersten, einfachen Körper erstellen.

* Erstellen einer Skizze,
* Eine Skizze aufpolstern
* Eine Tasche erzeugen
* Größe der Markerpunkte ändern

Vorgehen Schritt für Schritt:

### 1.1 Skizzenebene auswählen

* Beginne ein neues Dokument: <kbd>![Neues Dokument](FreeCAD1-assets/Std_New.svg){ .twemoji height=1em}</kbd>
* Wähle den Arbeitsbereich `Part Design` unter ④ ![PartDesign Arbeitsbereich Logo](FreeCAD1-assets/Workbench_PartDesign.svg){ .twemoji height=1em}
* Erstelle eine neue Skizze mit  <kbd>![Symbol neue Skizze erstellen](FreeCAD1-assets/Sketcher_NewSketch.svg){ .twemoji height=1em}</kbd> unter ⑤.
* Wähle unter ⑧ die XY-Ebene `XY_Plane` aus und bestätige mit <kbd>OK</kbd>.  
FreeCAD wechselt automatisch in den Arbeitsbereich `Sketcher` und die Skizzenbefehle werden unter ⑤ sichtbar:

![Skizzenbefehle in der Werkzeugleiste](FreeCAD1-assets/Sketchertools.png){ loading=lazy width="75%"}

### 1.2 Skizze Erstellen

* Erstelle ein Rechteck mit <kbd>![Sketcher Rechteck erstellen](FreeCAD1-assets/Sketcher_CreateRectangle.svg){ .twemoji height=1em}</kbd>
* Zeichne ein Rechteck beliebiger Größe
* Bestätige die Skizze <kbd>Schließen</kbd>  
Die Ansicht wechselt automatisch zurück in den Arbeitsbereich `Part Design`.

![Skizze mit Rechteck erstellen](FreeCAD1-assets/SkizzeErstellenRechteck.png){ loading=lazy width="80%"}

*Eine Skizze nicht vollständig einzuschränken ist schlechter Stil, doch für die ersten Schritte ist das nicht notwendig. Zum Einschränken SPÄTER (hier fehlt ein Link) mehr.*

### 1.3 Skizze aufpolstern

* Erzeuge aus der Rechteckskizze einen Quader durch Aufpolstern <kbd>![Pad Aufpolstern](FreeCAD1-assets/PartDesign_Pad.svg){ .twemoji height=1em}</kbd> in ⑤
* Wähle eine beliebige Höhe und bestätige mit <kbd>OK</kbd>

![Aufgepolsterte Skizze](FreeCAD1-assets/image34.png){ loading=lazy width="30%"}

### Weitere Veränderungen am Körper

Dein Körper kann nun nach diesem Prinzip weiter verändert werden. Du erstellst auf einer der Seiten des Körpers eine Skizze. Anschließend entscheidest du, ob diese Skizze aufgepolstert, oder nach innen vertieft (Tasche) werden soll.

### 1.4 Zylinder auf der Vorderseite erzeugen

* Du bist im `Part Design` Arbeitsbereich ![PartDesign Arbeitsbereich Logo](FreeCAD1-assets/Workbench_PartDesign.svg){ .twemoji height=1em}.
* Wähle die Vorderansicht <kbd>![Vorderansicht Logo](FreeCAD1-assets/View-front.svg){ .twemoji height=1em}</kbd>
* <kbd>Klick</kbd> auf die Vordereite des Körpers ①. Diese färbt sich grün:

    ![Ausgewählte Vorderseite](FreeCAD1-assets/image37.png){ loading=lazy width="30%"}

* Erstelle eine Skizze auf der ausgewählten Seite des Körpers mit <kbd>![Symbol neue Skizze erstellen](FreeCAD1-assets/Sketcher_NewSketch.svg){ .twemoji height=1em}</kbd>.  
FreeCAD wechselt automatisch in den `Sketcher`-Arbeitsbereich
* Erzeuge einen Kreis mit <kbd>![Sketcher erstelle Kreis](FreeCAD1-assets/Sketcher_CreateCircle.svg){ .twemoji height=1em}</kbd>: wähle einen beliebigen Mittelpunkt und die Größe des Kreises.

    ![Kreis auf Fläche erstellen](FreeCAD1-assets/image38.png){ loading=lazy width="30%"}

* Bestätige und verlasse die Skizze wieder mit <kbd>Schließen</kbd>.  
FreeCAD wechselt zurück in den `Part Design`-Arbeitsbereich.

* Erstelle aus der Kreisskize einen Zylinder du die Skizze wieder aufpolsterst: <kbd>![Pad Aufpolstern](FreeCAD1-assets/PartDesign_Pad.svg){ .twemoji height=1em}</kbd>.

    ![Zylinder auf Ebene aufpolstern Dialog](FreeCAD1-assets/Zylinder-aufpolstern.png){ loading=lazy width="40%"}

* Gib eine beliebige Höhe an und bestätige deine Eingabe mit <kbd>OK</kbd>

### 1.5 Vertiefung erzeugen

* Du bist im `Part Design`-Arbeitsbereich ![PartDesign Arbeitsbereich Logo](FreeCAD1-assets/Workbench_PartDesign.svg){ .twemoji height=1em}.
* Wähle nun die rechte Seite des Körpers mit <kbd>Klick</kbd> aus. Sie färbt sich grün.

    ![Rechte Seite auf Körper auswählen](FreeCAD1-assets/image43.png){ loading=lazy width="30%"}

* Erstelle auf der markierten Seite eine neue Skizze mit <kbd>![Symbol neue Skizze erstellen](FreeCAD1-assets/Sketcher_NewSketch.svg){ .twemoji height=1em}</kbd>.
* Erstelle nun in der Skizze ein Rechteck mit <kbd>![Sketcher Rechteck erstellen](FreeCAD1-assets/Sketcher_CreateRectangle.svg){ .twemoji height=1em}</kbd>. Größe und Position sind beliebig.

    ![Rechteck auf Körper erstellen](FreeCAD1-assets/image41.png){ loading=lazy width="30%"}

* Bestätige und verlasse die Skizze wieder mit <kbd>Schließen</kbd>.
* Erzeuge nun aus der Recheckskizze eine Vertiefung mit <kbd>![Symbol Vertiefung erzeugen](FreeCAD1-assets/PartDesign_Pocket.svg){ .twemoji height=1em}</kbd>  
Tiefe beliebig.
* Bestätige deine Eingabe mit <kbd>OK</kbd>
* Dein Körper sollte inzwischen in etwa so aussehen:

    ![Körper mit Zylinder und Vertiefung](FreeCAD1-assets/image42.png){ loading=lazy width="30%"}

### Baumansicht

In der Baumansicht ⑥ kannst du deine Arbeitsschritte in ihrer Reihenfolge sehen.

Um auch bei komplizierteren Körpern den Überblick zu behalten lohnt es sich, den einzelnen Elementen und Arbeitsschritten Namen zu geben.  
<kbd>Rechtsklick</kbd> auf die Bezeichnung und `Umbenennen` wählen.

Das tolle ist: du kannst jeden Arbeitsschritt durch <kbd>Doppelklick</kbd> und noch verändern, solltest du feststellen, dass dir im Verlauf ein Missgeschick passiert ist.  
Probiere es einfach aus!

### Aufgabe

Konstruiere ein kleines CAD-Monster!

Benenne die einzelnen Teile in der Baumansicht sinnvoll (Kopf, Mund, Arm, usw.)

Viel Spaß!

![CAD-Monster](FreeCAD1-assets/image46.png){ loading=lazy width="30%"}

## Größe der Markerpunkte ändern

Da du nun schon erste Erfahrungen mit FreeCAD gesammelt hast, ist hier ein guter Moment, um dir das Arbeiten weiter zu erleichtern - durch größere Markerpunkte. Wie das geht steht hier: [FreeCAD-FAQ: Größe der Markerpunkte](freecad-faq.md#sec:Markergroesse)

## 2. Bemaßung und Parametrisierung

Die Schritte werden am Beispielgrundkörper durchgeführt.

### 2.0 Darum geht's

* Skizzen bemaßen und symmetrisch beschränken
* Einem Objekt "feste" Abmessungen geben
* Die Schriftgröße der Bemaßungen ändern

### 2.1 Bemaßung des Quaders (Grundkörper)
