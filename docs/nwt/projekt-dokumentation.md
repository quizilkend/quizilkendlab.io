---
title: Projekt Dokumentation
description: Bestandteile einer guten Dokumentation eines NwT-Projekts
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Projekt Dokumentation

Zu jedem NwT-Projekt gehört eine Dokumentation, die es der Lehrkraft oder anderen Interessierten Personen ermöglicht deine Gedankengänge beim Projekt nachzuvollziehen.

Hier wird einmal dargelegt, welche Bestandteile von dir in einer Dokumentation mindestens erwartet werden. Je nach Klassenstufe und Projekt können die einzelnen Abschnitte ausführlicher oder knapper ausfallen.

Hier sind als Basis die einzelnen Abschnitte kurz charakterisiert.

## 0. Vornweg
Im besten Fall hat deine Dokumentation am Anfang

- Ein Deckblatt mit Projekttitel, euren Namen, Datum  und einem aussagekräftigen Bild
- Ein Inhaltsverzeichnis, das [automatisch aus den Überschriften erstellt ist](../office/vorlagen.md#sec:Inhaltsverzeichnis)

## 1. Einleitung
- Was ist das Projektthema und aus welchem Grund wird es bearbeitet. Hat es eine besondere Bedeutung für die Gruppe.
- Konkretes Projektziel.
- Gibt es Leitlinien oder Prinzipien auf die bei diesem Projekt besonders wert gelegt werden (kann von der Lehrkraft oder auch von der Projektgruppe festgelegt sein).

## 2. Planung
Das Projekt sollte in einzelne Bausteine aufgeteilt werden und für jeden Baustein erklärt werden:

- Worum geht es?
- Was es für Lösungsansätze gibt.
- Aus welchem Grund ihr euch für euren Lösungsansatz entschieden habt.
- Erste Entwürfe.
- Wer ist zuständig?

Meistens wird auch ein Zeitplan erwartet, in dem die Arbeitspakete, die sich aus den Bausteinen ableiten in eine sinnvolle Reihenfolge gebracht werden.

## 3. Durchführung
*Wichtig:* Keine tagebuchartige Schilderung eures Vorgehens. 
Stattdessen:

- Knappe Zusammenfassung eurer Tätigkeit.
- Angabe an welchen Stellen von der Planung abgewichen mit Fehleranalyse und Reflexion.

## 4. Abschluss
- Zielkontrolle: wurden die Ziele erreicht?
- Erläterung, warum manche Ziele nicht erreicht wurden.
- Zusammenfassung über die wichtigsten Ergebnisse und den Projekterfolg.
- Optimierungsmöglichkeiten

## Anhang
In den Anhang kommen meist noch einige Dinge (je nach dem was im konkreten Projekt erwartet wird):

- Quellenverzeichnis
- Projekttagebuch (Schilderung, was an den einzelnen Terminen gemacht wurde)
- Zusätzliche Materialien