---
title: Leiterplatten - Häufige Fragen
description: Häufige Fragen zum Leiterplattendesign, Fräsen, etc.
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Leiterplatinen und fritzing - Häufige Fragen

## Warum verwenden wir nicht Software [xyz] fürs Platinendesign?

Am Kepler-Gymnasium-Tübingen sind wir durch die verwendeten Fräsen (KOSY) etwas eingeschränkt.

Die Fräsen verstehen nicht den Industriestandard für die maschinelle Fertigung (G-CODE), sondern wollen in einem bestimmten Dialekt angesprochen werden. Das sorgt dafür, dass wir auf die Verwendung der mitgelieferten Software (nccad) angewiesen sind.

Wollt ihr andere Software nutzen, ist auch das gut möglich. Wichtig ist, dass die Software über passende Exportfilter verfügt (zum Beispiel Fusion360), oder die geplante Platine im `Gerber`-Format exportieren kann. Dann können wir an der Schule die Platine in HPGL exportieren (mit CopperCam), was wiederum von nccad verstanden wird.


## Ich finde Bauteil [xyz] nicht in der Bibliothek

Die Bibliothekt von fritzing Bauteilen ist vielfältig, allerdings ist sie nicht allumfassend. Teilweise muss man Kompromisse eingehen. Hier ein paar Tipps.

- Oft findet man ein Bauteil mit der gleichen Bauform, man kann also nach der Bauform suchen, das Bauteil einfügen und anschließend die Beschriftung passend abändern.
- Wenn die Suche nichts passendes zutage fördert, kann es sinnvoll sein, durch die Kategorien in der `CORE`-Bibliothek zu blättern. Dort findet man die häufigsten Bauteile und meistens sollte auch dein gesuchtes dabei sein.
- Manchmal lohnt es sich um die Ecke zu denken. Wenn man zum Beispiel ein Netzteil sucht, findet man das nicht, aber man kann einfach einen entsprechenden Stecker (`power plug`) einfügen und beschriften.

### Zusatz-Bauteile

Kann man immer noch kein passendes Bauteil finden, hilft oft eine Suche im Fritzing-Forum. Viele Bauteile sind bereits erstellt worden, sodass man auf sie zurückgreifen kann:  
[forum.fritzig.org](https://forum.fritzing.org) Dort einfach nach verschiedenen Bezeichnungen für das Bauteil suchen.


Bauteile, die wir am Kepi verwenden, die aber nicht in der Bibliothek sind:

- Motor-Treiber Platine: [Pololu DRV 8833](PCB-assets/Pololu-DRV-8833-Dual-Motor-Driver-Carrier.fzpz){ download="Pololu-DRV-8833-Dual-Motor-Driver-Carrier.fzpz"}
- Halogen-Lampe mit G4 Sockel: [12V Halogen Lampe](PCB-assets/Halogen-Lamp-G4.fzpz){ download="Halogen-Lamp-G4.fzpz"}