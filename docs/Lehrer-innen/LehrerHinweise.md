---
title: Hinweise für Lehrer:innen
description: Lizenz und Mitarbeit am Material
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---


# Hinweise für Lehrer:innen


Liebe Kolleg:innen,

das Material steht allen frei zur Verfügung (siehe [Lizenz](/index)) und ich freue mich, wenn es über meinen eigenen Unterricht hinaus genutzt wird. Die Dokumente sind in Markdown geschrieben und unter [GitLab](https://gitlab.com/quizilkend/quizilkend.gitlab.io) abrufbar. Dort kann man die Markdown Dateien und Bilder herunterladen und beispielsweise mit [pandoc](https://pandoc.org/) in allerlei andere Formate umwandeln (auch in .doxc oder .odt), dabei helfe ich auch gerne.

Ich würde mich sehr freuen, wenn das Material auch von anderen genutzt wird und am meisten freue ich mich über konstruktive Rückmeldung! Das Material lebt ja davon, dass es stetig verbessert wird.

Hinweise gerne an [greiner@kepi.de](mailto:greiner@kepi.de), oder gerne auch als [Issue](https://gitlab.com/quizilkend/quizilkend.gitlab.io/-/issues) bei GitLab oder sogar als [Merge-Request](https://gitlab.com/quizilkend/quizilkend.gitlab.io/-/merge_requests).

Beste Grüße
Samuel Greiner
