---
title: Messen, Steuern, Regeln
description: Zusammenstellung von Regelstecken mit unterschiedlicher Charakteristik für die Schülis zum Austesten
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Diverse Regelstrecken

Hier soll eine Zusammenstellung von Regelstrecken landen, die sich gut im Unterricht nutzen lassen, so dass die Schülis die Möglichkeit haben, den Regelungsprozess an unterschiedlichen Regelstrecken zu untersuchen.

## Pädagogische Hinweise

Die hier vorgestellte Idee für MSR in der Kursstufe (3 Stündig) ist, den Schülis erst einmal einige Regelstrecken mit Reglern zu zeigen, die die Problematik klar aufzeigen.  
Diese Regelstrecken sollten so komplex sein, dass klar wird, dass sich diese nicht mit einer Steuerung umsetzen lassen, um die Notwendigkeit einer Regelung zu motivieren.  
An diesen Regelstrecken können die Schülis bereits spielerisch mit den Regel-Parametern arbeiten und so etwas wie Schwingverhalten beobachten.

Anschließend wird die Theorie zu Regelungstechnik erarbeitet, wobei immer wieder auf die bereits vorgestellten Regelstrecken zurückgegriffen werden kann.

Am Ende setzen die Schülis selbstständig ein Mini-Projekt um: eine Temperaturregelung im Braunglas.  
Hier kann man auch Schwingverhalten beobachten, Hysterese als sinnvolle Lösung bei einem Zweipunkt-Regler thematisieren und das System ganz ordentlich mit einem PI-Regler ausregeln.

Eine weitere Einbettung in ein größeres Projekt bietet sich bei uns am Kepi in Tübingen am ehesten in Kombination mit Datenkommunikation und Energieversorgung an. Hier können wir am Thema Smart-Home das Zusammenspiel von Sensoren und Aktoren in einem vernetzten Haus umsetzen.


## Simulationssystem Boris

Ursprünglich hatte ich die Idee die Regelung mit Boris umzusetzen, sodass man die Regelstrecken und Regler zuerst simulieren und dann mit Boris auch die Regelung am Arduino umsetzen kann.  
Allerdings hätte das bedeutet, dass die Schülis sich noch mal in eine neue Softare mit ihren eigenen Tücken einarbeiten müssten und ein Regler lässt sich an sich recht simpel in Arduino umsetzen. Daher wird nun versucht die Regler mit einfachen Mitteln mit Arduino umzusetzen.

Sammlung an mit Boris simulierten Regelstrecken:
https://www.kahlert.com/web/download/info_streckenmodelle.pdf

## Schwebender Tischtennisball

Coole Regelstrecke, ein Objekt in einer Röhre wird durch den von einem Lüfter erzeugten Luftstrom in der Schwebe gehalten.

![Schwebender Tischtennisball mit Schwingverhalten](MSR-assets/TT-ball-schwingen.gif){ loading=lazy width="25%"}

### Quellen:

- https://github.com/giovannimasciocchi/Arduino/blob/master/AirFlowBallLevitationAndLightController.pdf
- https://www.youtube.com/watch?v=k0yTh2D-ypQ

### Einkaufsliste:

- HC-SR04 Ulltraschall-Abstandssensor
- Relativ starker Lüfter/Gebläse. Ich verwende eines mit 80mm Durchmesser, es sollte wohl mindestens 2W Leistung haben.
- Arduino Uno
- Röhre: https://www.plattenzuschnitt24.de/Plexiglas-XT-Rohr-3mm-farblos-50-44.html

### Weiteres Material

- 3D-Druckteile
- Tischtennisball oder Styroporball passender Größ
- Ein paar Schrauben
- Einige Dupont-Kabel
- Steckbrett mit Transistor oder Platine
- Netzteil als Spannungsversorgung für das Gebläse (ich habe ein Laptop-Netzteil mit defektem Stecker verwendet)

Die 3D-Druckteile habe ich in FreeCAD konstruiert, die Platine mit Fusion360.
 - [Fusion360 Leiterplatte](https://a360.co/3IWCmyG) wurde mit der KOSY gefräst.
 - [FreeCAD 3D-Druck-Modelle](MSR-assets/TT-ball.zip){ download="TT-ball.zip" }



### Aufbau

1. 3D-Druckteile drucken.
2. Platine fräsen und bestücken (es wurde ein Logic Level MOSFET *IRLZ34N* verwendet).
3. Lüfteranpassung auf Lüfter montieren.
4. Röhre einsetzen.
5. Abstandssensor mit 3D-Teil aufsetzen.
6. Verkabeln.
    1. Der Transistor wird über PIN 5 gesteuert.
    2. Der Ultraschall-Sensor hat seinen Trigger-Pin an 9 und seinen Echo-Pin an 10.
7. Code auf den Arduino laden, Ball rein und losprobieren.
    1. Mein Lüfter war leider nicht leistungsfähig genug, um einen Tischtennisball anzuheben, da noch zu viel Platz in der Röhre war.
    2. Ich habe daher eine Styroporkugel auf eine passende Größe geschliffen.

### Code

![Arduino Code in Aktion auf dem Serial-Monitor](MSR-assets/TT-ball-serial-mon.png){ loading=lazy width="75%"}

- Der Arduino-Code verwendet zum Messen des Abstands die NewPing library.
- Die Größen für die Regelung können über die serielle Schnittstelle eingegeben werden (Sollgröße und Regelparameter).
- Die Regelung ist ziemlich simpel umgesetzt:  
    $u = K_P \cdot e + K_I \cdot I + K_D \cdot D$  
    - e: Eingangsgröße/Regelabweichung
    - I: Summe der letzten 10 Regelabweichungen
    - D: Abweichung der Eingangsgröße im Vergleich zur Eingangsgröße vor 2 Messungen.
- Das meiste sollte im Code dokumentiert sein.


## Ballwippe

Coole Regelstrecke, da sehr schnell reagierend und etwas, was man nicht gut von Hand hinbekommt.

### Quellen

- https://www.youtube.com/watch?v=JFTJ2SS4xyA
- https://wiki.hshl.de/wiki/index.php/Projekt_22:_Ballbalancierer
- https://diglib.tugraz.at/download.php?id=576a77338b56c&location=browse
- https://www.youtube.com/watch?v=FidxDZ7X6OI

### Einkaufsliste

- Arduino Nano
- Sharp 2y0a21
- Futaba S3003



## Temperaturregelung

In braunem Glas mit Lampe. Wie bei Philipp Schenk.


## Balance Roboter

Roboter, der wie ein Segway das Gleichgewicht hält.

### Quellen

- https://circuitdigest.com/microcontroller-projects/arduino-based-self-balancing-robot
- https://www.youtube.com/watch?v=cjSw7sc2JKk

### Einkaufsliste

- Arduino Uno
-


## Höhenregelung

Ähnlich wie bei Philipp Schenk. Flasche auf eine gewisse Höhe bringen mit Motor. Die Höhe wird mittels Ultraschall-Sensor von unten ausgelesen.

Die Masse kann variiert werden und man kann die Flasche hochheben oder runterziehen, als sehr fühlbare Störgröße.

![Höhenregelung Schenk am Gymnasium Hechingen](MSR-assets/Hoehenregelung-Schenk.jpg){ loading=lazy width="25%" align=right}

## Solarnachführung

Eine Solarzelle wird mit einem Dreipunktregler der Strahlungsquelle nachgeführt.

## Helligkeitsgesteuerte Beleuchtung

Als Simulation bei den Boris Regelstrecken dabei. Lässt sich auch in echt nachbauen.
https://www.kahlert.com/web/download/info_streckenmodelle.pdf

## Drehzahlregelung

Drehzahl bestimmen über Lichtschranke und Regeln über die Spannung, die am Motor anliegt.

## Füllstandsregelung

Wassertank auf gewünscht Höhe füllen mit variablem Ablauf:
https://wiki.hshl.de/wiki/index.php/Projekt_24:_Wasserstandsregelung

## Abstandsregelung

Einfach einen 360°-Servo über Stab mit Ultraschall-Abstandssensor kombinieren und einen konstanten Abstand zu einer Hand halten.

Die Halterungen lassen sich super von den SuS selbst entwickeln.

Sehr einfach umzusetzen.

![Abstandsregelung mit 360°-Servo](MSR-assets/360-Servo-Abstandsrgelung.jpg){ loading=lazy width="65%"}


## Follow-Me

Wie ein Rollkoffer, der einem in einem festgelegten Abstand folgt. Der Abstand wird mit einem Ultraschall-Sensor gemessen, die Richtung mit zwei LDRs, sodass das Fahrzeug einer Lichtquelle folgt.

Ergibt schöne Differenzierungsmöglichkeiten, sodass entweder nur die Richtung festgelegt wird (mit konstanter Geschwindigkeit) oder der Abstand, oder eben beides.