---
title: Löten
description: Praktische Hinweise zum Löten (AB)
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Löten

Zum ersten Kontakt mit Löten ein Comic und Einstiegsaufgaben. Das Dokument findet sich [hier](loeten-assets/Loeten.odt){ download="Loeten.odt" }  

## Löt-Comic

Hier der Comic in Kurzform. 

![Löt-Comic in Kurzform](loeten-assets/soldercomic_de.jpg){ loading=lazy width="90%"}
<!-- from https://t.co/b9l49XFKy2 -->

Den Comic gibt es auch noch ausführlicherer Form, allerdings nicht mehr ganz aktuell (mit bleihaltigem Lötzinn): [längerer Löt-Comic](https://mightyohm.com/files/soldercomic/translations/DE_SolderComic.pdf).

## Löt-Aufgaben

![Löt-Übungen](loeten-assets/uebungen.png){ loading=lazy width="90%"}