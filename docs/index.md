---
title: Material für den Naturwissenschaftlichen Unterricht
description: Landingpage von greiner.schule
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
glightbox: false
---

::cards:: cols=2

- title: NwT
  content: Arduino, CAD, Platinendesign und mehr
  image: ./assets/images/NwT.png
  url: nwt/arduino-faq.md

- title: Messwerterfassung
  content: LoggerPro und Tracker
  url: messwerterfassung/loggerpro.md
  image: ./assets/images/Messwerterfassung.png

- title: Praktisches
  content: Löten
  url: praktisch/loeten.md
  image: ./assets/images/Praktisches.png

- title: Office
  content: Tabellen- und Textverarbeitung
  url: office/calc.md
  image: ./assets/images/Office.png

- title: Lehrer:innen
  content: Hinweise und Überlegungen
  url: Lehrer-innen/LehrerHinweise.md
  image: ./assets/images/Lehrer-innen.png

- title: Verschiedenes
  content: Der Rest/Blog
  url: verschiedenes/
  image: ./assets/images/Verschiedenes.png


::/cards::

 Material für den naturwissenschaftlichen Unterricht von Herr Greiner.


Schau dich einfach um oder benutze die Suche, falls du etwas nicht direkt findest!




Diese Seite lebt auch von deinen Hinweisen. Sollte dir also etwas auffallen oder du hast einen Verbesserungsvorschlag, bitte sende mir eine Mail an [greiner@kepi.de](mailto:greiner@kepi.de).
Viel Spaß beim Lesen und vielen Dank für deine Mithilfe!

Samuel Greiner


---

## Lizenz { #sec:Lizenz}

Sofern nicht anders angegeben, stehen alle Materialien hier unter CC-BY-NC-SA 4.0:

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src=assets/images/CC-BY-NC-SA.png /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
