---
date: 2024-01-01 


categories:
  - Software
  - Lehrer

tags:
  - Softare
  - Lehrer
---

# Software

Das ist ein Notizzettel für Software, die ich entweder benutze, oder gerne benutzen würde.

<!-- more -->

- LibreOffice
    - Live Data Stream: Vermutlich lassen sich damit Live Werte von einem Arduino erfassen und verarbeiten. [LO Help Live Data Stream](https://help.libreoffice.org/latest/hr/text/scalc/01/live_data_stream.html).
    Gibt es auch für Microsoft Office als Plugin (Datenstreamer).
    - Languagetool: Wichtiges Plugin, sonst mache ich so viele Rechtschreib- und Kommafehler
    - Im Präsentationsmodus kann man mit der Maus oder Stift Live Notizen machen. https://www.youtube.com/watch?v=O03-IanNG4E
- RNotes: Als OneNote Alternative für Freihand Notizen
