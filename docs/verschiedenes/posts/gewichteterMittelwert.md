---
date: 2024-01-31 

categories:
  - LibreOffice
  - Calc
  - Noten
tags:
  - LibreOffice
  - Calc
  - Noten
---


# Gewichteter Mittelwert mit Fehlstellen in Calc

Sollte man jemals in die Verlegenheit kommen, dass man einen gewichteten Mittelwert in Calc ausrechnen möchte (zum Beispiel zur Bewertung verschiedener Schüli-Leistungen), so wird man schnell feststellen, dass etwaiges Fehlen von Leistungen zu einer erheblichen Verbesserung führt:

<!-- more -->

![Gewichteter Mittelwert mit Fehler](gewichteterMittelwert-assets/gewichteterMittelwert1.png){loading=lazy}

Der gewichtete Mittelwert wurde gebildet, indem die Leistungen immer mit ihren Gewichten multipliziert werden und das Ergebnis durch die Summe der Gewichte geteilt wird. Allerdings geht jetzt eine nicht erbrachte Leistung mit 0 ein, so dass man bei Kathrin Specht erkennen kann, dass der Mittelwert bei fehlenden Leistungen nicht korrekt berechnet wird.

Wir brauchen also eine Möglichkeit die Gewichte nur dann zu zählen, wenn tatsächlich auch eine Leistung erbracht wurde. Abhilfe schafft hier der [`SUMIF`-Befehl](https://wiki.documentfoundation.org/Documentation/Calc_Functions/SUMIF) in Libreoffice. Summanden werden nur dazu gezählt, wenn eine bestimmte Bedingung erfüllt ist. In diesem Fall muss in dem entsprechenden Feld für die Bewertung eine Zahl größer als 0 stehen.

![Gewichteter Mittelwert mit SUMIF](gewichteterMittelwert-assets/gewichteterMittelwert2.png){loading=lazy}

Damit hat nun auch Kathrin den richtig bestimmten Mittelwert.
