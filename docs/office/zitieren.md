---
title: Richtig Zitieren mit Zotero
description: Beschreibung eines ordentlichen Umgangs mit Quellen und vernünftigen Quellenangaben mit LibreOffice und Zotero
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Richtig Zitieren mit Zotero

Beim Schreiben von Texten, die einem wissenschaftlichen Anspruch genügen sollen ist die Arbeit mit Quellen überaus wichtig. Im Idealfall ist im späteren Text jede Aussage, die nicht selbst (bspw. durch ein Experiment) herausgefunden wurde, durch Angabe einer Quelle belegt. Das gibt den Leser:innen die Möglichkeit selbst nachzuprüfen, ob die Information vertrauenswürdig und plausibel ist.

Damit ein Text nachher diesen Anspruch erfüllen kann, ist es notwendig bereits bei der Recherche auf ordentliche Quellenarbeit zu achten. Auf dieser Seite wird eine Methode gezeigt, wie man mit der Software Zotero Quellen zu einem Text gut sammeln und ordnen kann, um später im Text auf sie zu verweisen.

## Die Software Zotero

Zotero ist eine Software zur Verwaltung von Quellen. Praktisch an Zotero ist vor allem die Integration in

- den Browser, sodass direkt aus dem Browser Quellen zu deiner Quellendatenbank hinzugefügt werden können.
- die Textverarbeitung, so dass direkt in der Textverarbeitung Quellenverweise und ein Quellenverzeichnis durch Zotero erstellt und aktualisiert werden können.

### Installation und erster Start {#sec:Installation}

Zotero lässt sich unter [zotero.org/download](https://www.zotero.org/download/) herunterladen und installieren. Nach dem ersten Start öffnet Zotero ein Browser-Fenster, in dem angeboten wird Zotero-Connect für deinen Browser zu installieren. Um Zotero bestens nutzen zu können installierst du auch das Plugin. Möglicherweise wird dir auch noch angeboten die Integration in deine Textverarbeitung zu installieren. Darum kümmern wir uns [später](#sec:Textverarbeitung).

Sollte die Webseite nicht geöffnet werden, oder du möchtest das Browser-Plugin nachträglich installieren, kannst du Zotero-Connect [hier](https://www.zotero.org/download/connectors) herunterladen und installieren.

Nach dem ersten Start und der Installation des Plugins sollte dich Zotero mit diesem Fenster begrüßen:

![Zotero Willkommens-Bildschirm](zitieren-assets/zotero-welcome.png){ loading=lazy width="75%"}

Die Ansicht ist dreigeteilt:

- links: Die Bibliothek unter der du deine Sammlungen, aber auch gespeicherte Suchen und mehr finden kannst
- mittig: Eine Auflistung der Quellen der aktuell ausgewählten Sammlung (oder gespeicherten Suche, etc.)
- rechts: Informationen zu der aktuell ausgewählten Quelle

Außerdem gibt es noch das Suchfeld, mit dem du alle deine Quellen durchsuchen kannst, um schnell die geeignete zu finden.

Hier erstellen wir als ersten Schritt eine neue Sammlung durch einen <kbd>Klick</kbd> auf ![Zotero neue Sammlung Icon](zitieren-assets/zotero-new-collection.png){ .twemoji height=1em}.

Unsere Beispielsammlung hier heißt `NwT-Projekt`. Diese Sammlung möchte nun gefüllt werden, also geht es an die Recherche.

## Recherche: Sammeln von Informationen und Quellen

In der Recherche zu deinem Thema kannst du auf verschiedene Art und Weise an Informationen gelangen. Egal welche Quelle du nutzt, du solltest sie direkt in deine Sammlung aufnehmen, sodass du später unkompliziert auf sie zurückgreifen kannst.

In diesem Kapitel wird gezeigt, wie du unterschiedliche Arten von Quellen unkompliziert deiner Sammlung hinzufügen kannst.

### Buchquelle hinzufügen

Hast du eine relevante Information in einem Buch gefunden, so stehen die Chancen gut, dass du Autor:in, Verlag, Veröffentlichungsdatum, Auflage, etc. nicht händisch eintragen musst. Für die meisten Bücher stehen diese Daten bereits zur Verfügung und man benötigt nur eine Art, das Buch eindeutig zu identifizieren. Glücklicherweise steht quasi in jedem Buch eine `Internationale Standardbuchnummer` oder kurz `ISBN`. Diese ist eindeutig für ein Buch.

Wir möchten also das Buch `What if?` von `Randall Munroe` unserer Sammlung hinzufügen, das Buch hat die ISBN `978-3-813-50652-5`.

Dazu genügt ![Zotero Zauberstab Icon](zitieren-assets/zotero-magic-stick.png){  .twemoji height=1em}, das Einfügen der ISBN und <kbd>Enter</kbd>:

![Zotero Zauberstab ISBN hinzufügen](zitieren-assets/zotero-add-isbn.png){ loading=lazy width="50%"}

Du wirst durch einen Eintrag in unserer Sammlung in dem schon die wichtigsten Informationen zusammengefasst sind belohnt:

![Automatisch ausgefüllter Eintrag von what if](zitieren-assets/zotero-what-if-eintrag.png){ loading=lazy width="35%"}

Häufig ist es sinnvoll, noch eine Notiz zu dieser Quelle zu erstellen. Dort schreibst du, welche Aussage du mit dieser Quelle belegen kannst, und auf welcher Seite sich diese Information findet. So kannst du später über das Suchfenster direkt die nötigen Quellen finden.  
Eine Notiz fügst du im Reiter `Notizen` hinzu (siehe Rahmen im Bild).

### Paper, Journal-Beitrag und Ähnliches hinzufügen

Ähnlich wie eine Buchquelle lassen sich auch die meisten anderen Quellen hinzufügen, die irgendwie in einem Print-Medium veröffentlicht werden, oder in einem Journal erscheinen. Sie alle lassen sich über ein Identifizierungsmerkmal hinzufügen (ISBN, DOI und andere).

Als Beispiel fügst du ein Paper zum Thema [*Neural Style Transfer*](https://en.wikipedia.org/wiki/Neural_Style_Transfer) über die arXiv-ID `1508.06576v2` hinzu:

![Zotero Zauberstab arXiv-ID hinuzfügen](zitieren-assets/zotero-add-arXiv.png){ loading=lazy width="50%"}

Wieder sind bereits die wichtigsten Angaben zur Quelle automatisch eingetragen und du musst nur noch eine Notiz hinzufügen, in der du notierst, welche Aussage du konkret belegen möchtest.

### Internetquelle hinzufügen {#sec:Internetquelle}

Eine Internetquelle hat kein eindeutiges Identifizierungsmerkmal, wie die oben genannten Quellen. Aber du hast ja [bereits](#sec:Installation) das Zotero-Connect-Plugin für deinen Browser installiert.

Wir möchten als erstes Beispiel die Seite zum *Ölfleckversuch* (Experiment zur Bestimmung des Atomdurchmessers) auf [Leifiphysik.de](https://www.leifiphysik.de/atomphysik/atomaufbau/grundwissen/atomdurchmesser-aus-dem-oelfleckversuch) zitieren.

Dazu müssen wir im Browser nur auf das Symbol des Zotero-Connect-Plugins ![Zotero Connect Plugin Icon](zitieren-assets/zotero-connect-icon.png){ .twemoji height=1em} oder ![Zotero Connect Plugin Icon](zitieren-assets/zotero-connect-icon2.png){ .twemoji height=1em} <kbd>Klicken</kbd>:

![Leifi Physik mit Zotero-Connect abspeichern](zitieren-assets/zotero-leifi-browser.png){ loading=lazy width="80%"}

Wenn wir in den neuen Eintrag in der Sammlung sehen müssen wir dieses mal leider feststellen, dass einige Angaben fehlen. Wichtig wären in jedem Fall das Veröffentlichungsdatum oder etwas wie einen Autor. Diese Informationen müssen wir irgendwie der Webseite entlocken. Ganz unten sehen wir folgenden Abschnitt:

![Leifi Kontakt](zitieren-assets/leifi-kontakt.png){ loading=lazy width="40%"}

Wir übernehmen daher `2021` als *Datum* und `Joachim Herz Stiftung` als *Autor* und ergänzen den *Titel der Webseite*:

![Leifi Eintrag in Zotero](zitieren-assets/zotero-leifi-eintrag.png){ loading=lazy width="35%"}

Ebenso können wir auch mit anderen Webseiten verfahren. Für manche Webseiten (z.B. [Wikipedia](https://de.wikipedia.org/wiki/%C3%96lfleckversuch)) ändert Zotero die Einträge direkt entsprechend. Trotzdem müssen oft noch einige Angaben ergänzt werden:

![Wikipedia Eintrag zum Ölfleckversuch](zitieren-assets/zotero-wikipedia-eintrag.png){ loading=lazy width="35%"}

Internetseiten bergen die Gefahr, später nicht mehr in derselben Form auffindbar zu sein, eine Methode eine Webseite "haltbar" zu machen ist unter [Web-Archive](#sec:WebArchive) beschrieben.

Auch bei Internetseiten bietet es sich wieder an, dem Eintrag eine Notiz hinzuzufügen, um die Aussage zu notieren, die durch diese Quelle belegt wird, damit die passende Quelle nachher schnell gefunden wird.

### Quelle manuell hinzufügen

Leider ist es manchmal nicht möglich eine Quelle automatisiert der Sammlung hinzuzufügen. Dann muss man alle nötigen Daten manuell eintragen. Wir tragen als Beispiel den *[Denkangebot Podcast: DA010: Diskriminierung durch Technik](https://www.denkangebot.org/allgemein/da010-diskriminierung-durch-technik/)* ein.

![Zotero Podcast hinzufügen](zitieren-assets/zotero-podcast-hinzufügen.png){ loading=lazy width="35%"}

Hier sehen wir direkt, wie wir noch viele andere Quellentypen hinzufügen können. In unserem Podcast-Eintrag ergänzen wir nun alle relevanten Informastionen:

![Denkangebot Podcast Folge 010 Zotero Eintrag](zitieren-assets/zotero-denkangebot-hinzufügen.png){ loading=lazy width="35%"}

Auch hier sollten wir wieder eine Notiz hinzufügen mit der Aussage, die wir mit dieser Quelle belegen und in diesem Fall auch der Zeitposition, zu der die Aussage getätigt wird.

Jetzt haben wir alles gelernt, um unsere Sammlung mit unseren Quellen zu füllen.

## Zitieren im Text {#sec:Textverarbeitung}

Haben wir die Recherche (teilweise) abgeschlossen, so schreiben wir unseren wissenschaftlichen Text.

### Installation des Textverarbeitungs-Add-Ins

Um das Add-In in unsere Textverarbeitung zu installieren gehen wir in die Zotero-Einstellungen (in der Menüleiste: `Bearbeiten` → `Einstellungen`). Dort wählen wir die Kategorie `Zitieren` und dort wiederum den Reiter `Textverarbeitungsprogramme`:

![Zotero Einstellungen Add-In Installieren](zitieren-assets/zotero-install-addin.png){ loading=lazy width="40%"}

Nun <kbd>Klick</kbd> auf die Schaltfläche für die entsprechende Textverarbeitung (in diesem Artikel wird LibreOffice Writer genutzt) und navigiere durch die folgenden Dialoge, bis das Add-In Installiert ist.

Beim nächsten Start von LibreOffice Writer sollte die Zotero-Symbolleiste zu sehen sein. Ansonsten blendet man sie über `Ansicht` → `Symbolleisten` → `Zotero` ein:

![Zotero Symbolleiste in LibreOffice Writer](zitieren-assets/zotero-panel-LO.png){ loading=lazy}

### Zitationsstil wählen

Meistens werden Vorgaben gemacht, wie eine Quellenangabe im Fließtext und im Quellenverzeichnis auszusehen hat. In unserem Fall halten wir uns an die gültige DIN-Norm *DIN ISO 690:2013-10*. Glücklicherweise kennt Zotero diese Zitationsweise auch und wir müssen Zotero nur mitteilen, dass wir diesen Zitationsstil wünschen.

Dazu öffnen wir in LibreOffice Writer zuerst die Dokument-Eigenschaften mit ![Dokument-Eigenschaften in LO](zitieren-assets/zitero-LO-document-preferences-icon.png){ .twemoji height=1em}. Es öffnet sich folgende Fenster:

![LO Dokument-Eigenschaften Zotero](zitieren-assets/zotero-LO-document-preferences.png){ loading=lazy width="40%"}

Leider steht der gewünschte Zitierstil noch nicht zur Auswahl. Also müssen wir ihn noch hinzufügen durch <kbd>Klick</kbd> auf `Stile verwalten...`.  
Es öffnet sich das Zotero-Einstellungs-Fenster zu Zitierstilen in dem wir auf `Zusätzliche Stile erhalten...` <kbd>Klicken</kbd>:

![Zotero Einstellungen zum Zitieren](zitieren-assets/zotero-preferences-cite.png){ loading=lazy width="40%"}

Im Fenster das sich nun öffnet können wir eine riesige Datenbank mit über 10000 Zitationsstilen durchsuchen. Wir suchen also nach `ISO-690` und wählen `ISO-690 (author-date, Deutsch)` mit einem <kbd>Klick</kbd>.  
Der Zitationsstil sollte nun im Zotero-Einstellungsfenster zu sehen sein, sodass wir dieses mit <kbd>OK</kbd> verlassen können.

Nun müssen wir noch einmal in LibreOffice Writer die Dokument-Eigenschaften mit ![Dokument-Eigenschaften in LO](zitieren-assets/zitero-LO-document-preferences-icon.png){ .twemoji height=1em} aufrufen und können nun endlich `ISO-690` als Zitationsstil auswählen und mit <kbd>OK</kbd> den Dialog verlassen.

![Zotero Dokumenten Eigenschaften für LibreOffice Writer mit ISO-690](zitieren-assets/zotero-LO-document-preferences-ISO-690.png){ loading=lazy width="40%"}

### Zitieren im Fließtext

Wir schreiben nun einen Fließtext, in es um das Schlagen eines relativistischen Baseballs geht. Die Aussagen unseres Absatzes wollen wir natürlich mit der passenden Quelle belegen (das Buch *what if*):

![Zitationsabsatz in LibreOffice](zitieren-assets/LO-Zitationsabsatz.png){ loading=lazy width="50%"}

Dieser Absatz braucht definitiv eine Quellenangabe. Die fügen wir mit ![Zotero Zitation hinzufügen Icon](zitieren-assets/zotero-add-citation-icon.png){ .twemoji height=1em} ein.  
Es öffnet sich ein Suchfenster, in dem wir mit Stichworten nach unserer Buchquelle suchen können:

![Zotero Suchfenster in LibreOffice Writer](zitieren-assets/zotero-LO-search.png){ loading=lazy width="60%"}

Die Quelle wählen wir nun mit einem <kbd>Klick</kbd> oder <kbd>Enter</kbd> aus. Nun haben wir die Möglichkeit eine weitere Quelle hinzuzufügen (genau gleiche Vorgehensweise) und die Quellenangabe noch zu ergänzen. Wir ergänzen die Buchquelle um eine Seitenzahl durch <kbd>Klick</kbd> auf die blau unterlegte Quelle:

![Zotero Suchfenster mit Optionen zur Quellenangabe in LibreOffice Writer](zitieren-assets/zotero-add-Munroe.png){ loading=lazy width="60%"}

Zusätzlich zur Seite lassen sich auch noch Präfix und Suffix ergänzen, falls man zum Beispiel eine veränderte Grafik zitieren würde, dann würde man im Präfix *verändert nach* eintragen. Mehrmaliges <kbd>Enter</kbd> fügt nun unsere Quelle in den Text ein:

![Zitierter Absatz in LibreOffice Writer](zitieren-assets/LibreOffice-zitierter-Absatz.png){ loading=lazy width="60%"}

Nun kannst du alle Aussagen in deinem Fließtext mit Quellen belegen. Von guter Quellenarbeit trennt uns also nur noch ein ordentliches Quellenverzeichnis.

### Quellenverzeichnis anlegen

Die Kurzangaben der Quellen im Fließtext reichen natürlich nicht aus, um eine Quelle eindeutig zu finden. Deshalb muss es am Ende deines Dokumentes noch ein Quellenverzeichnis geben, welches auch gemäß des Zitationsstiles (DIN ISO 690:2013-10) gestaltet ist.

Glücklicherweise kann auch das Zotero für uns übernehmen, sodass wir nur eine neue Seite für das Quellenverzeichnis und die Überschrift erstellen müssen, um mit einem anschließenden <kbd>Klick</kbd> auf ![Zotero Quellenverzeichnis hinzufügen Icon](zitieren-assets/zotero-add-bibliography-icon.png){ .twemoji height=1em} das Quellenverzeichnis einzufügen:

![Quellenverzeichnis durch Zotero erstellt](zitieren-assets/LO-zotero-Quellenverzeichnis.png){ loading=lazy width="60%"}

Damit haben wir alles erledigt, um Quellenarbeit zu leisten, die allen Anforderungen gerecht wird.

## Fortgeschrittene Anwendungen

### Synchronisation

Solltet ihr im Team arbeiten, ist es sinnvoll, wenn ihr auf eine gemeinsame Literatursammlung zurückgreifen könnt. Dann könnt ihr gemeinsam in einem Dokument schreiben und alle können Referenzen zu euren gesammelten Quellen hinzufügen.

Für eine gemeinsame Literatursammlung legt ihr entweder ein gemeinsames Zotero-Konto an, das ihr jeweils mit Zotero auf eurem PC synchronisiert, oder ihr legt jede:r ein Zotero-Konto an und teilt euer Quellenverzeichnis in der Gruppe.

Die Dokumentation von Zotero dazu ist gut, daher hier der Verweis auf [Synchronisation](https://www.zotero.org/support/de/sync) und die [Arbeit in Gruppen](https://www.zotero.org/support/de/groups).

### Ganze Dokumente in der Sammlung haben

Sollte man die Quellen digital vorliegen haben (als PDF, ePUB, o.Ä.), dann Zotero auch diese Dateien verwalten. Die Dateien lassen sich als Anhang zu einem Eintrag hinzufügen, indem man den Eintrag in der mittleren Ansicht auswählt und auf ![Zotero Anhang Icon](zitieren-assets/zotero-attachment-icon.png){ .twemoji height=1em} <kbd>Klickt</kbd>.

Vorteil ist, dass die angehängten Dateien durch die Zotero-Suche mit durchsucht werden, so dass man einfacher wichtige Passagen finden kann, außerdem gehen die Dateien der Quellen nicht verloren.

### Web-Archive {#sec:WebArchive}

Internetseiten können sich im Laufe der Zeit ändern oder sogar ganz verschwinden. Das ist ungünstig, wenn man eine Aussage im Text belegen möchte. Im Idealfall könnten wir den Stand der Webseite archivieren, den wir als Quelle heranziehen.

Glücklicherweise gibt es genau dafür ein Projekt - das [Internet-Archive](https://archive.org). Mit deren Projekt der *Wayback-Machine* können wir einen Schnappschuss einer Webseite erstellen, auf den wir uns in unseren Quellen beziehen können.

Wir möchten nun den [aktuellen Stand eier Foren-Diskussion zum Thema Ölfleckversuch](https://www.physikerboard.de/topic,2147,-avogadro-konstante-und-oelfleckversuch.html) abspeichern.  
Wir rufen die Wayback-Machine auf [web.archive.org/save](https://web.archive.org/save) und tragen die Seite für das physikerboard-Forum ein:

![Internet Archive Save Page](zitieren-assets/archive-save-page.png){ loading=lazy width="60%"}

Anschließend wählen wir <kbd>SAVE PAGE</kbd>. Nun werden einige Dinge gespeichert, bis wir mit einem Link zum Snapshot belohnt werden.

![Internet Archive saved page](zitieren-assets/archive-saved-page.png){ loading=lazy width="60%"}

Jetzt haben wir einen schicken Link zu einem [Schnappschuss](https://web.archive.org/web/20210803064833/https://www.physikerboard.de/topic,2147,-avogadro-konstante-und-oelfleckversuch.html) der Foren-Diskussion.

Wir können den Link zum Schnappschuss nun in unserer Literaturliste verwenden. Dazu gehen wir vor, wie im [Abschnitt Internetquelle](#sec:Internetquelle) beschrieben und ersetzen am Ende in Zotero den Eintrag unter `URL` durch den Link auf den Web-Archive Schnappschuss:

![Zotero Eintrag mit Link auf Web-Archive](zitieren-assets/zotero-entry-web-archive.png){ loading=lazy width="40%"}

So haben wir in unserem Quellenvezeichnis einen Eintrag, der auch noch nach Jahren das abbildet, was unsere Aussage belegt hat.

## FAQ
### Fehler, wenn notwendige Daten fehlen.

Manchmal kann es passieren, dass wir eine Quellenangabe in unserem Dokument hinzufügen möchten, und eine Fehlermeldung erhalten.

Dann stehen die Chancen gut, dass notwendige Angaben im Quelleneintrag in Zotero fehlen. Verdächtig sind *Autor:in, Titel, Datum*. Wenn diese ergänzt sind, sollte sich die Quellenangabe einfach hinzufügen lassen.

### Zitationen aktualisieren

Wenn du die Quellenangaben, nachdem du auf sie in deinem Dokument verwiesen hast, noch einmal veränderst, tauchen diese Änderungen nicht sofort in deinem Dokument auf.

Um alle Quelleneinträge auf den aktualisierten Stand zu bringen <kbd>Klickt</kbd> man auf: ![Zotero Aktualisieren Icon](zitieren-assets/zotero-refresh-icon.png){ .twemoji height=1em}. Damit werden alle Quellenverweise und das Quellenverzeichnis aktualisiert.
