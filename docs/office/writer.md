---
title: LibreOffice Writer Werkzeuge
description: Hinweise für bessere Textdokumente
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# LibreOffice Writer Werkzeuge

Hier stehen einige nützliche Werkzeuge, die einem LibreOffice Writer an die Hand gibt, allerdings noch unvollständig.

## Überschriften




Under heavy construction.

![Under Construction](writer-assets/underConstruction.png){ width="25%"}

Hier fehlen noch einige Dinge, zum Beispiel:

* Querverweise
* Möglicherweise Bild und Tabellenformatierung (inklusive Über- und Unterschriften und Nummerierung)
* Folgevorlagen.
* ...


## Mathematische Formeln einfügen

Um Formeln in Textdokumente einzubinden, bietet Writer ein extra Werkzeug an, den *Formeleditor*.

Eine Formel lässt sich über `Einfügen` → `Objekt` → `Formel`.

![Writer Formel einfügen](writer-assets/writer-Formel-einfuegen.png){ loading=lazy width="80%"}

Es öffnet sich dann direkt der Formel-Editor:

![Writer Formeleditor](writer-assets/writer-Formeleditor.png){ loading=lazy width="75%"}

1. Hier kann man auswählen, wie man die einzelnen Elemente in Bezug zueinander setzen möchte, zB. Bruch, Multiplikation, etc.
2. Hier sieht man, wie die Formel am Ende aussieht.
3. Hier ist der Formel-Editor, indem man mithilfe von ① die Formel schreibt.

Man verlässt den Editor mit <kbd>Esc</kbd> und kann eine wundervoll gesetzte Formel im Textdokument bewundern.
