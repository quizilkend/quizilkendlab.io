---
title: Formgerechte Gestaltung einer Abschlussarbeit
description: Office Werkzeuge nutzen für ein einheitliches Dokument
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Formgerechte Gestaltung einer Abschlussarbeit

Nicht nur in der Schule wird man häufig damit konfrontiert sein, dass man ein Dokument erstellen/abgeben soll, das gewissen formalen Anforderungen entspricht. Zum Beispiel:

* festgelegte Seitenränder
* eine einheitliche Schriftart für das Dokument
* Schriftgrößen für Überschriften und Fließtext
* Zeilenabstände


Das Dokument nach dem Schreiben Arbeit selbst den Anforderungen entsprechend zu gestalten, ist in den allermeisten Fällen lästig und schnell vergisst man Kleinigkeiten (zB. eine Überschrift, die später hinzugefügt wurde und nun nicht im Inhaltsverzeichnis auftaucht).

![Lehrer:innen, wenn Schülis sich nicht an die Formatvorgaben halten](vorlagen-assets/TeacherStyleGuide.png){ loading=lazy width="75%"}

Glücklicherweise haben alle Textverarbeitungsprogramme Funktionen, die einem diese lästige Arbeit abnehmen und für ein einheitliches Aussehen des Dokuments sorgen können. Das Zauberwort heißt hier *Vorlagen*. Man kann Vorlagen für alles erdenkliche erstellen, für Absätze, Zeichen, Tabellen, Seiten, Inhaltsverzeichnisse und viel mehr.  
Der **Grundgedanke** ist hierbei: Formatierungen werden Funktionen im Dokument zugewiesen. So kann man beispielsweise die Formatierung für eine Überschrift festlegen und dann allen Überschriften das Format "Überschrift" zuweisen. Fällt später auf, dass die Überschriften doch etwas größer sein sollten, so ändert man nur die Schriftgröße der Vorlage und sofort sind alle Überschriften nach der neuen Vorgabe formatiert.  
Der Vorteil zur manuellen Formatierung, bei der für jede Überschrift einzeln die Schriftgröße geändert werden muss liegt auf der Hand.

Auf dieser Seite wird die Benutzung verschiedener Formatvorlagen für die Software `LibreOffice` (in der Version 7.1.0 , Benutzeroberfläche: in Registern) gezeigt. Allerdings haben quasi alle Textverarbeitungsprogramme diese Funktionalität und entweder lassen sich die Schritte mehr oder weniger direkt übertragen, oder es findet sich mit einer einfachen Suche auch eine Anleitung für die gewünschte Software.

## Anforderungen {#sec:Anforderungen}

In unserem Beispiel gibt es folgende Anforderungen

* [Seite:](#sec:Seitenvorlagen)
    * 3,5cm Rand links
    * 1,5cm Rand sonst
    * DinA4 Hochformat
    * [Seitennummer:](#sec:Seitennummern) Auf allen Seiten (außer Titelseite und Inhaltsverzeichnis) angegeben
* [Schrift:](#absatzvorlage)
    * Familie: Liberation
    * Schriftgröße Textkörper 12pt
    * Schriftgröße Überschriften: max 150% der Schriftgröße des Textkörpers
* [Zeilenabstand:](#absatzvorlage) 1,15
* [Titelseite](#sec:ErsteSeite) mit Dokumententitel und Symbolbild
* [Nummeriertes Inhaltsverzeichnis](#sec:Inhaltsverzeichnis)
* [Textsatz:](#sec:StandardAbsatz) Blocksatz mit Silbentrennung
* [Bilder](#sec:Bilder) brauchen eine Unterschrift mit fortlaufender Nummer und sollen im [Abbildungsverzeichnis](#sec:Verzeichnisse) mit der Seitenzahl aufgeführt sein
* [Tabellen](#sec:Tabellenbeschriftung) brauchen eine Überschrift mit fortlaufender Nummer und sollen im [Tabellenverzeichnis](#sec:Verzeichnisse) mit der Seitenzahl aufgeführt sein

![Schülis, wenn sie die Formatvorgaben lesen](vorlagen-assets/StyleGuide.svg.png){ loading=lazy width="30%"}

Wir gehen die verschiedenen Vorlagentypen so durch, dass wir diese Anforderungen mit unserem Dokument abdecken können. Danach werden noch weitere Vorlagentypen und ihr Sinn erklärt.

## Absatzvorlagen {#absatzvorlage}

Absatzvorlagen legen Vorgaben für das Schriftbild eines Absatzes vor (Zeilenanfang bis zum nächsten <kbd>Enter</kbd>). Dies ist also der Ort an dem viele unserer Wünsche an die Schrift in Erfüllung gehen.

Tatsächlich lassen sich die Absatzvorlagen an unterschiedlichen Stellen in Writer anwenden und zuweisen:

![Writer Hauptfenster mit Markierung bei den Formatvorlagen](vorlagen-assets/vorlagen1.png){ loading=lazy width="80%" #fig:LOhauptfenster}

1. Hier sieht man die am aktuellen Zeichen momentan zugewiesene Formatierung. Selbstverständlich kann man diese hier ändern, was wir aber nicht tun, da wir ja Vorlagen benutzen wollen, um einen Flickenteppich aus direkter Formatierung zu vermeiden.
2. Hier sieht man einen Überblick der Absatzvorlagen mit Vorschau. Ein <kbd>Klick</kbd> auf die jeweilige Vorlage wendet sie auf den aktuellen Absatz an. Allerdings können wir die Vorlagen hier nicht editieren, daher arbeiten wir mit:
3. Seitenleiste mit Absatzvorlagen. Diese öffnet man, indem man zunächst mit <kbd>Klick</kbd> auf den schmalen grauen Pfeil (3.1) am rechten Rand. Nun ist die Seitenleiste geöffnet. Durch <kbd>Klick</kbd> auf (3.2) bekommt man die Übersicht der Absaztzvorlagen (so lange ![Absatzvorlage Icon](vorlagen-assets/absatzvorlage-icon.png) ausgewählt ist).  
In dieser Ansicht lässt sich auch wunderbar die Hierarchie der Absatzvorlagen sehen (siehe auch [Hierarchie](#erbeVorlage)). So erben quasi alle Absatzvorlagen Eigenschaften von der Vorlage `Standard`. Überschriften erben wiederum zusätzlich Eigenschaften von der Vorlage `Überschrift`, und so weiter.

Hier versuchen wir nun unser Glück, die Anforderungen an die Schrift unterzubringen. Die Einstellungen der betreffenden Absatzvorlagen verändern wir durch Rechtsklick auf die Absatzvorlage in ③ und <kbd>Klick</kbd> auf `Ändern`:

![Absatzvorlage Ändern](vorlagen-assets/vorlagen2.png){ loading=lazy}.

### Standard Absatzvorlage {#sec:StandardAbsatz}

Für das gesamte Dokument soll die Schriftfamilie `Liberation` sein. Da es sich hier um einen längeren Text handelt, sollte der Text mit Serifen geschrieben sein (Liberation Serif) und nur für Hervorgehobenes keine Serifen (Liberation Sans) verwendet werden.  

Wir [ändern](#absatzvorlage) die Absatzvorlage Standard. Es öffnet sich folgendes Fenster:

![Absatzvorlage Ändern](vorlagen-assets/vorlagen3.png){ loading=lazy width="75%"}

Hier lohnt es sich tatsächlich einen Blick in die meisten Reiter zu werfen:

* **Verwalten:**  
Hier steht vor allem der Name der Vorlage und auf welcher Vorlage sie basiert (also von welcher Vorlage sie Eigenschaften erbt).
* **Einzüge:**  
Hier können wir angeben, an welcher Stelle der Text anfangen und enden soll und ob zwischen Absätzen zusätzlicher Abstand eingefügt werden soll.  
Außerdem finden wir hier die Einstellung für den Zeilenabstand und den setzen wir hier, gemäß den Vorgaben auf `1,15`.
* **Ausrichtung:**  
Hier geht es darum, wie der Text die Seite ausfüllt. Soll er immer am linken (linksbündig) oder rechten Rand (rechtsbündig) angrenzen, um die Mitte des Textbereiches angeordnet sein (zentriert) oder sollen die Zeilen so gestreckt sein, dass sie immer am linken und rechten Rand angrenzen (Blocksatz).  
Wir setzen hier gemäß den Vorgaben die Ausrichtung auf `Blocksatz`.
* **Textfluss:**  
Hier geht es um Randbereiche des Dokumentes, was passiert wenn der Text über eine Zeile oder über eine Seite hinausgeht.  
Wir setzen hier gemäß den Vorgaben auf `Silbentrennung - automatisch` (das ist in Kombination mit Blocksatz auch sinnvoll).
* **Schrift:**  
Hier wird der Schrifttyp und die Schriftgröße festgelegt.  
Wie setzen hier gemaß den Vorgaben die Schriftart auf `Liberation Serif` und die Schriftgröße auf `12pt`.
* **Andere Reiter:**
`Schrifteffekte, Position, Hervorhebung, Tabulatoren, Initialen, Fläche, Transparenz, Umrandung, Gliederung` brauchen wir für unser Beispiel nicht, viele Optionen dort sind allerdings selbsterklärend.

Damit haben wir für den Fließtext schon mal alle Vorgaben an die Schrift erfüllt. Allerdings gibt es noch andere Absätze, die konfiguriert werden wollen.

### Überschriften {#sec:Ueberschriften}

Überschriften sollen auch eine eigene Formatierung haben und auch hier machen wir von Absatzvorlagen Gebrauch, denn es wird eine Formatierung bis zum nächsten `Enter` vergeben.

Die Vorlagen für Überschriften sind hierarchisch organisiert (siehe auch [Hierarchie](#erbeVorlage)), so dass wir einige Optionen in der Absatzvorlage `Überschrift` und nur Details in den Vorlagen `Überschrift 1, Überschrift 2, Titel, etc` unterbringen.

* **Überschrift:**  
In der Allgemeinen Vorlage legen wir die Eigenschaften fest, die für alle Überschriften gelten sollte. Dazu [ändern](#absatzvorlage) wir in der Vorlage `Überschrift` im Reiter `Schrift`:

    * Schriftart: Zu `Liberation Sans`
    * Schriftgröße: auf `100%`. Die Prozent beziehen sich immer auf die übergeordnete Vorlage, so dass die allgemeine Überschrift jetzt immer die gleiche Schriftgröße hat, wie die Standard-Vorlage. Das ermöglicht uns gleich den einzelnen Überschriften entsprechende Größen zu geben.
    * Vorlage: Meistens sind Überschriften `Fett`, so dass wir das hier auch angeben können.

* **Titel:**  
Dazu gab es oben gar keine Vorgabe, hier können wir uns also nach Lust und Laune austoben. Zum Beispiel als Schriftgröße 250% vergeben. Genauso für den Untertitel, da können wir die Schriftgröße auf 200% und die Schrift auf `Standard` (also nicht Fett) setzen, um uns vom Titel abzugrenzen.  
Da `Titel` und `Untertitel` nur auf dem Deckblatt auftauchen darf man sich hier insgesamt vom Schriftbild des restlichen Dokuments abheben, ohne ein unprofessionelles Aussehen zu riskieren.

* **Überschrift 1-x:**  
Die Überschriften 1, 2, ... sollten wir jetzt möglichst einheitlich belassen, so dass wir hier alles wie bei der Standard-Überschrift belassen und nur die Schriftgröße anpassen:

    * `Überschrift 1`: 150%
    * `Überschrift 2`: 125%
    * `Überschrift 3`: 110%
    * `Überschrift 4`: 100%

    Durch die Angabe der Schriftgröße in % ändert sich die Schriftgröße der Überschriften gleich mit, wenn wir die Schriftgröße der Vorlage `Standard` ändern - wie praktisch.

Das Ergebnis der ersten Schritte unserer Formatierung kann sich sehen lassen (auch wenn der Inhalt noch zu wünschen übrig lässt):

![Beispiel für die Nutzung von Absatzvorlagen](vorlagen-assets/vorlagen4.png){ loading=lazy width="80%"}

### Weitere Absatzvorlagen

Genau so lassen sich auch die anderen Absatzvorlagen einrichten. Es bietet sich an von der [Hierarchisierung](#erbeVorlage) Gebrauch zu machen. Die meisten weiteren Vorlagen, die bereits vorgegeben sind erklären ihren Sinn durch ihren Titel und viele brauchen wir für unser Beispiel nicht mehr.


## Seitenvorlagen {#sec:Seitenvorlagen}

So nachdem wir das Schriftbild im Text vereinheitlicht und den Vorgaben entsprechend angepasst haben, widmen wir uns den Vorgaben, die für die Seiten gemacht werden. Praktischerweise kann man hier ganz ähnlich vorgehen, wie beim Schriftbild. Wir definieren für unterschiedliche Zwecke Vorlagen für unsere Seiten und weisen diese den entsprechenden Seiten zu.

Die Vorlagen für Seiten bekommen wir zu Gesicht, indem wir recht ähnlich vorgehen wie bei Absatzvorlagen. Konkret wählen wir in der Seitenleiste ③ des [Hauptfensters](#fig:LOhauptfenster) das Icon für die Seitenvorlagen ![Seitenvorlage Icon](vorlagen-assets/seitenvorlage-icon.png) und werden mit dieser Übersicht belohnt:

![Seitenleiste mit Seitenvorlagen](vorlagen-assets/seitenleiste-seitenvorlagen.png){ loading=lazy width="40%"}

Man kann auf den ersten Blick erkennen, dass es hier keine Hierarchisierung gibt, also keine Seitenvorlagen, die auf anderen basieren. Das bedeutet, wir müssen für jede Seitenvorlage alles selbst festlegen, daher erstellen wir lieber nicht allzu viele Seitenvorlagen.

Wir werden zur Erfüllung der Vorgaben von oben mindestens zwei verschiedene Seitenvorlagen benötigen:

1. Eine für Titelseite und Inhaltsverzeichnis: hier wollen wir keine Seitennummern.
3. Eine für den ganzen Rest (`Standard`)
4. Optional weitere, zum Beispiel für den Anhang.

Wir ändern die Seitenvorlagen so wie die Absatzvorlagen (<kbd>Rechtsklick</kbd> → `Ändern`), so dass wir uns direkt um die einzelnen [Anforderungen](#sec:Anforderungen) kümmern.

### Seitenvorlage Standard {#sec:seitenvorlage-standard}

`Standard` ist die Vorlage für alle "nicht speziellen" Seiten im Dokument. Die Enstellungen nehmen wir wieder in den Reitern vor:

* **Verwalten:**  
  Hier gibt es für uns nichts zu tun, außer wir wollen den Namen ändern.
* **Seite:**  
  Hier stellen wir die Seitenränder ein: wie vorgegeben 3,5cm links und sonst 1,5cm

    ![Seitenvorlage Seite](vorlagen-assets/seitenvorlage1.png){ loading=lazy width="60%"}

* **Kopfzeile:**  
  Brauchen wir in diesem Dokument nicht, allerdings wird in längeren Texten häufig der Titel des Kapitels in der Kopfzeile angezeigt, da bräuchte man sie.
* **Fußzeile:**  
  Hier kommt unsere Seitennummer hin, also wählen wir ![Fußzeile einschalten](vorlagen-assets/Seitenvorlage-fusszeile-einschalten.png){ loading=lazy}.  
  Unter Zusätze könnten wir noch eine Umrandung für die Fußzeile festlegen, worauf wir aber in diesem Dokument verzichten.
* **Weitere:**  
  Hier müssen wir nichts einstellen und die Funktionen dort sind größtenteils selbsterklärend. Möchte man besonders viel Text auf einer Seite unterbringen, so lohnt sich unter `Spalten` das zweispaltige Layout auszuwählen. Für unser Dokument ist das allerdings nicht notwendig.

Damit haben wir die Vorgaben für die Seiten im Dokument erfüllt und müssen uns nur noch um unsere Titelseite und die fürs Inhaltsverzeichnis kümmern.

### Seitenvorlage Titel und Inhaltsverzeichnis {#sec:ErsteSeite}

Für unsere ersten beiden Seiten übernehmen wir einfach die Vorlage `Erste Seite` (auch wenn es sich bei uns um die ersten beiden Seiten handelt) und ändern sie nach unseren Wünschen ab.

* **Seite:**  
  Wie in der [Seitenvorlage Standard](#sec:seitenvorlage-standard) bereits beschrieben.
* **Fußzeile:**  
  Hier sollten wir nur darauf achten, dass sie nicht eingeschaltet ist.
* **Weitere:**  
  Hier müssen wir nichts mehr ändern.

Wunderbar. Jetzt haben wir die Seitenvorlagen und müssen sie nur noch an der richtigen Stelle zuweisen.

### Weitere Seitenvorlagen

Sollten uns noch weitere Seitenvorlagen fehlen, so können wir die einfach erstellen mit <kbd>Rechtsklick</kbd> und Neu:

![Neue Seitenvorlage](vorlagen-assets/Seitenvorlage_neu.png){ loading=lazy width="35%"}

Nun begrüßt uns der bereits bekannte Dialog, nur dass der Name der Seitenvorlage noch `Unbenannt 1` ist.

Ab jetzt stellen wir alles wie gewohnt ein.

### Seitenvorlagen zuweisen und Gültigkeit von Seitenvorlagen

Generell können wir eine Seitenvorlage einfach durch `Doppelklick` auf die entsprechende Seitenvorlage in der Seitenleiste ③ zuweisen. Allerdings ist dann das gesamte Dokument mit dieser Seitenvorlage versehen. Wir müssen also LibreOffice irgendwie mitteilen, dass unsere ersten Beiden Seiten eine andere Vorlage als der Rest haben sollen.

Generell geben wir LibreOffice bei einem *manuellen Umbruch* bekannt, welche Seitenvorlage nach dem Umbruch genutzt werden soll.

![Manuellen Umbruch einfügen](vorlagen-assets/Seitenvorlage-manueller-umbruch.png){ loading=lazy width="80%"}

Für unser Dokument könnte das Vorgehen also so aussehen. Wir erstellen erst zwei leere Seiten (Seitenumbruch mit <kbd>Strg</kbd> + <kbd>Enter</kbd>) und nach diesen beiden fügen wir einen manuellen Umbruch ein, bei dem wir als Folgevorlage Standard angeben und die Seitennummer auf 1 ändern.

![Manueller Umbruch Fenster](vorlagen-assets/Seitenvorlage-manueller-umbruch-fenster.png){ loading=lazy width="35%"}

Jetzt legen wir noch bei unseren ersten beiden Seiten als Seitenvorlage `Erste Seite` fest und haben unsere Seitenvorlagen korrekt zugewiesen. (Zwei Spezialseiten mit `Erste Seite` und der Rest `Standard`-Seiten).

Sollten wir im weiteren Verlauf noch einmal einen Wechsel der Seitenvorlage haben wollen, so erreichen wir das wieder mit einem *manuellen Umbruch*.

### Seitennummern einfügen {#sec:Seitennummern}

Da wir gerade dabei sind, die Seiten zu formatieren, schreiben wir jetzt noch in die Fußzeile der Standard-Seiten die Seitenzahl rein. Die Fußzeile lässt sich editieren, indem wir einfach noch auf der Seite, aber unterhalb des Textbereichs <kbd>Klicken</kbd>.

Nun können wir direkt in die Fußzeile schreiben. Da die Fußzeile für alle Seiten identisch ist, können wir nicht die Seitenzahl eintragen. Daher schreiben wir nur `S. ` und fügen danach einen sogenannte `Feldbefehl` für die `Seitennummer` ein. Die Schrift in der Fußzeile richten wir nach rechts aus.

![Feldbefehl einfügen](vorlagen-assets/Seitenvorlage-Feldbefehl-einfuegen.png){ loading=lazy width="80%"}

Das Ergebnis sollte in etwa so aussehen.

![Fußzeile mit Seitenzahl](vorlagen-assets/Seitenvorlage-Seitenzahl.png){ loading=lazy width="80%"}

### Seitenvorlagen fertig

Um den [Ansprüchen von oben](#sec:Anforderungen) zu genügen müssen wir jetzt nur noch einen Titel schreiben, ein Bild für die Titelseite einfügen und dann haben wir die Formatierung der Seiten abgehakt. Wir können uns nun also als nächstes einem schicken Inhaltsverzeichnis widmen.

## Inhaltsverzeichnis {#sec:Inhaltsverzeichnis}

Beim Formatieren der Seiten haben wir ja extra eine Seite Platz gelassen für unser Inhaltsverzeichnis, das wollen wir jetzt auch hinzufügen.

Daher navigieren wir auf die Seite auf der später unser Inhaltsverzeichnis sein soll und wählen dort aus dem Reiter `Bezüge` der Werkzeugleiste `Inhaltsverzeichnis` aus.

![Inhaltsverzeichnis Button](vorlagen-assets/vorlage-inhaltsverzeichnis-button.png){ loading=lazy width="80%"}

Es öffnet sich ein Dialog in dem wir unser Inhaltsverzeichnis nach Lust und Laune konfigurieren können.

![Inhaltsverzeichnis Dialog](vorlagen-assets/vorlagen-inhaltsverzeichnis-dialog.png){ loading=lazy width="50%"}

Tatsächlich können wir hier quasi alle Einstellungen unverändert lassen, für unseren Fall passt es genau so.

Nach einem <kbd>Klick</kbd> auf <kbd>OK</kbd> erscheint ein wunderbares Inhaltsverzeichnis, das *fast* alle Anforderungen von [oben](#sec:Anforderungen) erfüllt. Es fehlen nur leider noch die Nummern der Überschriften.  

### Kapitelnummerierung

Natürlich könnten wir die Nummern manuell vergeben, da wir aber gerade schon so gut dabei sind die Dinge zu automatisieren, sollen auch unsere Überschriften automatisch nummeriert werden. Der Begriff, der hier unsere Herzen höher schlagen lässt ist *Kapitelnummerierung*. Wir finden diese im Reiter `Start`unter `Start → Kapitelnummerierung`.

![Kapitelnummerierung einfügen](vorlagen-assets/vorlagen-Kapitelnummerierung.png){ loading=lazy width="80%"}

Es öffnet sich dieser Dialog:

![Kapitelnummerierung Dialog](vorlagen-assets/vorlagen-kapitelnummerierung-dialog.png){ loading=lazy width="50%"}

Hier können wir jetzt für jede Überschriftenebene, die wir bereits bei den [Absatzvorlagen](#sec:Ueberschriften) eingestellt haben angeben, wie sie nummeriert sein sollen.

* Links ist die **Ebene** angegeben. Um nachher der zweiten Überschrift des dritten Kapitels die Nummer `3.2` geben zu können, muss LibreOffice wissen, wie die Nummern zueinander angeordnet sein sollen. Ebene 1 steht dann an erster Position, Ebene 2 an zweiter, usw.
* Bei **Nummer** wird angegeben, wie die Nummerierung vorgenommen wird, da kann man sich etwas passendes aussuchen.
* **Beginnen mit** sollte selbsterklärend sein
* **Absatzvorlage** spezifiziert welche Absatzvorlage zu dieser Ebene gehört. Meist gehört zur Ebene 1 die Absatzvorlage `Überschrift 1`
* **Zeichenvorlage:** sollte man meist auf `Keine` belassen
* **Vollständig:** Ab Ebene 2 kann man hier angeben, wie viele Ebenen noch mit nummeriert werden. Um eine Nummerierung wie 2.1 bei Überschriften der `Ebene 2` zu erhalten, sollte dort `2` ausgewählt werden. Bei `Ebene 3` dann `3` usw.
* Mit den anderen Optionen kann man rumprobieren, bis einem das Format der Nummerierung gefällt.
* Eine Nummerierung tiefer als bis `Ebene 3` wird meistens unübersichtlich und sollte nur für sehr lange Dokumente verwendet werden

Haben wir für Ebene 1 und 2 eine Nummerierung ausgewählt und für Ebene 2 die Nummerierung vollständig ausgewählt, sehen wir gleich, dass unsere Überschriften Nummern bekommen haben. Auch beim Erstellen weiterer Überschriften übernimmt jetzt LibreOffice die Nummerierung für uns.

![Kapitelnummerierung im Text](vorlagen-assets/kapitelnummerierung-text.png){ loading=lazy width="50%"}

Leider sieht nur das Inhaltsverzeichnis noch gar nicht nach Nummerierung aus. Das liegt daran, dass die Querverweise nicht automatisch aktualisiert werden rein <kbd>Rechtsklick</kbd> auf das Inhaltsverzeichnis und `Verzeichnis aktualisieren` sollte die Nummerierung auch hier erscheinen lassen.

![Kapitelnummerierung im Inhaltsverzeichnis](vorlagen-assets/kapitelnummerierung-inhaltsverzeichnis.png){ loading=lazy width="50%"}

Damit bleiben uns nur noch die Beschriftungen für Bilder und Tabellen, dann haben wir uns schon durch die Anforderungen gekämpft.

## Bilder {#sec:Bilder}

Auch Bilder haben eine eigene Vorlage, die wir an bekannter Stelle in der Seitenleiste ③ finden, und zwar unter `Rahmenvorlagen` ![Rahmenvorlagen Icon](vorlagen-assets/rahmenvorlagen-icon.png){ loading=lazy}.

![Seitenleiste Rahmenvorlagen](vorlagen-assets/Seitenleiste-Rahmenvorlagen.png){ loading=lazy width="35%"}

Wie immer ändern wir die Vorlage über <kbd>Rechtsklick</kbd> und `Ändern` und uns erwartet ein Fenster, das uns von der Struktur schon recht bekannt vorkommt.  
Hier vergeben wir die für wissenschaftlich angehauchte Texte sinnvollen Optionen, dass das Bild mittig angeordnet sein sollte und das Bild nicht von Text umflossen wird.

* **Typ:** Hier wird bei `Position - Horizontal` `Mitte ` ausgewählt
* **Umlauf:** Hier wird `Kein` ![Kein Umlauf Icon](vorlagen-assets/Bildumlauf-Kein.png){ loading=lazy} ausgewählt. Vom Text umflossene Grafiken wirken häufig unprofessionell.
* Die restlichen Reiter kann man so lassen, wie sie bereits eingestellt sind.

### Bildunterschriften {#sec:Bildunterschriften}

Die Bilder sollten immer mit einer nummerierten Unterschrift versehen werden, die kurz zusammenfasst, was auf dem Bild zu sehen ist.

Nachdem wir eine Grafik in unser Dokument eingefügt haben, wird diese über einen <kbd>Rechtsklick</kbd> und `Beschriftung einfügen` beschriftet.

![Bildunterschrift Fenster](vorlagen-assets/Bildunterschrift-Fenster.png){ loading=lazy width="60%"}

* **Beschriftung:** Hier wird die Beschriftung eingetragen. Die können wir nachher zum Glück noch abändern
* **Kategorie:** Hier kann man entweder eine der bereits voreingestellten auswählen oder man vergibt selbst eine. Um Platz zu sparen kann man hier einfach eine Abkürzung eintragen, wie hier `Abb.`
* **Nummerierung:** Hier ist in quasi allen Fällen `Arabisch` die richtige Option
* **Trenner:** Was nach der Nummer steht
* **Position:** Platz der Beschriftung in Bezug auf das Bild, da wir Bildunterschriften wollen, wird hier natürlich `Am Ende` ausgewählt

Das Resultat sieht wieder nett aus:

![Bild mit Unterschrift im Text](vorlagen-assets/Bildunterschrift-im-Text.png){ loading=lazy width="75%"}

Die Nummerierung wird automatisch angepasst, so dass die Abbildungen immer in der richtigen Reihenfolge nummeriert sind, wenn wir die Beschriftung so einfügen.

## Tabellen {#sec:Tabellenbeschriftung}

Verschiedene Arten von Informationen lassen sich besonders übersichtlich und platzsparend in Tabellen ablegen. Beim Dokumentieren von Messergebnissen von Versuchsreihen kommen sie in jedem Fall zum Einsatz. Einfügen kann man sie über `Einfügen → Tabelle` und im Aufklappmenü wählt man die Dimensionen aus. Anschließend befüllt man die Tabelle und formatiert sie so, dass sie übersichtlich aussieht.

![Tabelle einfügen](vorlagen-assets/tabelle-einfuegen.png){ loading=lazy width="75%"}

![Einfache befüllte Tabelle](vorlagen-assets/tabelle-einfach.png){ loading=lazy width="50%"}

Tatsächlich kann man auch eine Tabelle aus Calc direkt einbinden, wenn man diese dort bereits schön formatiert hat, bietet sich das an. Dazu wählt man im Reiter `Einfügen` den Eintrag `OLE-Objekt` aus:

![OLE-Objekt einfügen](vorlagen-assets/tabelle-OLE.png){ loading=lazy width="75%"}

Es öffnet sich ein Fenster, indem wir entweder auswählen können ein neues OLE-Objekt (eingebettetes Office-Dokument) zu erstellen oder ein bestehndes einzubetten. Da wir ja schon eine schicke Tabelle haben, wählen wir `aus Datei erstellen` und wählen das entsprechende Tabellendokument aus. Nun haben wir noch die Option die Datei zu verknüpfen. Ist dieses Häkchen nicht gesetzt, so wird das Tabellendokument in unserem Textdokument gespeichert; ist es gesetzt, wird nur auf das Tabellendokument verwiesen, die Tabelle in unserem Textdokument könnte sich also noch dadurch verändern, dass jemand an dem Tabellendokument arbeitet:

![OLE einfügen Fenster](vorlagen-assets/tabelle-OLE-Fenster.png){ loading=lazy width="35%"}

Haben wir so eine Tabelle eingefügt, so können wir mit einem <kbd>Doppelklick</kbd> alle Späße in dieser Tabelle machen, die wir aus [Calc](calc.md) kennen.

Eigentlich geht es ja hier um die Beschriftung, daher halten wir uns einfach an das Vorgehen der [Bildunterschriften](#sec:Bildunterschriften): <kbd>Rechtsklick</kbd> auf die Tabelle, `Beschriftung einfügen` anklicken und uns im folgenden Fenster austoben:

![Tabellenbeschriftung Fenster](vorlagen-assets/tabelle-Fenster.png){ loading=lazy width="40%"}

Als Bezeichner wurde, wie bereits bei den Bildern eine Abkürzung ausgewählt, die Position ist, entsprechend den [Anforderungen](#sec:Anforderungen) nun `Am Anfang`. Nun haben wir eine korrekt formatierte Tabelle:

![Korrekt formatierte und beschriftete Tabelle](vorlagen-assets/tabelle-beschriftet.png){ loading=lazy width="75%"}

## Andere Verzeichnisse {#sec:Verzeichnisse}

In den [Anforderungen](#sec:Anforderungen) ist noch die Rede davon, dass Abbildungen und Tabellen am Ende noch einmal in einem Verzeichnis aufgeführt sein sollen. Das erledigen wir recht ähnlich, wie bereits beim Inhaltsverzeichnis. Da wir Tabellen und Bilder mit Beschriftungen versehen haben, weiß LibreOffice Bescheid und kann aus den Informationen je ein Verzeichnis bauen.

Analog zum Einfügen eines [Inhaltsverzeichnisses] gehen wir vor um ein Abbdildungs- oder Tabellebverzeichnise einzufügen:

![Inhaltsverzeichnis Button](vorlagen-assets/vorlage-inhaltsverzeichnis-button.png){ loading=lazy width="80%"}

Im Fenster das sich dann öffnet nehmen wir die Einstellungen für ein Abbildungs- oder Tabellenverzeichnis vor:

![Abbildungsverzeichnis Fenster](vorlagen-assets/abbildungsverzeichnis-fenster.png){ loading=lazy width="60%"}

Bey Typ wählt man Abbildungsverzeichnis oder Tabellenverzeichnis aus, dann wird schon einiges so eingestellt, dass es passt. Möglicherweise muss noch die Kategorie angepasst werden, dass sie auf unsere Abkürzung passt. Haben wir unser Abbildungs- und unser Tabellenverzeichnis eingefügt, sieht das doch schon sehr ordentlich aus:

![Abbildungs- und Tabellenverzeichnis im Text](vorlagen-assets/abbildungsverzeichnis-tabellenverzeichnis.png){ loading=lazy width="75%"}

Die Einträge hier sollten sich auch automatisch anpassen, also wenn neue Abbildungen oder Tabellen hinzukommen, oder sich eine Abbildung auf eine andere Seite verschiebt. Sollte sich doch nicht alles automatisch aktualisieren, dann kann man das händisch anstoßen. Dabei werden alle Verzeichnisse und Verweise aktualisiert:

![Alle Verzeichnisse aktualisieren](vorlagen-assets/verzeichnisse-aktualisieren.png){ loading=lazy width="80%"}

Damit haben wir alle Vorgaben erfüllt!

## Vererbung und Hierachie in Vorlagen {#erbeVorlage}

Wie bereits bei den [Absatzvorlagen](#absatzvorlage) angerissen gibt es einige Vorlagentypen, die hierarchisch organisiert sind. Das bedeutet im Endeffekt nichts anderes, als dass es übergeordnete Vorlagen gibt und diese einige Eigenschaften an ihre nachgeordneten Vorlagentypen weitergeben. Damit lassen sich in einem Dokument sehr viele Eigenschaften auf einmal ändern, zum Beispiel die Schriftart in allen Textteilen.

In der Seitenleiste kann man die Hierachie der Absatzvorlagen schon gut erkennen:

![Hierarchie bei den Absatzvorlagen in der Seitenleiste](vorlagen-assets/hierarchie-vorlagen-ansicht.png){ loading=lazy width="35%"}

Alle Absatzvorlagen sind Folgevorlagen von `Standard`. Einige Vorlagen haben wieder eigene nachgeordnete Vorlagen, wie zB. `Textkörper`.

Wir wollen nunr eine Vorlage erstellen, die uns ermöglicht immer die gleichen Eigenschaften zu haben, wie der restliche Text (`Textkörper`), nur dass die Schriftfarbe eben rot sein soll.
Wir erstellen eine neue Vorlage mit <kbd>Rechtsklick</kbd> und `neu`. Im Reiter `Verwalten` legen wir den Namen unserer neuen Vorlage (`Rot`) fest und auf welcher Vorlage sie basiert (`Textkörper`).

![Verwalten eine beerbten Vorlage](vorlagen-assets/hierarchie-neuevorlage.png){ loading=lazy width="50%"}

Damit werden alle Einstellungen von der übergeordneten Vorlage übernommen und wir können in einzelnen Punkten davon abweichen. In unserem Fall also nur, dass die Schrift rot ist. Also setzen wir im Reiter `Schrifteffekte` die Schriftfarbe auf Rot.

![Schriftfarbe einer Absatzvorlage einstellen](vorlagen-assets/hierarchie-schriftfarbe.png){ loading=lazy width="50%"}

Die restlichen Eigenschaften behält unsere Vorlage `Rot` von der Vorlage `Textkörper` bei. Sollten wir nun beispielsweise die Schriftgröße unseres Textes ändern wollen, ändert sich die Schrift bei den roten Absätzen gleich mit.

![Ergebnis einer neuen Absatzvorlage Rot](vorlagen-assets/hiarachie-ergebnis.png){ loading=lazy width="80%"}

Selbstverständlich kann auch in mehr als in einem Punkt von der übergeordneten Vorlage abgewichen werden. Es werden immer alle Eigenschaften der übergeordneten Vorlage übernommen, die nicht explizit abgeändert worden sind. Ist allerdings gewünscht, dass eine gewisse Eigenschaft doch wieder der übergeordneten Vorlage folgt, so können wir das leider nur Reiterweise einstellen. Ein <kbd>Klick</kbd> aus `Standard` setzt im ausgewählten Reiter wieder alle Eigenschaften auf die der übergeordneten Vorlage zurück.


## Weitere Vorlagentypen

Vermutlich raucht dir jetzt sowieso schon der Kopf, aber der Vollständigkeit halber sind hier noch ein paar weitere Vorlagentypen beschrieben, die man aber deutlich seltener braucht, als die oben genannten.

### Zeichenvorlagen

Die Absatzvorlagen legen Eigenschaften der Schrift von einem <kbd>Enter</kbd> bis zum nächsten fest. Sollen sich aber mal Eigenschaften innerhalb eines Absatzes ändern (zum Beispiel Hervorhebungen einzelner Wörter), dann braucht man die Zeichenvorlagen. Zeichenvorlagen ändern nicht die Eigenschaften eines gesamten Absatzes sondern eben nur einzelner Zeichen. Man findet sie neben den Absatzvorlagen:

![Zeichenvorlagen Seitenleiste](vorlagen-assets/zeichenvorlagen.png){ loading=lazy width="30%"}

Praktischerweise ist schon eine für Hervorhebungen eingerichtet. So dass man diese nach den eigenen Vorlieben ändern kann, so dass Hervorhebungen am Ende einheitlich aussehen.

### Rahmenvorlagen

Es gibt einige Objekte in in einem Dokument, die LibreOffice vor allem über ihren Rahmen definiert, für uns sind am wichtigsten wohl die Bilder. Bei den Rahmenvorlagen lässt sich beispielsweise einstellen, wo Bilder positioniert sein sollen (am professionellsten sieht meistens mittig aus) und wie  sie von Text umflossen sein sollen (am professionellsten sieht meist kein Umlauf aus).

![Rahmenvorlagen Seitenleiste](vorlagen-assets/rahmenvorlagen.png){ loading=lazy width="30%"}

### Listenvorlagen

Wenn einem die eingebauten Listen und Aufzählungen von LibreOffice nicht gefallen, dann kann man sie hier anpassen. In den meisten Fällen, sollten aber die eingebauten Vorlagen ausreichend sein.

![Listenvorlagen Seitenleiste](vorlagen-assets/listenvorlagen.png){ loading=lazy width="30%"}

### Tabellenvorlagen

Tabellenvorlagen sind leider noch eine schwierige Sache, man kann sich mal die eingebauten ansehen, allerdings gibt es noch keinen richtig guten Weg in LibreOffice automatisiert alle Tabellen einheitlich zu gestalten, da muss man leider noch an jeder Tabelle einzeln Hand anlegen.

![Tabellenvorlagen Seitenleiste](vorlagen-assets/tabellenvorlagen.png){ loading=lazy width="30%"}

## Anhang

Wow, damit hast du es geschafft und alle Voraussetzungen, um ein vollständig einheitliches und professionell wirkendes Dokument zu erstellen. Der konsequente Einsatz dieser Technik wird dich noch Jahrelang von anderen abheben und deine Dokumente besser aussehen lassen, bis jemand kommt, der LaTeX benutzt.

Hier im Anhang habe ich mal das Dokument mit dem ich die einzelnen Schritte durchgegangen bin noch verlinkt und ansonsten noch ein paar Ressourcen, die über das hier geschriebene hinaus gehen oder es sinnvoll ergänzen.

* Beispieldokument:  
  [Bespieldokument](vorlagen-assets/vorlagen.odt)  
* Dokumentation von LibreOffice zum Thema [Erste Schritte mit Formatvorlagen](https://web.archive.org/web/20210310084114/https://wiki.documentfoundation.org/images/b/b3/ErsteSchritte_Handbuch_Kapitel_03_FormateVorlagen_V62.pdf)


PS: Falls du dich fragst, warum die Bilder hier und im Beispieldokument ohne Quellenangabe daher kommen, diese Bilder sind gemeinfrei. Die Urheberin hat alle Rechte daran abgetreten, so dass man diese Bilder ohne Quellenverweis nutzen und weiter veröffentlichen kann. Eine gute Fundstelle für solche Bilder ist [pixabay](https://pixabay.com).
