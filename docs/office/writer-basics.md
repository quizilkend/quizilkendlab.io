---
title: LibreOffice Writer Basics
description: Grundlagen in der Textverarbeitung mit LibreOffice Writer
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# LibreOffice Writer Basics

Hier sind einige Basisarbeiten mit einer Textverarbeitung erklärt. Fortgeschrittene Werkzeuge findest du unter [LibreOffice Writer Werkzeuge](writer.md).

## Überschriften

Nahezu jedes Dokument braucht für die Struktur Überschriften. Um Überschriften hervorzuheben, kann man der Textverwaltung mitteilen, dass es sich bei bestimmten Zeilen um Überschriften handelt. Ein <kbd>Klick</kbd> auf Überschrift 1 macht die Zeile groß und fett.

![Writer mit zwei Überschriften](writer-assets/writer-basics-ueberschrift.png){ loading=lazy width="75%"}

Wenn du Überschriften brauchst, die nicht ganz so groß sind, so gibt es auch eine *Überschrift 2*, die etwas kleiner ist, eine *Überschrift 3* die noch kleiner ist, usw.

### Überschrift ändern

![Writer Überschrift bearbeiten](writer-assets/writer-basics-ueberschrift-bearbeiten.png){ loading=lazy width="35%" align=right}

Falls du gerne eine andere Schrift oder andere Farbe für deine Überschrift hättest, änderst du das direkt bei der *Vorlage* der Überschrift. Wir wollen zum Beispiel gerne große blaue Überschriften:

In dem Fenster, das sich nun öffnet, lässt sich sehr viel einstellen. Die Schriftfarbe findet man im Reiter `Schrifteffekte`:

![Writer Blau für Überschrift 1 auswählen](writer-assets/writer-basics-ueberschrift-blau.png){ loading=lazy width="50%"}

Nach <kbd>OK</kbd> sollten wie durch ein Wunder alle Zeilen, die davor als `Überschrift 1` markiert wurden, blau geworden sein.

![Writer mit blauer Überschrift 1](writer-assets/writer-basics-ueberschrift-blau2.png){ loading=lazy width="50%"}

Nun kannst du nach Lust und Laune deine Überschriften anpassen und sie werden immer einheitlich bleiben. Pass dabei nur auf, dass es nicht unübersichtlich wird :-)

## Aufzählungen

![Writer Aufzählungen in der Werkzeugleiste](writer-assets/writer-basics-aufzaehlungen.png){ loading=lazy align=right}

Oft ist es übersichtlicher einige Dinge nicht als Fließtext in ganzen Sätzen, sondern als Aufzählung aufzuschreiben. Dafür gibt es in der oberen Werkzeugleiste eine Option für nicht nummerierte Aufählungen ① und nummerierte Aufzählungen ②. 

Eine Aufzählung ohne Nummerierung könnte dann so aussehen:

![Writer Beispiel einer Aufzählung](writer-assets/writer-basics-aufzaehlungen-beispiel.png){ loading=lazy}

## Bilder einfügen

Oft kann es auch sein, dass du eine Grafik oder ein Bild einfügen möchtest. Dazu musst du das Bild nur als Datei finden und kannst es dann mit der Maus ins Dokument ziehen. Dazu hältst du die *linke Maustaste* gedrückt und lässt sie erst wieder los, wenn du die Maus über dein Dokument bewegt hast.

Die Grafik kannst du nun durch *Ziehen* mit der Maus an die gewünschte Stelle im Dokument bewegen und an den Ecken Verkleinern und Vergrößern.

![Writer: Beispiel eines eingefügten Bilds](writer-assets/writer-basics-bild-eingefuegt.png){ loading=lazy width="60%"}


![Writer: Bild mit Rechtsklick einfügen](writer-assets/writer-basics-bild-rechtsklick-einfuegen.png){ loading=lazy width="30%" align=right}

Bilder aus dem Internet kannst du im Normalfall so einfügen, dass du auf das Bild einen <kbd>Rechtsklick</kbd> machst und `Kopieren` auswählst. An der passenden Stelle im Dokument machst du wieder einen <kbd>Rechtsklick</kbd> und wählst `Einfügen`.

