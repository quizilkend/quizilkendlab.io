---
title: Allgemeine Office Hinweise
description: Mit diesen Tipps bessere Dokumente schreiben
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---


# Allgemeine Office Hinweise

Beziehen sich auf LibreOffice.

## Rechtschreib- und Grammatikprüfung

Für LibreOffice gibt es das hervorragende Plugin Languagetool, welches neben einer Rechtschreibprüfung auch Grundlagen in der Grammatik und Kommasetzung beherrscht. Außerdem kann es einen auf stilistische Feinheiten aufmerksam machen, wie z. B. das Vermeiden von Wortwiederholungen und umgangssprachlichen Phrasen.

Installation von hier [LibreOffice Extension Languagetool](https://extensions.libreoffice.org/en/extensions/show/languagetool)

Nach Installation ist Languagetool direkt als Rechtschreibprüfung eingerichtet. Die automatische Rechtschreibprüfung lässt sich über `Extras -> Automatische Rechtschreibprüfung` aktivieren.

## Feldbefehle

Stell dir vor, du möchtest, dass am Anfang deines Dokuments immer das Datum der letzten Bearbeitung steht. Selbstverständlich könntest du das bei jeder Bearbeitung neu ändern, oder du benutzt einen **Feldbefehl**.

Bei Feldbefehlen teilst du dem Programm mit welchen Wert es für dich hinschreiben soll z. B.:
- Aktuelles Datum
- Aktuelle Seitenzahl oder Gesamtseitenzahl des Dokuments
- Name der Autorin
- und vieles mehr.

Du findest sie unter <kbd>Einfügen</kbd> - <kbd>Feldbefehl</kbd>:

![Feldbefehl einfügen](office-assets/feldbefehl-einfuegen.png){ width="50%"}

Du erkennst eingefügte Feldbefehle daran, dass der Text grau hinterlegt ist. Beim Drucken oder Export wird dieser Hintergrund entfernt.

![Satz mit Feldbefehlen](office-assets/satz-mit-feldbefehl.png){ width="50%"}