---
title: LibreOffice Calc Hinweise
description: Wie man ein Tabellenkalkulationsprogramm richtig benutzt
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---


# LibreOffice Calc

In einem Tabellenkalkulationsprogramm kann man sehr praktisch viele Daten ablegen. Insbesondere bietet es sich damit für die Dokumentation und Auswertung von Messreihen an. Hier ein Beispiel in dem Messwerte zur Bestimmung der Wärmekapazität von Wasser durchgeführt wurden:

![Calc mit Messwerttabelle](calc-assets/calc-Messwerttabelle.png){ width="75%" loading=lazy}


Das ist zwar übersichtlich, aber noch kein großer Zugewinn, gegenüber einer Tabelle in einem Textdokument. Ein Tabellenkalkulationsprogramm spielt seine Stärken vor allem in der Auswertung von Messwerten aus.

## Berechnungen mit Calc

Calc kann angewiesen werden Rechenoperationen mit Daten aus anderen Zellen auszuführen.

Wir bleiben bei unserem Beispiel (siehe oben). Tatsächlich interessiert an dieser Stelle die Dauer, die das Wasser erhitzt wurde gar nicht, sondern vor allem, wie viel Energie dem Wasser dabei zugeführt wurde.

Aus Physik ist bekannt, wie die übertragene Energie mit der Leistung zusammenhängt: $E = P \cdot t$.

Um nicht selbst rechnen zu müssen, geben wir also Calc die Anweisung diese Berechnung auszuführen.

**Eine Rechenanweisung in Calc beginnt immer mit einem**  ``=``  **in der Zelle.**

In unserem Beispiel steht die Leistung im Feld A30 und die Zeit, die zum Erhitzen aufgewandt wurde in der Spalte A. Für den ersten Messwert ergibt das:

![Calc mit einer simplen Berechnung in einer Zelle](calc-assets/Calc-Berechnung1.png){ width="50%" loading=lazy}

Und nach Enter:

![Calc mit einer simplen Berechnung in einer Zelle](calc-assets/Calc-Berechnung2.png){ width="50%" loading=lazy}

Das ist jetzt schon ganz schön, denn falls uns auffallen sollte, dass die Leistung in diesem Fall doch $P = 310 W$ ist, so können wir das einfach im Feld A30 ändern und die Berechnung in B4 wird erneut ausgeführt, so dass dort weiterhin der korrekte Wert steht.

In dieser Messreihe haben wir nun 19 Messwerte und es ist müßig 19 mal eine quasi gleiche Berechnung in die Zellen einzutippen. Auch hier unterstützt uns Calc. Wir können die Berechnung auf benachbarte Zellen übertragen. Dazu wählt man die Zelle mit der Berechnung aus und "fasst" das kleine Quadrat an der unteren rechten Ecke an. Nun kann man die Berechnung auf weitere Zellen übertragen (nur in eine Richtung horizontal oder vertikal). Hier wird nicht die gleiche Berechnung immer wieder in die weiteren Zellen kopiert, sondern die referenzierten Zellen werden in der gleichen Richtung mit verschoben.

In der Zelle B5 wird dann also ``=A31*A5`` stehen. Dummerweise ist das gar nicht genau das, was wir wollen. Denn wir wünschen uns, dass nur eine Referenzzelle mitbewegt wird (die für die Zeit), die andere Referenz soll immer auf die selbe Zelle zeigen.

**Um eine Referenz zu fixieren setzt man vor den Bezeichner ein $-Zeichen.**

Also steht jetzt in der Zelle B4 ``=$A$30*A4`` und man kann die Berechnung auf die darunterstehenden Zellen übertragen:

![Calc Übertragen einer Berechnung](calc-assets/Calc-Berechnung3.png){ width="50%" loading=lazy}

Aufmerksamen Leser:innen wird aufgefallen sein, dass wir zwei $-Zeichen verwendet haben, obwohl wir nur eine Zelle festgelegt haben. Tatsächlich lässt sich mit den $-Zeichen ein Mitbewegen in einer Dimension unterbinden.

- Das $-Zeichen vor einem Buchstaben legt die Spalte fest und verhindert ein horizontales Mitbewegen
- Ein $-Zeichen vor einer Zahl legt die Zeile fest und verhindert ein vertikales Mitbewegen

Damit wurde die Berechnung auf weitere Zellen übertragen! Das Resultat kann sich sehen lassen:

![Calc mit übertragener Berechnung](calc-assets/Calc-Berechnung4.png){ width="75%" loading=lazy}


## Diagramme mit Calc

Um Messdaten auszuwerten und zu interpretieren möchte man oft ein Diagramm haben. In Calc markiert man dazu erst die Daten, die man visualisieren möchte. Liegen die Zellen nicht nebeneinander, so kann man <kbd>Strg</kbd> gedrückt halten und damit weitere Zellen markieren. Eine Zeile für die Bezeichnung der Daten kann mit markiert werden, dann kann die Bezeichnung automatisch mit ins Diagramm übernommen werden.

![Calc mit markierten Zellen für ein Diagramm](calc-assets/Calc-Diagramm1.png){ width="75%" loading=lazy}

Über <kbd>Einfügen</kbd> -> <kbd>Diagramm</kbd> öffnet sich ein Dialog:

![Calc mit Diagramm Dialog 1](calc-assets/Calc-Diagramm2.png){ width="50%" loading=lazy}

Hier kann man das Diagramm nach Lust und Laune anpassen und mit <kbd>Vor</kbd> durch die Dialoge navigieren. Die Einstellungen ergeben sich je nach Anforderung. Für Messwerte sollte meist ein *x-y-Streudiagramm* gewählt werden. In diesem Beispiel sind die Datenreihen in Spalten und die erste Zeile ist die Beschriftung. Die Namen der Datenreihen sollte man noch anpassen, so dass die Masse des Wassers als Beschriftung dasteht. Belohnt werden sollte man mit etwas ähnlichem wie:

![Calc mit Diagramm](calc-assets/Calc-Diagramm3.png){ width="50%" loading=lazy}

### Trendlinie einfügen

Häfig hat man eine Vermutung, durch was für eine Funktion die Messwerte beschrieben werden können (bspw. linear, quadratisch, Wurzel, exponentiell). Calc kann eine solche Funktion so in die Messwerte legen, dass sie diese am besten beschreibt (genauer: die Quadrate der Abstände zwischen Messwerten und Ausgleichsgerade werden minimiert).

Wenn man das Diagramm bearbeitet (Doppelklick aufs Diagramm), klickt man mit der rechten Maustaste auf einen Punkt einer Datenreihe und wählt ``Trendlinie einfügen``. Daraufhin öffnet sich ein Dialog:

![Calc Trendlinien Dialog](calc-assets/Calc-Diagramm-Trendlinie1.png){ width="50%" loading=lazy}

In diesem kann man die entsprechende Funktionsart auswählen (in unserem Fall ``Linear``) und noch weitere Optionen wie den Namen festlegen. Bspw. müsste man für eine Ursprungsgerade den Y-Achsenabschnitt auf 0 festlegen. Oft ist es auch sinnvoll, sich die ermittelte Gleichung anzeigen zu lassen, da so etwas wie die Steigung von Interesse ist. In diesem Fall wählt man am besten auch noch `Bestimmtheitsmaß (R²) azeigen`, das ist ein Maß dafür, wie gut die Werte auf die Trendlinie passen (0: passt nicht, 1: passt perfekt):

![Calc Diagramm Trendlinie1](calc-assets/Calc-Diagramm-Trendlinie2.png){ width="50%" loading=lazy}

In diesem Fall wissen wir nun, dass $\Delta T (E) = 0,00159 \frac{K}{J} \cdot E$ ist, wenn wir eine Wassermasse von $m=150g$ erhitzen.  


### Unterschiedliche y-Achsen

Sollen in einem Diagramm unterschiedliche Daten dargestellt werden, so kann es praktisch sein, unterschiedliche y-Achsen zu haben. Um einer Datenreihe eine y-Achse zuzuweisen, geht man wie folgt vor:

-  <kbd>Rechtsklick</kbd> auf einen Datenpunkt im Diagramm und `Datenreihe formatieren` wählen.
- Im Fenster nun `Sekundäre Y-Achse` auswählen und mit <kbd>OK</kbd> bestätigen.

Sinvollerweise werden alle Datenreihen, die die gleiche Größe darstellen der gleichen y-Achse zugewiesen.

### Wärmekapazität von Wasser

Der Vollständigkeit halber hier noch (sehr knapp) die abschließenden Schritte zur Berechnung der Wärmekapazität bei der Messreihe mit 150g Wasser. Die Schritte bringen uns keinen Erkenntnisgewinn mehr für das Nutzen eines Tabellenkalkulationsprogramms.
Unter der Annahme, dass $\Delta T (E) \sim \frac{1}{m}$ ergibt sich: $\Delta T (E) = 0,00159 \cdot 150 \frac{g \cdot K}{J} \cdot E = 0,2385 \frac{g \cdot K}{J} \cdot E$.  
Die Wärmekapazität ist nun aber genau als die Energie die aufgewandt werden muss um 1g Wasser um 1K zu erhitzen. Daher müssen wir noch den Kehrbruch unsere Proportionalitätskonstante berechnen: $c = 4,19 \frac{J}{g \cdot K}$

## Funktionen zur Auswertung von Messwerten

Tabellenkalkulationsprogramme haben meist praktische Funktionen eingebaut, die es einem erleichtern Messwerte und Messreihen auszuwerten und einzuordnen.

### Mittelwert { #sec:Mittelwert}

Kurz: `=MITTELWERT(Zellbereich)`

Hat man eine Messung vielfach durchgeführt, oder eine Größe mehrfach bestimmt, so erhält man experimentell meist nicht für alle Messungen den exakt gleichen Wert.

Kann man davon ausgehen, dass alle Messwerte ähnlich sind, so bestimmt man den Mittelwert aller gültigen Messergebnisse als Ergebnis der verschiedenen Messungen.

$ Mittelwert = \frac{\text{Summe der einzelnen Werte}}{\text{Anzahl der Messwerte}}$ 

Damit man das nicht händisch machen muss, gibt es in Calc die Funktion Mittelwert. In das Feld, in dem der Mittelwert stehen soll schreibt man:  
`=MITTELWERT(Zellbereich)`

Zum Beispiel:

![Calc Mittelwert 1](calc-assets/Calc-Mittelwert1.png){ loading=lazy width="80%"}

Bestätigen mit <kbd>Enter</kbd> spuckt einem den Mittelwert aus:

![Calc Mittelwert 2](calc-assets/Calc-Mittelwert2.png){ loading=lazy width="80%"}

### Standardabweichung

Kurz: `=STABW(Zellbereich)`

Wie bereits bei [Mittelwert](#sec:Mittelwert) beschrieben, weichen die Messwerte oft mehr oder weniger stark voneinander ab. Ein Maß dafür, wie ähnlich sich die Messwerte sind ist die Standardabweichung.

Je kleiner die Standardabweichung für die Messwerte, desto geringer sind die Abweichungen der Messwerte untereinander und desto geringer sind zufällige Messungenauigkeiten. Wenn wir also Messungen besonders präzise ausführen, sollte unsere Standardabweichung auch besonders gering sein.

In Calc bestimmen wir die Standardabweichung analog zum Mittelwert mit einer integrierten Funktion, wir schreiben in eine Zelle:  
`=STABW(Zellbereich)`

Zum Beispiel:

![Calc Standardabweichung 1](calc-assets/Calc-Standardabweichung1.png){ loading=lazy width="80%"}

Bestätigen mit <kbd>Enter</kbd> spuckt einem die Standardabweichung aus:

![Calc Standardabweichung 2](calc-assets/Calc-Standardabweichung2.png){ loading=lazy width="80%"}

Es lohnt sich erst ab einer Menge Messwerte (ungefähr 15) die Stanardabweichung zu bestimmen.


## Calc FAQ

??? info "Calc zeigt mir in meiner Zelle gar nicht das an, was ich eingetippt habe"
    Calc versucht einem Arbeit abzunehmen indem es den Datentyp in einer Zelle erkennt. Leider liegt es dabei auch oft falsch. Zum Glück, kann man Calc explizit mitteilen, welchen Datentyp man in einer Zelle abgelegt hat:

    * Die betroffenen Zellen markieren (Spalten oder Zeilen durch Klick in den Bezeichner oben oder an der Seite).
    * <kbd>Rechtsklick</kbd> im markierten Bereich,  auf `Zellen formatieren`
    * Im linken Reiter kann man das Format für Zahlenwerte auswählen - mit <kbd>OK</kbd> bestätigen.
