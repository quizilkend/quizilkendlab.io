---
title: Impressum
description: Rechtliche Hinweise zu greiner.schule
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Impressum

## Angaben gemäß §5 TMG
Verantwortlich für die Inhalte:

Samuel Greiner  
Kepler-Gymnasium Tübingen  
Uhlandstraße 30  
72072 Tübingen

### Kontakt
greiner@kepi.de

## Datenschutzerklärung

Benennung der verantwortlichen Stelle

Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist Samuel Greiner, siehe oben.

Diese Webseite hat keine externen Inhalte und speichert keine personenbezogenen Daten über Ihre Besucher:innen. Es werden allein statische .html-Seiten ausgeliefert.

Die Webseite wird über gitlab gehostet, es kann sein, dass dort Verbindungsmetadaten gespeichert und ausgewertet werden, wie:

- Besuchte Seite auf dieser Domain
- Datum und Uhrzeit der Serveranfrage
- Browsertyp und Browserversion
- Verwendetes Betriebssystem
- Referrer URL
- Hostname des zugreifenden Rechners
- IP-Adresse

Diese Daten sind dem Betreiber dieser Webseite nicht zugänglich.

Es findet keine Zusammenführung dieser Daten mit anderen Datenquellen statt. Grundlage der Datenverarbeitung bildet Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.

Recht auf Beschwerde bei der zuständigen Aufsichtsbehörde

Als Betroffener steht Ihnen im Falle eines datenschutzrechtlichen Verstoßes ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu. Zuständige Aufsichtsbehörde bezüglich datenschutzrechtlicher Fragen ist der Landesdatenschutzbeauftragte des Bundeslandes, in dem sich der Sitz unseres Unternehmens befindet. Der folgende Link stellt eine Liste der Datenschutzbeauftragten sowie deren Kontaktdaten bereit: https://www.bfdi.bund.de/DE/Infothek/Anschriften_Links/anschriften_links-node.html.


Quelle: Datenschutz-Konfigurator von mein-datenschutzbeauftragter.de
