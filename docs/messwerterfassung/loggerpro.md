---
title: LoggerPro - Messwerte erfassen
description: Beschreibung zur Benutzung der Software LoggerPro um Daten Messdaten aufzunehmen und auszuwerten
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Messwerte erfassen mit LoggerPro

LoggerPro ist eine Software von Vernier um Messdaten der Sensoren von Vernier einfach aufnehmen und auswerten zu können. Auf dieser Seite wird beschrieben, wie man grundsätzlich Messreihen mit LoggerPro aufnimmt und grafisch auswertet.

Die Anleitung bezieht sich auf LoggerPro in der Version 3.16.1 diese ist auf den Rechnern am Kepler-Gymnasium Tübingen installiert. Allerdings sollte sie auch für die meisten anderen Versionen von LoggerPro hilfreich sein.

## Erster Start von LoggerPro

Beim ersten Start von Logger Pro wird man mit folgendem Fenster empfangen. Viele Optionen gibt es nicht, die Gliederung ist klar:

1. Datentabelle: Hier finden sich später die Daten der Messreihe des Versuchs wieder.
2. Diagramm(e): Hier sind später Diagramme der Messdaten zu finden.
3. Werkzeugleiste: Hier finden sich häufig genutzte Werkzeuge wieder.
4. Menüleiste: Seltener gebrauchte Funktionen finden sich hier.

![LoggerPro Startfenster](loggerpro-assets/LoggerPro-Startfenster.png){ loading=lazy width="80%"}


## Anschluss von Sensoren und Auslesen von Messwerten

Sensoren lassen sich unterschiedlich an den Rechner mit LoggerPro anschließen.

1. Direkt: Manche Sensoren verfügen über einen eigenen USB-Controller und lassen sich direkt per USB anschließen.
2. Über eine "Anschlussbox": Viele Vernier Sensoren geben ihre Messdaten nicht über USB weiter, so dass man noch eine Anschluss-Box benötigt. Das kann ein *Go!Link* zum Anschluss eines einzelnen Sensors sein oder ein *LabQuest* zum Anschließen mehrerer Sensoren.

Ist ein Sensor angeschlossen wird er von LoggerPro sogleich erkannt und das Fenster ändert sich entsprechend. Hier als Beispiel der Anschluss eines *Go!Motion* Entfernungssensors:

![LoggerPro mit angeschlossenem Go!Motion Sensor](loggerpro-assets/LoggerPro-Sensor-angeschlossen.png){ loading=lazy width="80%"}

1. In der Datentabelle sind jetzt direkt die passenden Spalten mit entsprechenden Einheiten aufgeführt.
2. Es gibt nun zwei Diagramme, die LoggerPro für diesen Sensor für interessant hält.
3. In der Werkzeugleiste ist der Button ![Icon Messung Starten](loggerpro-assets/icon-messung-starten.png){ .twemoji height=1em}<kbd>Starten</kbd> zum Start einer Messung nun aktiv.
4. Es gibt nun ein Fenster unten links, dass die aktuellen Messwerte der angeschlossenen Sensoren anzeigt.

## Aufnehmen einer Messreihe

Eine Messreihe startet man mit ![Icon Messung Starten](loggerpro-assets/icon-messung-starten.png){ .twemoji height=1em}<kbd>Starten</kbd> in ③.

Allerdings lohnt es sich davor einmal einen Blick auf die Einstellungen zur *Datenerfassung* zu werfen. Diese ruft man über
![Icon Datenerfassung](loggerpro-assets/icon-datenerfassung.png){ .twemoji height=1em} in ③ auf (alternativ über `Versuch`  → `Datenerfassung`).

![Fenster Datenerfassung](loggerpro-assets/loggerpro-Datenerfassung.png){ loading=lazy width="50%"}

Im Fenster für Datenerfassung lassen sich einige Parameter zur Messreihe einstellen. Wichtig sind hier vor allem

* Die Werte zur `Dauer` der Messreihe.  
Ist man unsicher, wie lange die Messreihe sein soll, kann man einfach `Fortlaufende Datenerfassung` auswählen, dann läuft die Messung bis sie manuell durch ![Icon für Stop der Messreihe](loggerpro-assets/icon-Stop.png){ .twemoji height=1em} beendet wird.
* Die `Abtastrate`: Häufigkeit mit der Messwerte aufgenommen werden.

Hat man die Einstellungen vorgenommen, so lässt sich die erste Messreihe aufnehmen. Die Messreihe wird mit ![Icon Messung Starten](loggerpro-assets/icon-messung-starten.png){ .twemoji height=1em}<kbd>Starten</kbd> in ③ gestartet.

### Beispiel Messreihe {#sec:Beispiel}

 Hier wird als Beispiel die Bewegung eines Spielzeugwagens mit den Standard-Einstellungen aufgezeichnet:

![Versuchsaufbau Go!Motion mit Wagen](loggerpro-assets/GoMotion-mit-Wagen.jpg){ loading=lazy width="75%"}

Die Messung wird gestartet und der Wagen anschließend angeschuckt. Das Ergebnis in LoggerPro:

![Erste Messung in LoggerPro](loggerpro-assets/loggerpro-erste-messung.png){ loading=lazy width="75%"}

Man sieht auf den ersten Blick, wie sich in ① die Tabellen mit Messdaten gefüllt haben und in ② Diagramme zur Messung erstellt wurden. In ④ sieht man den letzten aufgenommenen Messwert.

In den Diagrammen lässt sich wunderbar erkennen, dass der Wagen durchs Anschucken schnell auf die Maximalgeschwindigkeit gebracht wurde, dann durch Reibung die Geschwindigkeit wieder nachgelassen hat, bis er zur Ruhe gekommen ist.

## Diagramm-Einstellungen

Häufig sind in den Diagrammen nicht nur die Werte zu sehen, die für den Versuch relevant sind. Daher ist es sinnvoll die Diagramme anzupassen, um die meisten Informationen daraus ziehen zu können.

### Zoom

In unserem obigen [Beispiel](#sec:Beispiel) sind einige Bereiche uninteressant. Der Zeitbereich am Anfang, als das Auto noch nicht angeschuckt wurde und der Zeitbereich am Ende, als es bereits wieder ruht.  

Es gibt mehrere Methoden, um das Diagramm auf die relevanten Werte einzugrenzen:

1. An den Enden der Achsen lässt sich eintragen welchen Wertebereich das Diagramm abdecken soll:  

    ![Loggerpro Wertebereich der Achsen einstellen](loggerpro-assets/loggerpro-achsenenden.png){ loading=lazy width="60%"}

2. Der Zoom-Bereich lässt sich durch *ziehen* eines Rechtecks im Diagramm auswählen und durch <kbd>Rechtsklick</kbd> und `Diagramm vergrößern` anpassen:  

    ![LoggerPro Zoom-Bereich](loggerpro-assets/loggerpro-zoom-bereich.png){ loading=lazy width="60%"}


### Andere Achsen (zB. Kennlinie aufnehmen)

Gerade, wenn man eine Kennlinie eines elektronischen Bauteils aufnehmen möchte, interessiert man sich nicht besonders, für den Verlauf der Größe in Abhängigkeit der Zeit, sondern in Abhängigkeit einer anderen Größe. Für elektronische Bauteile ist oft die UI-Kennlinie interessant, die die Stromstärke in Abhängigkeit der angelegten Spannung angibt.

So eine Kennlinie lässt sich in LoggerPro direkt aufnehmen. Hat man Sensoren für die Spannung und Stromstärke bereits angeschlossen, so muss man nur noch das Diagramm anpassen. Dazu <kbd>Rechtsklick</kbd> ins Diagramm und `Optionen für Diagramme` auswählen.

![Loggerpro Kontextmenü Optionen für Diagramme](loggerpro-assets/loggerpro-Diagrammoptionen.png){ loading=lazy width="25%"}

Im Fenster `Optionen für Diagramm` macht man die Einstellungen im Reiter `Achsenoptionen`. Für ein UI-Diagramm wählt man für die Y-Achse die `Stromstärke I` aus und für die X-Achse die `Spannung U`.

![Loggerpro Diagrammoptionen Achsenoptionen](loggerpro-assets/loggerpro-Achsenoptionen.png){ loading=lazy width="40%"}



### Weitere Einstellungen

Möchte man die Diagramme weiter anpassen, so gelingt das mit <kbd>Rechtsklick</kbd> ins Diagramm und `Optionen für Diagramme`.

Hier lassen sich nach Lust und Laune Dinge einstellen, zum Beispiel, dass die Punkte nicht verbunden werden sollen (sinnvoll!).
Im Reiter `Achsenoptionen` lassen sich die Einstellungen für die Achsen (Start- und Endpunkt, etc.) einstellen.

## Sensor Einstellungen

### Nullpunkt für einen Sensor setzen

Manchmal kann es sinnvoll sein, den Messwert des Sensors auf Null zu setzen.  
Das gelingt unter `Versuch → Auf Null stellen` oder mittels <kbd>Strg + 0</kbd>

### Verschiedene Sensoren

1. Go!Motion: Ulltraschall Entfernungsmesser  
  Dieser Sensor lässt sich direkt per USB anschließen. Der Sensor lässt sich aufklappen und unter der Abdeckung sollte der Schieber in Richtung der Person und des Balles zeigen.

    ![Go!Motion mit der richtigen Einstellungen](loggerpro-assets/GoMotion-Sensor-Einstellungen.jpg){ loading=lazy width="50%"}


## Funktionsanpassung

Hat man eine Vermutung, welche Funktion die Messwerte beschreiben könnte, so kann man mit LoggerPro auch eine Funktion an die Messwerte anpassen lassen.

In unserem [Beispiel](#sec:Beispiel) sieht der Teil der Bewegung ja verdächtig nach einer umgekehrten Parabel aus. Das wollen wir überprüfen:

Dazu wählen wir den Button für Kurvenanpassung ![Icon Kurvenanpassung](loggerpro-assets/icon-kurvenanpassung.png){ .twemoji height=1em} in ③ aus.

![LoggerPro Kurvenanpassung Fenster](loggerpro-assets/loggerpro-kurvenanpassung.png){ loading=lazy width="80%"}

- Zuerst wählen wir den Bereich aus, in dem wir eine Kurvenanpassung vornehmen wollen.  
Dazu ziehen wir in ⓐ ein Rechteck um den Bereich, der uns interessiert.
- Nun müssen wir den Funktionstyp angeben, der auf unsere Daten passen soll.  
Da wir eine Parabel vermuten, müssen wir leider einen eignen Funktionstyp *Parabolisch* definieren.
  - <kbd>Funktion definieren</kbd>:  
    ![LoggerPro Funktion definieren](loggerpro-assets/loggerpro-funktion-definieren.png){ loading=lazy width="40%"}
  - Da der Höchste Punkt der vermuteten Parabel leider nicht bei `t=0s` liegt, müssen wir auch das in unserer Formel beachten:  
  `A*(t-B)^2+C`  
  Mit dem zeitlichen Versatz `B` und dem Höhenversatz `C`.
  - Wir geben der Funktion einen schönen Namen `Parabolisch` und verlassen den Dialog <kbd>OK</kbd>.
- Nun lassen wir LoggerPro die Anpassung vornehmen, indem wir <kbd>Anwenden</kbd> auswählen.
LoggerPro versucht nun selbstständig die Konstanten `A,B,C` so zu bestimment, dass die Messwerte möglichst gut beschrieben werden.
- In diesem Beispiel wurde tatsächlich eine sinnvolle Anpassung durchgeführt und es sieht danach aus, als ob unsere Vermutung stimmt. Wir können das Fenster also mit <kbd>OK</kbd> verlassen.

Ergebnis:

![Angepasste Funktion](loggerpro-assets/loggerpro-angepasste-funktion.png){ loading=lazy width="80%"}

#### Einhüllende

Leider kann LoggerPro keine Einhüllende zu einer periodischen Schwingung zeichnen. Dafür bräuchte man wohl CassyLab.


## Weiterverwendung der Werte {#sec:Weiterverwendung}

Möchte man die Messwerte nur als Ausgangspunkt für weitere Berechnungen nutzen, so lohnt es sich die Werte in ein Tabellenkalkulationsprogramm zu übertragen. Dort sind weitere Berechnungen und Darstellungen leichter umzusetzen als mit LoggerPro.

Die interessanten Messwerte oder Spalten lassen sich einfach markieren, mittels <kbd>Strg + C</kbd> kopieren und mit <kbd>Strg + V</kbd> in ein Tabellenkalkulationsprogramm einfügen.

![Daten aus LoggerPro in ein Tabellenkalkulationsprogramm übertragen](loggerpro-assets/loggerpro-daten-raus.png){ loading=lazy width="80%"}.

Hinweise zur weiteren Verwendung von Daten in einer Tabellenkalkulation finden sich [hier](/office/calc).

## Aufnehmen der Messdaten in eine Versuchsdokumentation

Um eine Annahme zu bestätigen oder zu widerlegen benötigt man meistens nicht die gesamten Messwerte, sondern ein aussagekräftiges Diagramm.

Beim Schreiben einer Dokumentation bietet es sich daher an, ein Diagramm als Grundlage der Schlussfolgerung zu nehmen und die Messwerte in den Angang zu packen. So bleibt das Protokoll übersichtlich und nachvollziehbar.

Möchte man ein konsistentes Bild im Protokoll haben, so sollte man Diagramm und Protokoll mit der gleichen Software erstellen, also die Daten aus Logger-Pro, [wie oben beschrieben](#sec:Weiterverwendung), in eine Tabellenkalkulation übertragen und auch dort das Diagramm erstellen.
