---
title: Tracker - Bewegungsaufzeichnung
description: Beschreibung zur Benutzung der Software Tracker zum Auswerten von Bewegungen in Videos
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Bewegungsauswertung mit Tracker

Tracker ist eine Software, mit der sich Bewegungen von Objekten in einem Video auswerten lassen. Die Daten lassen sich anschließend in Diagrammen darstellen oder weiter auswerten.

## Die Basics

### Das Tracker Hauptfenster {#sec:Hauptfenster}

In der Standardansicht gliedert sich Tracker in drei Bereiche:

1. Ein Fenster auf der linken Seite, in dem das Video angezeigt wird
2. Rechts oben ein Bereich für ein Diagramm
3. Rechts unten ein Bereich für die tabellarische Darstellung der Messwerte
4. Unter dem Video-Fenster die Video-Leiste mit Bedienelementen für die Video-Wiedergabe
5. Oben eine Werkzeugleiste.

![Tracker Hauptfenster](tracker-assets/TrackerHauptfenster1.png){ loading=lazy}

### Aufzeichnen einer Bewegung mit dem Autotracker {#sec:Bewegungsaufzeichnung}

#### 1. Ein Video öffnen:

![Tracker Öffnen](tracker-assets/TrackerOffnen.png) in der Werkzeugleiste ⑤.

#### 2. Den richtigen Zeitbereich auswählen:

Häufig stellt die Bewegung, die untersucht werden sollen nur einen Teil des Videos dar. Daher wählt man in der Video-Leiste ④ mit den beiden Dreiecken den Start und das Ende der zu untersuchenden Bewegung ein.

![Tracker Video-Leiste](tracker-assets/TrackerVideoLeiste1.png){ loading=lazy}

#### 3. Koordinatenachsen festlegen:

Damit die Bewegung von Tracker korrekt ausgewertet werden kann, ist es wichtig Tracker mitzuteilen, wo der Koordinatenursprung liegt und in welche Richtung x- und y-Achse zeigen.

1. Dazu fügt man ein `Achsenkreuz` ein mit ![Tracker Achsenkreuz hinzufügen](tracker-assets/TrackerAchsenkreuz.png) in der Werkzeugleiste ⑤.  
2. Den Ursprung des Koordinatensystems legt man in den zu verfolgenden Körper zum Startzeitpunkt der Bewegung: das Koordinatensystem lässt sich verschieben, indem man es am Ursprung *greift*.
3. Die positive x-Richtung des Koordinatensystems (angezeigt durch einen kleinen Querstrich auf der Achse) legt man so fest, dass sie in Richtung der Bewegung zeigt.

    ![Tracker Koordinatensystem1](tracker-assets/TrackerKoordinatensystem1.png){width="49%" loading=lazy} ![Tracker Koordinatensystem2](tracker-assets/TrackerKoordinatensystem2.png){ width="49%" loading=lazy}

#### 4. Maßstab setzen:

Damit in der Auswertung später korrekte Werte angezeigt werden, muss man Tracker mitteilen, wie lange Strecken im analysierten Video sind. Dazu identifiziert man zwei Punkte im Video, die einen bekannten Abstand haben und teilt diesen Tracker mit einem `Kalibrierungsmaßstab` mit.

1. In der Werkzeugleiste ⑤ klickt man auf `neue Kalibrierungsoption und/oder Anzeige` ![Tracker Maßstab Button](tracker-assets/TrackerMassstabButton.png){ loading=lazy} und wählt anschließend `neu -> Kalibrierungsmaßstab` ![Tracker Maßstab Neu](tracker-assets/TrackerMassstabNeu.png){loading=lazy}.
2. Nun wählt man mit <kbd>Shift ↑</kbd>+<kbd>Linksklick</kbd> einen Start- und einen Endpunkt aus und trägt die Entfernung zwischen diesen beiden ein.

    ![Tracker Koordinatensystem eintragen](tracker-assets/TrackerMassstabEintragen1.png){ loading=lazy width="50%"}

#### 5. Objekt erstellen: {#sec:PunktmasseErstellen}

Es wird ein Track erstellt, in dem die Bewegung eines Objekts von Tracker ausgewertet werden soll.  

1. Dazu klickt man auf ![Werkzeugleiste Track](tracker-assets/tracker-track.png){ loading=lazy} , dann auf ![Track Neu](tracker-assets/tracker-neu.png){ loading=lazy} und wählt im einfachsten Fall `Punktmasse` aus: ![Werkzeugleiste Neue Punktmasse](tracker-assets/TrackerPunktmasse.png){ loading=lazy}. Nun erscheint ein kleines Zusatzfenster mit dem Namen des Tracks (meist erst mal `Masse A`).
2. Diesem Track kann man jetzt allerlei Informationen hinzufügen, wenn man möchte. Die Masse kann man in der Leiste zum Track (unter der Werkzeugleiste ⑤) ändern: ![Tracker Track-Leiste](tracker-assets/TrackerTrackLeiste.png){ loading=lazy}.  
Außerdem kann man noch einen Namen und eine Beschreibung für den Track vergeben, indem man auf den Namen des Tracks in dem Zusatzfenster klickt und den entsprechenden Punkt auswählt:

    ![Tracker Track Menü](tracker-assets/TrackerTrackMenu.png){ loading=lazy}

#### 6. Bewegungsverfolgung mit Autotrack  

Tracker kann die Bewegung eines Objektes automatisch verfolgen. Dazu wird im Menü des eben erstellten Objekts  `Autotrack` ausgewählt.  

  ![Tracker Track Menü Autotrack](tracker-assets/TrackerAutotrackWahl.png){ loading=lazy width="40%"}  

Daraufhin öffnet sich das Autotracker Fenster:

  ![Tracker Autotracker Fenster](tracker-assets/TrackerAutotrackFenster.png){ loading=lazy width="60%"}

Da ja die gesamte Bewegung verfolgt werden soll, sollte man bevor man mit dem Autotracking beginnt an den Beginn der Bewegung springen. Dazu springt man mit ![Tracker Video Leiste zurück](tracker-assets/TrackerVideoLeisteZurueck.png) in der Video-Leiste ④ zum Beginn der Bewegung.  

1. Man wählt eine Schablone aus, die durch das Video verfolgt werden soll. Damit dies gut gelingt ist es wichtig eine Form zu finden, die im gesamten Video sichtbar, gut erkennbar und unverändert ist. Zusätzlich sollte man darauf achten, keinen oder wenig Hintergrund in der Schablone zu haben, da sich dieser meist ändert und ja eben nicht verfolgt werden soll.
2. Mit <kbd> Strg</kbd> + <kbd> Shift  ↑</kbd> + <kbd> Klick</kbd> wird das Zentrum der Schablone ausgewählt.
3. Um den angewählten Punkt erscheinen zwei Rahmen:  
  Ein durchgezogener Kreis: Dieser zeigt die Schablone. Nach dieser Form wird Tracker im nächsten Bild suchen.  
  Ein eckiger gepunkteter: Dieser gibt den Suchbereich an. In diesem Bereich wird Tracker im nächsten Bild nach der Schablone suchen.  
  Bei beiden sollte man die Größe anpassen, indem man sie auf mit dem kleinen Rechteck in der jeweils rechten unteren Ecke auf die gewünschte Größe zieht.  

    ![Tracker Schablone](tracker-assets/tracker-schablone.png){ loading=lazy width="80%"}  

4. Wenn die Schablone und der Suchbereich ordentlich ausgewählt sind, <kbd>klickt</kbd> man im Autotracker-Fenster auf `Suchen`. Tracker verfolgt nun die ausgewählte Form Bild für Bild automatisch. Sollten sich Schwierigkeiten ergeben informiert einen Tracker darüber im Autotrack-Fenster. Meist muss dann entweder ein Punkt akzeptiert werden, bei dem sich Tracker nicht sicher ist, oder ein Punkt manuell ausgewählt werden (mit <kbd>Shift</kbd> + <kbd>Klick</kbd>).  
  Bewegt sich das Objekt nur parallel zur x-Achse kann das auch ausgewählt werden. Meist erzielt man damit bessere Ergebnisse.

    ![Tracker Autotrack Suchen](tracker-assets/tracker-autotrack-suchen.png){ loading=lazy width="60%"}

#### 7. Auswertung:

Ist die Bewegung fertig verfolgt, kann man sich das Ergebnis in verschiedenen Diagrammen und Datentabellen von Tracker anzeigen lassen. Im [Hauptfenster](#sec:Hauptfenster) findet man die Auswertungen in den Bereichen ② und ③.  
Interessant sind vor allem die Diagramme:  

  ![Tracker Diagramm](tracker-assets/tracker-diagramm.png){ loading=lazy width="60%" #sec:Auswertung}

Hier lassen sich ganz unterschiedliche Dinge auf den Achsen darstellen. Ein <kbd>Klick</kbd> auf die Achsenbeschriftung öffnet ein Auswahlmenü der verschiedenen Darstellbaren Größen.  
Ein <kbd>Klick</kbd> auf die Achsenenden ermöglicht das manuelle Festlegen von Minimum und Maximum auf der Achse (ist oft praktisch um mathematische Zusammenhänge vernünftig zu erkennen).

In der Tabelle in ③ lassen sich mit einem <kbd>Klick</kbd> auf `Daten` auch noch unterschiedliche Spalten anzeigen, falls man die Daten für andere Größen noch benötigt:  

![Tracker Datentabelle](tracker-assets/tracker-datentabelle.png){ loading=lazy width="60%"}


Damit haben wir die grundlegende Anwendung von Tracker gemeistert!


## Eine Bewegung in mehreren Dimensionen verfolgen

Selbstverständlich kannst du mit Tracker auch eine Bewegung, die sich nicht nur in einer Dimension abspielt aufzeichnen und analysieren. Tracker zeichnet sogar automatisch die x- und y-Werte des bewegten Objektes auf.  
Um eine Bewegung auch in mehreren Dimensionen zu analysieren, kannst du die Diagramme, wie unter [Auswertung](#sec:Auswertung) beschrieben, anpassen.  
Für die Bewegung in mehreren Dimensionen ist es sinnvoll den Bereich ③ im [Hauptfenster](#sec:Hauptfenster) auch als Diagramm zu nutzen. Dann lässt sich die Bewegung beispielsweise wunderbar in einem *x-t-Diagramm* und einem *y-t-Diagramm* darstellen.

![Tracker Daten zu Diagramm](tracker-assets/tracker-daten-diagramm.png){ loading=lazy width="60%"}


## Mehrere Objekte verfolgen

Mit Tracker lassen sich beliebig viele Objekte verfolgen. Dazu verfährst du wie gewohnt ([Bewegungsaufzeichnung ab Objekt erstellen](#sec:PunktmasseErstellen)) und fügst einfach eine weitere Punktmasse hinzu.

Anschließend musst du darauf achten, dass viele Module in Tracker eine Angabe der Datenquelle benötigen: In den Diagrammfenstern muss angegeben werden, welches Objekt gemeint ist:

![Tracker Diagramm Datenquelle auswählen](tracker-assets/tracker-datenquelle.png){ loading=lazy width="60%"}

## Externe Ressourcen

* Eine etwas knappere Beschreibung der Funktionen von Tracker auf den Webseiten der [Lehrerfortbildung BW](https://web.archive.org/web/20210322152959/https://lehrerfortbildung-bw.de/u_matnatech/physik/gym/bp2016/fb5/4_mechanik/2_mb/02_tracker/)
