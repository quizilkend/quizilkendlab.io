---
title: Tracker - häufige Fragen
description: Hilfestellung zu häufig auftretenden Problemen
author: Samuel Greiner
documentclass: scrartcl
fontfamily: libertinus
geometry: "left=1.5cm,right=1.5cm,top=2.5cm,bottom=2.5cm"
urlcolor: "blue"
header-includes: |
    \usepackage{float}
    \makeatletter
    \def\fps@figure{H}
    \makeatother
    \usepackage{fancyhdr}
    \pagestyle{fancy}
    \fancyfoot[C]{\thepage}
    \fancyfoot[L]{CC-BY-NC-SA 4.0}
---

# Häufige Fragen zu Tracker

## Mein Diagramm zeigt nach unten, es soll aber nach oben zeigen {#sec:Achsen}

Vermutlich liegt das Achsenkreuz nicht im Ursprung der Bewegung, oder die positive x-Richtung zeigt in die Gegenrichtung der analysierten Bewegung.  
Abhilfe schafft eine Korrektur des Koordinatensystems. Springe im Video an den Start der Bewegung (mit der Video-Leiste ④ im Hauptfenster) und

* überprüfe ob dein Koordinatenursprung am verfolgten Punkt liegt
* überprüfe ob die positive x-Richtung (angezeigt durch einen kleinen Querstrich an der Koordinatenachse) in Richtung der Bewegung zeigt.

Es kann sein, dass zum Ändern der Achsen das aktive Objekt auf die Achsen umgestellt werden muss:

![Tracker aktives Objekt auswählen](tracker-assets/tracker-aktives-objekt.png){ loading=lazy width="40%"}

## Das Video ist nicht richtig orientiert

Es kann sein, dass man für eine vernünftige Auswertung das Video in Tracker drehen muss.

Dazu wählt man in der Menü-Leiste `Video → Filter → Neu → Drehung`:

![Tracker Video Drehen](tracker-assets/tracker-video-drehen.png){ loading=lazy width="75%"}

Im nächsten Fenster wählt man die passende Drehung aus.

![Tracker Drehung Fenster](tracker-assets/tracker-drehung.png){ loading=lazy width="50%"}

## Ich habe etwas falsches an irgendeiner Stelle eingetragen

Quasi alle Parameter lassen sich im Nachhinein noch ändern. Tracker passt die Auswertung automatisch an die geänderten Parameter an.

* Die Länge des Maßstabes lässt sich durch <kbd>Klick</kbd> auf den blauen Maßstab nachträglich ändern.

* Die Achsen lassen sich nachträglich Drehen und der Ursprung verschieben (siehe [oben](#sec:Achsen)).

* Die Masse des Objekts lässt sich ändern, indem man das Objekt als aktives auswählt (siehe [oben](#sec:Achsen)) und dann die Masse ändert.
