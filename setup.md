# Setup dieser Webseite

## mkdocs

Die Webseite basiert auf dem Mkdocs template für Gitlab-CI.

## Material for mkdocs

* Als Theme wurde Material for Mkdocs ausgewählt. Und entsprechend den Empfehlungen in der sehr guten Dokumentation eingerichtet.
* Um den PDF-Download-Button oben rechts zu haben, wurde eine kleine Verrenkung mit der Repo-URL angestellt (sieht man in der `mkdocs.yml`)

## PDF-Download

Alles dazu findet sich im pdfpages Abschnitt in der `.gitlab-ci.yml`.   
Im Endeffekt werden die Seiten auf einem Minimal-Linux mit pandoc und Latex gebaut, die Schriftfamilie hat eine kleine Verrenkung benötigt.

## mathjax

Mathjax ist lokal gehostet. Die js Dateien befinden sich in dem Verzeichnis `javascripts/mathjax/` aus dem mkdocs.yml wird auf diese verwiesen. Damit entfallen die zwei Zeilen code für externe js-Ressourcen, die sonst von Material for mkdocs empfohlen werden.

Um Mathjax zu aktualisieren können via git die upstream aktualisierten js Dateien geholt werden.  
[Self Host Mathjax](https://docs.mathjax.org/en/latest/web/hosting.html#getting-mathjax-via-git)
